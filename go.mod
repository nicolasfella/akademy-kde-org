module akademy-kde-org

go 1.15

require (
	github.com/PhuNH/hg-companion v0.0.0-20230913110054-b505a89c6dad // indirect
	invent.kde.org/websites/hugo-kde v0.0.0-20240405124143-a8a9a6a5066c // indirect
)
