---
title: Home
---

# Akademy

Akademy is the annual world summit of [KDE](https://kde.org/), one of the largest Free Software communities in the world. It is a free, non-commercial event organized by the KDE Community.

{{< container class="pt-1 pb-4 text-center" >}}

![](/media/2022/akademy2022-groupphoto-med.jpg)
*Akademy 2022*  
<small>by Akademy Team <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="/media/2018/ccby-wee.png"></a></small>

{{</ container >}}

The conference is expected to draw hundreds of attendees from the global KDE Community to discuss and plan the future of the Community and its technology. Many participants from the broad free and open source software community, local organizations and software companies will also attend.

Akademy features a 2-day conference with presentations on the latest KDE developments, followed by 4 days of workshops, Birds of a Feather (BoF) and coding sessions. It is an opportunity to meet key KDE contributors, learn about the latest features and enjoy the great atmosphere. You will meet developers, artists, translators, upstream and downstream maintainers, users, and free and open software industry leaders from all over the world.

**[Akademy 2024](/2024) is a hybrid event, in Würzburg, Germany & online from Saturday 7th - Thursday 12th September**
