---
title: Akademy 2014 T-shirt
menu:
  "2014":
    weight: 48
    parent: program
---

<div style="float: right; padding: 1ex; margin: 1ex; width:
320px;text-align:center; "> 
<a href="/media/2014/diva-mockup.png">
 <img src="/media/2014/diva-mockup_wee.png" width="320"
  alt="Blue t-shirt with Akademy 2014 design" />
</a>
<figcaption>Mockup of Akademy t-shirt</figcaption>
</div>

Following on from last year we have the option for buying an official Akademy
2014 t-shirt. Attendees can pre-order a t-shirt, pick it up at Akademy and pay a
reduced price.

It's hard for the team to guess how many of each size shirt to order. Sometimes
people aren't able to get the right size because they have sold out, and we
often end up with leftovers of other sizes.

If you are definitely coming to Akademy 2014, you can pre-order the size of
t-shirt that you want. Then pick it up and pay during the conference.
Pre-ordered t-shirts have a reduced price of 300CZK (approx €10), normally
350CZK.

To [pre-order your t-shirt](https://conf.kde.org/Akademy2014), select the option
in the registration system.

**T-shirt pre-orders are open till 10th August**

If you don't know whether or not you will be at Akademy, or if you don't want to
pre-order, there will be t-shirts available to buy at the event for 350CZK.

The fitted style is suitable for some women, while others may prefer to wear the
unisex style.

The approximate sizes of the t-shirts are:

**Unisex**

![Table with unisex t-shirt dimensions](/media/2014/TU004.jpg)

**Fitted**

![Table with fitted t-shirt dimensions](/media/2014/TW040.jpg)
