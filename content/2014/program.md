---
title: Conference Overview
menu:
  "2014":
    weight: 42
    parent: program
---

<table border="1">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<tr>
<td>Fri 5th</td>
<td colspan="2"><center>
  <a href="http://ev.kde.org/generalassembly/">KDE e.V. AGM</a>
</center></td>
<td>
  <a href="/2014/pre-registration-event">Pre-registration &amp; Welcome</a>
</td>
</tr>
<tr>
<td>Sat 6th</td>
<td colspan="2"><center>
  <a href="https://conf.kde.org/en/Akademy2014/public/schedule/2014-09-06">
  Akademy Main Talks</a>
</center></td>
<td> </td>
</tr>
<tr>
<td>Sun 7th</td>
<td colspan="2"><center>
  <a href="https://conf.kde.org/en/Akademy2014/public/schedule/2014-09-07">
  Akademy Main Talks</a>
</center></td>
<td> </td>
</tr>
<tr>
<td>Mon 8th</td>
<td colspan="2"><center>
  <a href="https://community.kde.org/Akademy/2014/Monday">
  Akademy BoFs, Workshops &amp; Meetings</a>
</center></td>
<td> </td>
</tr>
<tr>
<td>Tue 9th</td>
<td colspan="2"><center>
  <a href="https://community.kde.org/Akademy/2014/Tuesday">
  Akademy BoFs, Workshops &amp; Meetings</a>
</center></td>
<td> </td>
</tr>
<tr>
<td>Wed 10th</td>
<td colspan="1"><center>
  <a href="https://community.kde.org/Akademy/2014/Wednesday">
  Akademy BoFs, Workshops &amp; Meetings</a>
</center></td>
<td colspan="2"><center>
  <a href="/2014/daytrip">Akademy Daytrip</a>
</center></td>
</tr>
<tr>
<td>Thu 11th</td>
<td colspan="2"><center>
  <a href="https://community.kde.org/Akademy/2014/Thursday">
  Akademy BoFs, Workshops &amp; Meetings</a>
</center></td>
<td> </td>
</tr>
<tr>
<td>Fri 12th</td>
<td colspan="2"><center>
  <a href="https://community.kde.org/Akademy/2014/Friday">
  Akademy BoFs, Workshops &amp; Meetings</a>
</center></td>
<td> </td>
</tr>
</table>
