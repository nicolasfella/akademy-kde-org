---
title: Local Transport
menu:
  "2014":
    weight: 54
    parent: travel
---

The public transport in Brno is provided by
[DPMB](http://www.dpmb.cz/default.aspx) which maintains 13 tram lines,
13 trolley-bus lines and 37 bus lines. During the night the public transport is
maintained by 11 night bus lines. If you want to use public transport, buy
tickets only for 100+101 zones! If you're staying in Brno for several days,
consider buying a 5-day ticket which costs CZK 250. There are several places
where you can buy it, but the best one for you is probably the main railway
station, where you can buy it at any counter (look for the international ones
because they are more likely to speak English).  
**SMS tickets**: DPMB also offers
[sms tickets](http://www.dpmb.cz/Default.aspx?seo=sms-jizdenky). You just need
to send the message to a phone number 90206. The code
for a 75-minute ticket (CZK 29) is BRNO, BRNOD for an all-day ticket (CZK 99),
and BRNO20 for 20-minute ticket (CZK 20). After sending the message, you'll
receive a ticket via sms within a few minutes. The service only works for Czech
SIM cards. If you don't have one, you can use a mobile app
[Sejf](http://www.sejf.cz/en/) which is
available in AppStore and Google Play.

- [Brno Local Transport
  Timetable](http://jizdnirady.idnes.cz/brno/spojeni/?lng=E)
- [Brno Local Transport Map](http://www.brno.planydopravy.cz/) 

From Main Railway Station to the Akademy Venue Take tram No.12, which serves the
bus station Zvonařka (tram stop Zvonařka), the bus station at the Grand Hotel
(tram stop Hlavní nádraží), and the main railway station (tram stop Hlavní
nádraží). From any of those, take tram No.12 to the end station Technologický
Park (about 20 minutes). Next, take bus No. 53 (towards Husitská) and get off at
the second stop
([Podnikatelská](http://jizdnirady.idnes.cz/brno/spojeni/?submit=true&f=Hlavn%C3%AD+n%C3%A1dra%C5%BE%C3%AD&t=Podnikatelsk%C3%A1&lng=Eng)).

If you get lost, consider yourself fortunate. That's when the fun begins...have
an adventure, enjoy Brno, meet local people, ask for help, learn čeština words
([be
prepared](http://www.loecsen.com/travel/0-en-67-2-45-free-lessons-czech.html)).

