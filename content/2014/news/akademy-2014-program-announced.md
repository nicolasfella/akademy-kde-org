---
title:  Akademy 2014 Program announced
---

The Akademy Program Committee is excited to announce the Akademy 2014 Program.
It is worth the wait! We waded through many high quality proposals and found
that it would take more than a week to include all the ones we like. However we
managed to bring together a concise and (still packed) schedule. (Full details
in the [dot
story](https://dot.kde.org/2014/08/04/akademy-2014-program-schedule-fast-fun-inspiring))
