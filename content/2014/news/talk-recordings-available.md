---
title:  Talk Recordings Available
---

Videos of all of the Akademy Talks are now available online to watch in your own
time.

You can access them from the Schedule:
https://akademy.kde.org/2014/conference-schedule From here, if you click on a
talk, you will see that there are links (the videos are here) and files (the
slides will be here if they have been uploaded).

A small note, due to technical issues with equipment at the venue, some of the
audio isn't great - we can only apologise about this.

