---
title: Akademy 2014 Call for Papers
---

The goal of the conference section of Akademy is to learn and teach new skills
and share our passion around what we're doing in KDE with each other.

For the sharing of ideas, experiences and state of things, we will have short
Fast Track sessions in a singe track section of Akademy. Teaching and sharing
technical details is done through longer sessions in the multi-track section of
Akademy.

If you think you have something important to present, please tell us about it.
If you know of someone else who should present, please nominate them. For more
details see the proposal guidelines and the [Call for
Papers](http://akademy.kde.org/2014/cfp). The
[Submission](https://conf.kde.org/en/Akademy2014/cfp/session/new)
Deadline is Sunday 18th May, 23:59:59 CEST.


