---
title: Are you going to Akademy 2014
---

If you are joining us for Akademy 2014 let others know by using the badges on
the [wiki](https://community.kde.org/Akademy/2014/badges)

