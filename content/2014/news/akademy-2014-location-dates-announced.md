---
title: Akademy 2014 Location & Dates announced
---


Today KDE announced the location of Akademy 2014 to be Brno, Czech Republic from
6th - 12th September.

More information is available in the story on the dot: [Akademy 2014 - Brno,
Czech Republic](http://dot.kde.org/2013/12/19/akademy-2014-brno-czech-republic)

