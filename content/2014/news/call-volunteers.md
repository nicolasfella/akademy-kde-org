---
title: Call For Volunteers
---

Are you attending Akademy 2014 in Brno? Help make it unforgettable, and get an
exclusive Akademy 2014 t-shirt in the bargain. Please consider [being a
volunteer](/2014/volunteer).

Help is needed for various tasks. Training is provided where needed or
requested.



