---
title: Pre-Registration Event
menu:
  "2014":
    weight: 45
    parent: program
---

For all attendees that arrive on Friday, 5 September or earlier that week there
is a chance to pick up their badges at the early registration get-together on
Friday, 5 September at the Red Hat office from 17:00 to 21:00 with food and
drinks provided. This way, you'll be also able to avoid the Saturday morning
crowd.

You can pick up your badge there, meet fellow Akademy attendees, have a drink,
food (vegetarian option available, hot and cold buffet) or play games (table
football etc.). This event is hosted by Red Hat in a brand new office opened
last month located next to the Technologicky Park tram stop (the address is
Purkyňova 111, don't worry it's not on any maps yet - see provided location
details bellow) and very close to the Palacky Hotel/Hostel and Akademy venue.
Food and drinks will be served during the whole event, distributed in the way
even late comers will not suffer from starvation.

![Red Hat office](/media/2014/rhoffice.jpg)

Address

[Red Hat Czech s.r.o.](http://www.redhat.com/en/global/czech-republic)

- Purkyňova 111, Brno: [49.2313564,
  16.5761999](http://www.openstreetmap.org/node/3000389014#map=17/49.23130/16.57771)

Note: Red Hat has more offices in the area, don't worry if you pass it in a
tram - continue to the Technologicky park stop.

Don't worry if you arrive later! There will be a registration/info desk open all
day from Saturday to Tuesday and also during the rest of the week where you can
pick up your badge, buy some KDE merchandising or get information about the
conference.

