---
title: Sponsors
menu:
  "2014":
    weight: 6
---

## Sponsorship Opportunities

**A big thank you to our sponsors who help to make this event happen!
There are still sponsorship opportunities.  
If you are interested, please see [Sponsoring Akademy 2014](/2014/sponsoring)
for more information, including valuable sponsor benefits.**


## Sponsors for Akademy 2014

### Silver


{{< sponsor homepage="https://blue-systems.com" img="/media/2014/bluesystems.png" >}}

<p id="bluesystems"><strong>Blue Systems</strong>
is a company investing in Free/Libre Computer Technologies. It sponsors several
KDE projects and distributions like Kubuntu and Netrunner. Their goal is to
offer solutions for people valuing freedom and choice.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://digia.com" img="/media/2014/digia.png" >}}

<p id="digia"><strong>Digia</strong> is responsible
for all worldwide Qt activities including product development, commercial
and open source licensing working together with the Qt Project under the
open governance model. Digia has in-depth Qt expertise and experience from
demanding mission-critical development projects and hundreds of in-house
certified Qt developers. Together with our licensing, support and services
capabilities, we operate with the mission to work closely with developers
to ensure that their Qt projects are deployed on time, within budget and
with a competitive advantage.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://www.redhat.com" img="/media/2014/redhat.png" >}}

<p id="redhat"><strong>Red Hat</strong> is the world's
leading provider of open source solutions, using a community-powered approach
to provide reliable and high-performing cloud, virtualization, storage, Linux,
and middleware technologies.</p>

{{< /sponsor >}}

### Bronze


{{< sponsor homepage="https://www.froglogic.com" img="/media/2014/froglogic.png" >}}

<p id="froglogic"><strong>froglogic GmbH</strong>
is a software company based in Hamburg, Germany. Their flagship product is
Squish, the market-leading automated cross-platform testing tool for GUI
applications based on Qt, QML, KDE, Java AWT/Swing and SWT/RCP, JavaFX,
Windows MFC and .NET, Mac OS X Carbon/Cocoa, iOS Cocoa Touch, Android
and for HTML/Ajax or Flex-based Web applications running in various Web
browsers.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://developers.google.com/open-source" img="/media/2014/google.png" >}}

<p id="google"><strong>Google</strong>
is a proud user and supporter of open source software and development
methodologies. Google contributes back to the Open Source community in
many ways, including more than 50 million lines of source code, programs
for students including Google Summer of Code and the Google Code-in
Contest, and support for a wide variety of projects, LUGs, and events
around the world.  Learn more at:</p>

<https://developers.google.com/open-source/>

{{< /sponsor >}}

{{< sponsor homepage="https://www.kdab.com" img="/media/2014/kdab.png" >}}

<p id="kdab"><strong>KDAB</strong>,
the world's largest independent source of Qt knowledge, provides services,
training and products for Qt development. Our engineers deliver peerless
software, providing expert help on any implementation detail or problem.
Market leaders in Qt Training, our trainers are all active developers,
ensuring that the knowledge delivered reflects real-world usage</p>

{{< /sponsor >}}

### Supporters


{{< sponsor homepage="https://openinventionnetwork.com" img="/media/2014/oin.png" >}}

<p id="oin"><strong>Open Invention Network (OIN)</strong>
is a collaborative enterprise that enables Linux through a patent non-aggression
community of licensees. OIN also supports Linux Defenders, which helps the open
source community defend itself against poor quality patents by crowdsourcing
defensive publications and prior art.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://owncloud.com" img="/media/2014/owncloud_0.png" >}}

<p id="owncloud"><strong>ownCloud</strong>
Based on the popular ownCloud open source file sync and share community
project, ownCloud Inc. was founded in 2011 to give corporate IT greater
control of their data and files — combining greater flexibility, openness
and extensibility with on premise servers and storage.</p>

{{< /sponsor >}}

### Patrons of KDE 

[KDE's Patrons](http://ev.kde.org/supporting-members.php) also
support the KDE Community through out the year.
