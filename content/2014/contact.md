---
title: Contact
menu:
  "2014":
    weight: 6
---

Akademy is organized by [KDE e.V](http://ev.kde.org/).

To contact the Akademy organizing team, please email [The Akademy
Team](mailto:akademy-team@kde.org). For other matters, contact the [KDE e.V.
Board](mailto:kde-ev-board@kde.org).

The IRC channel for the event is #akademy on freenode.net, you are welcome to
sit in the channel or just pop in and ask questions. You may not always get a
quick answer as the channel is not monitored constantly so please stick around
for an answer.
