---
title: Venue
menu:
  "2014":
    weight: 56
    parent: travel
---

![Image of the venue](/media/2014/Akademy2014Venue.jpg)

Akademy 2014 takes place at the [Faculty of Electrical Engineering and
Communication, Brno University of Technology in Brno, Czech
Republic](http://www.openstreetmap.org/?mlat=49.22671&mlon=16.57461#map=17/49.22671/16.57461)

The **Friday** AGM and talks on **Saturday and Sunday** will take place in
**Technická 12** building, while the **BoF sessions** will happen in **Technická
10** building just across the street.

## Address

[The Faculty of Electrical Engineering and Communication Brno University of
Technology](http://www.feec.vutbr.cz/fakulta/home.php.en)

- Technická 10, 616 00 Brno: [49.226123 N, 16.575583
  E](http://www.openstreetmap.org/?mlat=49.226123&mlon=16.575583#map=17/49.226123/16.575583)
- Technická 12, 616 00 Brno: [49.227288 N, 16.574580
  E](http://www.openstreetmap.org/?mlat=49.227288&mlon=16.574580#map=17/49.227288/16.574580)

More information can be found at [Venue wiki
page](https://community.kde.org/Akademy/2014/Venue).

![Image showing building locations for hostel, BOFs, AGM, main talks and bus
stop ](/media/2014/FEKT-plan-small.jpg)
