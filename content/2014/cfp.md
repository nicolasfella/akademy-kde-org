---
title: Akademy 2014 Call for Papers
menu:
  "2014":
    weight: 47
    parent: program
---

Akademy is the KDE Community conference. It is where we meet, discuss plans for
the future, get inspired, learn and get work done. If you are working on topics
relevant to KDE, this is your chance to present your work and ideas at the
Conference from the 6th - 12th of September in Brno, Czech Republic. The days
for talks are Saturday 6th and Sunday 7th of September. The rest of the week
will be BoFs, unconference sessions and workshops.

If you think you have something important to present, please tell us about it.
If you know of someone else who should present, please nominate them. The
proposal guidelines and a link to the Call for Papers are further down on this
page.

**Submission Deadline is Sunday 18th May, 23:59:59 CEST.**

## What we are looking for

The goal of the conference section of Akademy is to learn and teach new skills
and share our passion around what we're doing in KDE with each other.

**For the sharing of ideas, experiences and state of things, we will have Fast
Track sessions in a singe track section of Akademy. Teaching and sharing
technical details is done through longer sessions in the multi-track section of
Akademy.**

We are looking for session proposals in these areas:

- Porting to Qt 5 and KDE Frameworks 5
- Mobile/embedded applications, use cases and frameworks
- Overview of what is going on in various areas of the KDE community
- KDE in action: use cases of KDE technology in real life; be it mobile, desktop
  deployments and so on.
- Presentation of new applications; new features and functions in existing
  applications
- Collaboration between KDE, the Qt Project and other projects that use KDE or
  Qt
- Improving our governance and processes
- Increasing our reach through efforts such as accessibility, promotion,
  translation and localization

*Don't let this list restrict your ideas though. You can submit a proposal even
if it doesn't fit the list of topics as long as it is relevant to KDE.* To get
an idea of talks that were accepted previously, check out the program from
[Akademy 2013](http://akademy2013.kde.org/program).

## What we offer

Many creative, interesting KDE, Qt and Free Software contributors and supporters
- your attentive and receptive audience. An opportunity to present your
application, share ideas and best practices, or gain new contributors. A cordial
environment with people who want you to succeed. This is your chance to make a
big splash!

We have limited travel funds available, so please apply for them. Akademy travel
costs may be reimbursed based on the KDE e.V. [reimbursement
policy](http://ev.kde.org/rules/reimbursement_policy.php). You can apply via the
[registration system](https://akademy.kde.org/2014/register).

[Submit](https://conf.kde.org/en/Akademy2014/cfp/session/new) your proposal by
**Sunday 18th May, 23:59:59 CEST.**

Akademy attracts people from all over the world. For this reason, all talks are
in English. Please don't let this requirement stop you. Your ideas and
commitment are what your audience will want to know about.

- Fast Track sessions are 10 minutes long, followed by some time for Q&A. They
  are featured in a single morning track.
- Workshops and technical talks are 30 minutes to two hours. They will be
  scheduled concurrently with other sessions in the afternoon.

The multi-track part of Akademy has a tight schedule, so beginning and ending
times are enforced. If your presentation must be longer than the standard time,
please provide reasons why this is so and it will be considered.

Akademy is upbeat, but it is not frivolous. Help us see that you care about your
topic, your presentation and your audience. Typos, sloppy or all lower case
formatting and similar appearance oversights leave a bad impression and may
count against your proposal.

There is no need to overdo it. If it takes more than two paragraphs to get to
the point of your topic, it's too much and should be slimmed down. The quicker
you can make a good impression, the better.

We are looking for originality. Akademy is intended to move KDE forward. Having
the same people talking about the same things doesn't accomplish that goal.
Thus, we favour original and novel content. If you have presented on a topic
elsewhere, please add a new twist, new research, or recent development,
something unique. Of course, if your talk is plain awesome as is, go for that.

Everyone [submitting a
proposal](https://conf.kde.org/en/Akademy2014/cfp/session/new) will be notified
the week beginning the 2nd of June, whether or not their proposal is accepted
for the Akademy 2014 program.

All talks will be recorded and published on the Internet for free, along with a
copy of the slide deck, live demo or anything else associated with the
presentations. This benefits the larger KDE Community and those who can't make
it to Akademy. You will retain full ownership of your slides and other
materials, we request that you make your materials available explicitly under
the CC-BY license.

## Who we are

Meet the Akademy 2014 Program Committee:

**David Edmundson** is a long time KDE hacker, now working for Blue Systems. He
is the maintainer of KDE's instant messaging client and playing a lead role in
the development of Plasma Next.

**Frank Reininghaus** maintains the KDE file manager Dolphin. He works as a
theoretical physicist at the RWTH Aachen University and has been contributing to
Dolphin and other parts of KDE for 6 years.

**Jos Poortvliet** is part of the KDE Marketing Workgroup and active in KDE
promo, writing, coordinating, talking to press and representing KDE at
conferences. He works as community manager for ownCloud Inc, used to have a
similar role at SUSE Linux and has been a Free Software evangelist for over 10
years.

**Marta Rybczynska** can be usually found in embedded and kernel development.
She helps create the KDE Commit Digest and coordinates KDE Polish translation
efforts. As member of KDE e.V. board she's taking care of KDE finances.

**Peter Grasch** is a co-founder and current maintainer of KDE's open source
speech recognition system Simon. Currently, he is finishing up his master's
degree in computer science at the Graz University of Technology and working
towards a truly open speech recognition system capable of large vocabulary
dictation.

**Thomas Pfeiffer** is part of the KDE Usability and HIG groups as well as the
"Resident Designer" of KDE Telepathy and works on various KDE projects together
with developers and the Visual Design Group. He is currently doing a PhD in the
usabile security field at TU Darmstadt.

If you have any questions you can email the [Program
Committee](mailto:akademy-talks@kde.org)

*Thank you to the team from jsconf.eu for their exemplary Call for
Presentations, which we borrowed heavily from.*

