---
title: Sponsoring
menu:
  "2024":
    weight: 6
hideSponsors: true
---
By sponsoring this event, you will receive substantial exposure and reach top technical people from around the world. We expect around 200 attendees, including Free Software professionals and enthusiasts such as developers, translators, designers, and other community members, company representatives, journalists, students and educators from around the world. Akademy is an event where many bright people meet in an open and creative atmosphere, work hard, solve immediate challenges and come up with new ideas and solutions for the future.

**Your benefits as a sponsor of Akademy:**

* **Meet key people** – Meet key KDE and Qt contributors, upstream and downstream maintainers, and users of one of the world's foremost User Interface technology platforms.
* **Hire top talent** – KDE attracts some of the best technical people in the world. Sponsors have the opportunity to recruit or gain mindshare.
* **Meet other leading players** – Besides the KDE community, many other Free Software projects and companies participate in Akademy.
* **Get inspired** – Akademy provides an excellent environment for collecting feedback on new products, and for brainstorming new ideas.
* **Influence future product plans** – At Akademy, the KDE project sets direction for the upcoming months. This is your chance to be among the first to hear of those plans, to discuss and influence them.
* **Be visible** – A sponsorship gives you valuable promotional possibilities such as advertising, prominent talks, and international press coverage.
* **Encourage Free software technologies** – You support the development of new applications taking place in Akademy hackfests and intensive coding sessions.
* **Support Free software** – You will be recognized and enjoy publicity as a sponsor of the event. As importantly, you will also be supporting Free and Open Source Software, the KDE project, and the ideals that they are based on.
* **Be part of the KDE Community** – KDE is one of the largest Free and Open Source Software communities in the world. It is cooperative, committed and dynamic, fun-loving, creative and hard working. To people associated with KDE, it is the ultimate expression of community. You will have direct access to the magic sauce that makes KDE so effective.

Join us to help make this event a success! Please see the **[Sponsors Brochure](/media/2024/AkademySponsorsBrochure2024.pdf)** for full details of the options available. If you would like more information or want to discuss sponsoring, please contact the [Akademy Sponsoring Team](mailto:akademy-sponsoring@kde.org). Sponsors are critical to the success of Akademy.
