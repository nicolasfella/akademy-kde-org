---
title: Call for Participation
menu:
  "2024":
    parent: details
    weight: 2
hideSponsors: true
---
*[Akademy](https://akademy.kde.org/ "https\://akademy.kde.org/")* is the annual KDE Community conference. If you are working on topics relevant to KDE, this is your chance to present your work and ideas to the KDE community at large. It will be held at the Julius-Maximilians-Universität Würzburg. The talks will be held on Saturday, September 7th, and Sunday, September 8th. (CET daytime). The rest of the week (Monday - Thursday) will be dedicated to Birds-of-a-Feather meetings (BoFs), trainings, unconference sessions and workshops.

If you think you have something interesting to present, please tell us about it. If you know of someone else who should present, please encourage them to do so too.

**What We Are Looking For**

We are asking for talk proposals on topics relevant to the KDE community and technology such as:

* New people and organisations that are discovering KDE,
* Work towards *[KDE's goals](https://kde.org/goals/ "https\://kde.org/goals/")*: KDE For All, Sustainable Software, and Automate And Systematize Internal Processes,
* Giving people more digital freedom and autonomy with KDE,
* New technological developments,
* Guides on how to participate for new users, intermediates and experts,
* What's new after porting KDE Frameworks, Plasma and applications to Qt6
* Anything else that might interest the audience.

To get an idea of talks that were accepted, check out the program from previous years: ***[2023](https://conf.kde.org/event/5/contributions/ "https\://conf.kde.org/event/5/contributions/")***, *[2022](https://conf.kde.org/event/4/timetable/ "https\://conf.kde.org/event/4/timetable/")*, *[2021](https://conf.kde.org/event/1/timetable/#20210619 "https\://conf.kde.org/event/1/timetable/#20210619")*, *[2020](https://conf.kde.org/en/akademy2020/public/events "https\://conf.kde.org/en/akademy2020/public/events")*, *[2019](https://conf.kde.org/en/akademy2019/public/events "https\://conf.kde.org/en/akademy2019/public/events")*, *[2018](https://conf.kde.org/en/akademy2018/public/events "https\://conf.kde.org/en/akademy2018/public/events")*, and *[2017](https://conf.kde.org/en/akademy2017/public/events "https\://conf.kde.org/en/akademy2017/public/events")*.

**What We Offer**

Many creative and interested KDE, Qt and Free Software contributors and supporters — your attentive and receptive audience. An opportunity to present your application, share ideas and best practices, or gain new contributors. A cordial environment with people who want you to succeed. This is your chance to make a big splash.

If you would like advice before sending in a proposal, you can email *[akademy-talks@kde.org](mailto:akademy-talks@kde.org "mailto\:akademy-talks@kde.org")* to ask. The program committee can answer questions via mail or in a call.

**Submission**

Proposals require the following information:

* Title — the title of your session/presentation
* Content — a brief summary of your presentation
* Description — additional information not in the abstract; potential benefits of the topic for the audience
* A short bio — including anything that qualifies you to speak on your subject
* A headshot — your picture that we can use to help promote your talk as part of the final Akademy schedule (not essential but desirable)

**Please *submit* your proposals by May 24, 2024 23:59 UTC.**

*[Submit](https://conf.kde.org/event/6/abstracts/)*

Akademy attracts people from all over the world. For this reason, all talks are in English.

Not everyone has lots of experience in giving talks, we don't want this to put people off from submitting. If you would like help and advice with preparing your talk, please request this in the *Private Notes* section of your submission.

Your submissions can be in any of the following forms:

* Talks are 40 minutes
* Panels are 40 minutes
* Fast Track talks are 10 minutes long
* Lightning talks are 5 minutes long

Fast Track and Lightning talks are featured in a single track and do not include time for Q&A. Talks and Panels are encouraged to keep content to 30 minutes or less, leaving ample time for virtual, live Q&A with participants.

Workshops (BoFs) are included in the 2nd part of Akademy and can last for up to three hours.

Akademy has a lot of content so we will try hard to fit it into the schedule and make sure that presentations start and end on time.

If you think your presentation could work better with a time slot longer than the standard time slots given, please provide your reasons for this. Longer talks may also be turned into two or three sessions, a workshop (BoF), or special session later in the week, outside of the main conference tracks. In the case a workshop is best, we suggest a lightning-talk teaser to promote the workshop. You don't have to submit detailed proposals for the workshop parts yet. This will be done later. This call for participation focuses on the talks that take place during the weekend.

For Lightning talks, we will be collecting all of the presentations (we suggest no more than three slides per presentation) the month before the scheduled date. We strongly prefer pre-recorded Lightning talks.

Akademy is upbeat, but it is not frivolous. Help us see that you care about your topic, your presentation and your audience. Typos, sloppy or all-lowercase formatting and similar appearance oversights leave a bad impression and may count against your proposal. There's no need to overdo it. If it takes more than two paragraphs to get over the point of your topic, it's too much and should be slimmed down. The quicker you can make a good impression, both on the Program Committee and your audience, the better.

We are looking for originality. Akademy is intended to move KDE forward. Having the same people talking about the same things doesn't accomplish that goal. Thus, we favour original and novel content. If you have presented on a topic elsewhere, please add a new twist, new research, or recent development, something unique. Of course, if your talk is plain awesome as is, go for that.

Everyone submitting a proposal will be notified by the end of June 2024, as to whether or not their proposal was accepted for the Akademy 2024 Program.

The Akademy Team will provide assistance to you in preparing your talk and schedule a test-run to ensure your AV setup works. Help will also be available to provide any suitable webcam/microphone etc to ensure a good experience for attendees.

All talks will be recorded and published on the Internet for free, along with a copy of the slide deck, live demo or anything else associated with the presentations. This benefits the larger KDE Community and those who can't make it to Akademy. You will retain full ownership of your slides and other materials, we request that you make your materials available explicitly under the CC-BY license.

**The Akademy 2024 Program Committee**

This years committee are **Albert Astals Cid, Nicolas Fella, Harald Sitter, Johannes Zarl-Ziel and Neofytos Kolokotronis** *[akademy-talks@kde.org](mailto:akademy-talks@kde.org "mailto\:akademy-talks@kde.org")*
