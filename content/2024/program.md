---
title: Program Overview
menu:
  "2024":
    parent: details
    weight: 1
hideSponsors: true
---
<p>
 <div class="row py-5">
    <div class="col-lg-10 mx-auto">
      <div class="card rounded shadow border-0">
        <div class="card-body p-5 rounded">
          <div class="table-responsive">
<!--table border=1 style="border-top-style: solid; border-top-color: #ccc; border-top-width: 2px;"-->
<!--table id="program" style="width:100%" class="table table-striped table-bordered"-->
<table id="program" class="table table-striped  table-bordered">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<tr>
<td>Fri 6th</td>
<td colspan=2><center>&nbsp;<center></td>
<td><center><!--a href="https://akademy.kde.org/2024/welcome-event-friday"-->Welcome Event<!--/a--></center></td>
</tr>
<tr>
<td>Sat 7th</td>
<td colspan=2><center><!--a href="https://conf.kde.org/event/6/timetable/#20240907"-->Talks Day 1<!--/a--></center></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Sun 8th</td>
<td colspan=2><center><!--a href="https://conf.kde.org/event/6/timetable/#20240908"-->Talks Day 2<!--/a--></center></td>
<td><center><!--a href="https://akademy.kde.org/2024/social-event-sunday"-->Social Event<!--/a--></center></td>
</tr>
<tr>
<td>Mon 9th</td>
<td colspan=2><center><!--a href="https://community.kde.org/Akademy/2023/Monday"-->BoFs, Workshops & Meetings<!--/a--></center></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Tue 10th</td>
<td colspan=2><center><!--a href="https://community.kde.org/Akademy/2023/Tuesday"-->BoFs, Workshops & Meetings<!--/a--></center></td>
<td></td>
</tr>
<tr>
<td>Wed 11th</td>
<!--td><center><a href="https://community.kde.org/Akademy/2023/Wednesday">BoFs, Workshops & Meetings</a></center><--/td-->
<td colspan=3><center><!--a href="https://akademy.kde.org/2023/daytrip-wednesday"-->Daytrip<!--/a--></center></td>
</tr>
<tr>
<td>Thu 12th</td>
<td colspan=2><center><!--a href="https://community.kde.org/Akademy/2023/Thursday"-->BoFs, Workshops & Meetings<!--/a--></center></td>
<td>&nbsp;</td>
</tr>

</table>
          </div>
        </div>
      </div>
    </div>
  </div>
</p>
