---
title: Getting around Würzburg
menu:
  "2024":
    parent: travel
    weight: 3
hideSponsors: true
---

For going between the venue and the city, several bus lines (14, 114, 214, 10, 34, 7) can be used. For travel within the city (not to the venue), the tram is also an option. A day ticket for the bus costs 3,50€. There are also Taxis, bike rentals, and scooters available.