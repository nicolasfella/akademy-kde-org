---
layout: home

title: Akademy 2024
subtitle: Akademy is the annual world summit for [KDE Community](https://kde.org/) members, developers, translators, designers, and friends.
location:
  name: Würzburg, Germany and Online
datetime:
  name: 'Saturday 7th – Thursday 12th September'

community:
  title: Come Join Us
  description: Meet hundreds of KDE contributors and other actors in the open source world.

images:
  - /media/2022/akademy2022-groupphoto-med.jpg

menu:
  "2024":
    name: 2024
    weight: 1
  main:
    name: "2024"
    weight: 976
hideSponsors: true
scssFiles:
- /scss/2024/home.scss
---
