---
title: Accommodation
menu:
  "2015":
    weight: 32
    parent: travel
---

## Rialta

This year's recommended accommodation is [Aparthotel
Rialta](http://www.rialta.net/aparthotel/?lang=en).

Rialta is an apartment hotel located near the venue, with enough space for all
Akademy attendees. Rialta provides free bus transportation both to the city
centre and the venue.

**Single room:** 22.95 - 30 € per night

![Image of a single room](/media/2015/rialta-portada5.jpg)

**Twin room:** 36.98 - 40 € per night 45 € inc breakfast 	

![Image of a double room](/media/2015/residencia-doble-cocina2.jpg)

If you would like to share a room, but you don't have a roommate yet, there's a
wiki page for searching a roommate. You can search if someone is staying in the
same dates than you or you can announce yourself.

The following services are included in the price:

- Daily cleaning of the rooms
- En-suite bathroom with shower and toilet
- Towels and bedding provided
- TV in each room
- Free WiFi
- Free bus service, to the venue and city center
- Access to the swimming pool, the gym, and the sports facilities

## Kitchen in the rooms

![Image of Kitchen](/media/2015/cocina_indi.jpg)

Most Rialta rooms are equipped with kitchens, other rooms only have a microwave
oven. If you want to ensure that you're assigned a room with a kitchen, specify
it in the comments section while booking.


## Gym and swimming pool

![Image of gym](/media/2015/rialta-portada2.jpg)

![Image of swimming pool](/media/2015/rialta-portada1.jpg)
     
## Common rooms

Each floor has a common room with where you can relax with other attendees,
chatting or hacking together.

## Sponsored accommodation

**People requesting accommodation sponsorship should not book their own
accommodation**

## Booking process

In order to book a room and get the Akademy rates, use the [Rialta booking
page](https://web.archive.org/web/20150407231013/http://www.rialta.net/aparthotel/?lang=en)
and use the promotional code GPUL2015.

However depending on when you book your accommodation there may be other deals
available that are cheaper. After entering the dates in the form along with the
promotional code you will be presented with a choice of options it is important
to click on the *See all* link to see all price options.

![Display rates information image](/media/2015/rialta-rates.png)

Rialta reservation system might fail if you enter the promotion code, click
"View all" and then you do not select a GPUL2015 fare.

For that reason, the suggested way of booking is as follows:

1. Go to http://www.rialta.net/aparthotel/?lang=en
2. Enter the desired check-in date and the number of nights
3. Enter "GPUL2015" in promotional code and click "Check Rates and Book"
4. Click "View all" and check the fare that better suits you.
5. If the fare you choose is either a GPUL2015 or a GPUL2015AD fare, just select
the fare. If the fare which better suits you is neither a GPUL2015 nor a
GPUL2015AD fare, you must then empty the "Promotional code" in the booking page
header and click "Check Rates and Book" before selecting the fare.

You can choose if you want breakfast during reservation or check-in for an
additional cost this will depend on when you book or choose, the approx cost is
2.50 - 5€ per person per day. Please note that there are no other places where
breakfast is served near Rialta.

On credit card bills Rialta may show as *Fundacion Maria Jose J Rutis*

## How to arrive

### From the airport

- [Line 4051 Puerta Real - Aeropuerto. (1,5
  €)](http://autoscalpita.com/tarifas-horarios/asc-linea-4051-coruna-puerta-real-vilaboa-aeropuerto/)
- Get off in "Alfonso Molina Estación de Autobuses".
- Cross Avenida Alfonso Molina.
- Take the Rialta bus line which stops there or [Line
  24](http://www.tranviascoruna.com/lineas-y-horarios/?linea=24), get off in
"Nueva York, Rialta" and [walk 2
minutes](http://www.openstreetmap.org/directions?engine=graphhopper_foot&route=43.32364%2C-8.40786%3B43.32105%2C-8.40652#map=17/43.32223/-8.40783&layers=N).

### Other options

Should you prefer to stay in a different place, A Coruña has a wide range of
hotels you can choose from. You can use sites such as TripAdvisor or Trivago to
search for accommodation you like.

