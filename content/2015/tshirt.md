---
title: Pre-order Akademy 2015 T-shirt
---

<div style="float: right; padding: 1ex; margin: 1ex; width:
320px;text-align:center; "> 
<img src="/media/2015/mockup3_wee.png" width="320"/>
<br />
<figcaption>Mockup of Akademy t-shirt front</figcaption>
</div>

**Pre-ordering is no longer available. However there will be a small number of
t-shirts available for anyone who has not pre-ordered.**

Following on from previous years we have the option for buying an official
Akademy 2015 t-shirt. Attendees can pre-order a t-shirt, pick it up at Akademy
and pay a reduced price.

It's hard for the team to guess how many of each size shirt to order. Sometimes
people aren't able to get the right size because they have sold out, and we
often end up with leftovers of other sizes.

This year's design was created by Jens Reuterberg and is inspired by the [Tower
of Hercules](https://en.wikipedia.org/wiki/Tower_of_Hercules)

*Following some printing issues the design has had to be changed, sorry about
this*

If you are definitely coming to Akademy 2015, you can pre-order the size of
t-shirt that you want. Then pick it up and pay during the conference.
Pre-ordered t-shirts have a reduced price of 10€, normally 12€.

To [pre-order your t-shirt](https://conf.kde.org/Akademy2015), select the 
option in the registration system.

**T-shirt pre-orders are open till the 5th of July**

If you don't know whether or not you will be at Akademy, or if you don't want to
pre-order, there will be a limited number of t-shirts available to buy at the
event for 12€.

The fitted style is suitable for some women, while others may prefer to wear the
unisex style.

This years t-shirts are the same models as last year. The approximate sizes of
the t-shirts are:

Unisex

![Image and dimensions of unisex t-shirt](/media/2014/TU004.jpg)

*XXXL is only available in navy*

Fitted

![Image and dimensions of fitted t-shirt](/media/2014/TW040.jpg)
