---
title: News
menu:
  "2015":
    weight: 2
---


## Akademy 2015 videos available

Video recordings of the Akademy talks are now available in a low quality version
to enable them to be released quickly. Higher quality version will be available
later.

You can find these linked from the [talks
schedule](https://conf.kde.org/en/akademy2015/public/schedule/2015-07-25) or look
through the video files [directly](http://files.kde.org/akademy/2015/videos/)

Links to slides will also be added in the schedule as presenters upload them

[Read more](/2015/news/akademy-2015-videos-available)

## Daytrip

As the weather forecast predicts a 70% chance of rain on Wednesday afternoon,
the initial plan for daytrip (outdoor activities in the Tower of Hercules area
and beach) has been cancelled and a new daytrip is proposed. In the new plan, we
propose to visit Acquarium Finisterrae and Domus museum, if the weather is good
enough in the evening we might still visit Tower of Hercules area.

[Read more](/2015/news/daytrip)

## Pre-registration & Welcome event

As usual, we'll organize a Pre-registration / Welcome event so people who are
already at Coruña on the 24th can get their badge, greet old friends and meet
new ones.

The event will take place at Aparthotel Rialta. The official starting time is
20:00, however some Akademy attendees might already be at Rialta before that
hour, and don't worry if you cannot make it at 20:00, as we'll be there until
23:30.

During the evening, complimentary drinks and food will be available for all
attendees, and when the sun is gone, a traditional Galician ceremony will take
place.

[Read more](/2015/news/pre-registration-welcome-event)

## Call For Volunteers

Are you attending Akademy 2015 in A Coruña? Help make it unforgettable, and get
an exclusive Akademy 2015 t-shirt in the bargain. Please consider being a
volunteer.

[Read more](/2015/news/call-volunteers)

## Pre-order your Akademy 2015 t-shirt

Akademy 2015 t-shirt are now available to pre-order till the 5th of July

[Read more](/2015/news/pre-order-your-akademy-2015-t-shirt)

## Akademy 2015 Call for Papers has now been published and registration is open

The Akademy 2015 Call for Papers has now been published and registration is
open:

https://dot.kde.org/2015/03/04/akademy-2015-call-papers-and-registration-open

[Read more](/2015/news/akademy-2015-call-for-papers-and-registration-open) 

## Location and dates of Akademy 2015 announced

Akademy 2015 is announced to be taking place in A Coruña, Galicia, Spain from
the 25th to 31st July

For more details see the announcement on the dot: [A Coruña, Spain hosting
Akademy
2015](https://dot.kde.org/2015/01/20/akademy-2015-coru%C3%B1a-spain-25-31-july)

[Read more](/2015/news/location-and-dates-akademy-2015-announced)
