---
title: Program Overview
menu:
  "2015":
    weight: 32
    parent: program
---

<table border="1">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<tr>
<td>Thu 23th</td>
<td colspan="2"><center><a href="https://www.kde-espana.org/akademy-es2015/programa.php">Akademy-es</a></center></td>
<td> </td>
</tr>
<tr>
<td>Fri 24th</td>
<td colspan="2">
  <center>
    <a href="http://ev.kde.org/generalassembly/">KDE e.V. AGM</a>
    <br />
    <a href="https://www.kde-espana.org/akademy-es2015/programa.php">
    Akademy-es</a>
  </center>
</td>
<td>
<a href="https://akademy.kde.org/2015/news/pre-registration-welcome-event">
Pre-registration &amp; Welcome</a>
</td>
</tr>
<tr>
<td>Sat 25th</td>
<td colspan="2">
  <center>
  <a href="https://conf.kde.org/en/akademy2015/public/schedule/2015-07-25">
  Main Talks
  </a>
  </center>
</td>
<td> </td>
</tr>
<tr>
<td>Sun 26th</td>
<td colspan="2">
  <center>
    <a href="https://conf.kde.org/en/akademy2015/public/schedule/2015-07-26">
    Main Talks</a>
  </center>
</td>
<td> </td>
</tr>
<tr>
<td>Mon 27th</td>
<td colspan="2">
  <center>
    <a href="https://community.kde.org/Akademy/2015/Monday">
    BoFs, Workshops &amp; Meetings</a>
  </center>
</td>
<td> </td>
</tr>
<tr>
<td>Tue 28th</td>
<td colspan="2">
  <center>
    <a href="https://community.kde.org/Akademy/2015/Tuesday">
    BoFs, Workshops &amp; Meetings</a>
  </center>
</td>
<td>
  <center>
    <a href="/2015/social-event-terraza-party">
    Social event</a>
  </center>
</td>
</tr>
<tr>
<td>Wed 29th</td>
<td colspan="1">
  <center>
    <a href="https://community.kde.org/Akademy/2015/Wednesday">
    BoFs, Workshops &amp; Meetings</a>
  </center>
</td>
<td colspan="2">
  <center>
    <a href="/2015/news/daytrip">Daytrip</a>
  </center>
</td>
</tr>
<tr>
<td>Thu 30th</td>
<td colspan="2">
  <center>
    <a href="https://community.kde.org/Akademy/2015/Thursday">
    BoFs, Workshops &amp; Meetings</a>
  </center>
</td>
<td> </td>
</tr>
<tr>
<td>Fri 31st</td>
<td colspan="2">
  <center>
    <a href="https://community.kde.org/Akademy/2015/Friday">
    BoFs, Workshops &amp; Meetings</a>
  </center>
</td>
<td> </td>
</tr>
</table>

