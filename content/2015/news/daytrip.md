---
title: Daytrip
---

As the weather forecast predicts a 70% chance of rain on Wednesday afternoon,
the initial plan for daytrip (outdoor activities in the Tower of Hercules area
and beach) has been cancelled and a new daytrip is proposed. In the new plan, we
propose to visit Aquarium Finisterrae and Domus museum, if the weather is good
enough in the evening we might still visit Tower of Hercules area.

<div style="float: right; padding: 1ex; margin: 1ex; width:
320px;text-align:center; "> 
<img src="/media/2015/320px-Nautilus_en_el_Aquarium_Finisterrae.jpg" alt="aquarium"
width="320"/>
<br />
<a href="https://commons.wikimedia.org/wiki/File:Nautilus_en_el_Aquarium_Finisterrae.jpg">
<i>Aquarium Finisterrae, La Coruña by Marcus, GFDL</i></a>
</div>


The new daytrip will be defined as follows:
- We will begin after lunch, the official place for lunch will still be the
  venue cafeteria. If some people prefer they can go to the city center to have
lunch there and meet with the rest at the aquarium.
- For people who are having lunch in the faculty, if they're staying in Rialta
  they will be able to take the Rialta bus in an special service which will take
us to the Aquarium. This special service will depart at 16:30 from Rialta, stop
in the venue and do the "Recorrido turístico". If you're not staying on Rialta,
please come to the Infodesk on before Wednesday 13:00 and we'll check your
options.
- At 17:00, we propose to meet at the [Aquarium
  Finisterrae](http://mc2coruna.org/gl/aquarium.html) and visit it. **Please
  remember to bring your Akademy badge in order to have a free entrance**.
- After the aquarium, at about 18:30, if the weather is good enough to make a
  visit to the [Hercules Tower area](http://www.torredeherculesacoruna.com/) 
  enjoyable, we'll visit the **Hercules Tower area**, and you'll have the chance
  to climb the tower.
- If the weather makes a visit to the Hercules Tower area unenjoyable, we'll
  visit [Domus museum](http://mc2coruna.org/gl/domus.html).

