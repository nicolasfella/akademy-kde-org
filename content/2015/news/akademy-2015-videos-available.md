---
title: Akademy 2015 Videos Available
---

Video recordings of the Akademy talks are now available in a low quality version
to enable them to be released quickly. Higher quality version will be available
later.

You can find these linked from the [talks
schedule](https://conf.kde.org/en/akademy2015/public/schedule/2015-07-25) or
look through the video files
[directly](http://files.kde.org/akademy/2015/videos/)

Links to slides will also be added in the schedule as presenters upload them

