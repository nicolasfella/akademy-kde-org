---
title: Pre-registration & Welcome event
---

As usual, we'll organize a Pre-registration / Welcome event so people who are
already at Coruña on the 24th can get their badge, greet old friends and meet
new ones.

The event will take place at Aparthotel Rialta. The official starting time is
20:00, however some Akademy attendees might already be at Rialta before that
hour, and don't worry if you cannot make it at 20:00, as we'll be there until
23:30.

During the evening, complimentary drinks and food will be available for all
attendees, and when the sun is gone, a traditional Galician ceremony will take
place.

The address of Rialta is: Aparthotel Rialta Rua de Laxe, 122 15174 Culleredo A
Coruña

You can check [Rialta location at
OSM](http://osm.org/go/b9lBcj96B-?way=55179177).

In order to get there, you can take Rialta bus, bus 24 or take a taxi.

