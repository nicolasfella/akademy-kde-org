---
title: Akademy 2015 Call for Papers has now been published and registration is open
---


The Akademy 2015 Call for Papers has now been published and registration is
open:

[dot.kde.org/2015/03/04/akademy-2015-call-papers-and-registration-open](https://dot.kde.org/2015/03/04/akademy-2015-call-papers-and-registration-open)


