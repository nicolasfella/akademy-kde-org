---
title: How to get to the venue
menu:
  "2015":
    weight: 33
    parent: travel
---

- [From Rialta](#from-rialta)
- [From city center](#from-city-center)
- [From train station](#from-train-station)
- [From bus station](#from-bus-station)
- [By car](#by-car)
- [From the airport](#from-the-airport)

## From Rialta

- You can use Rialta bus which stops in front of Computer Science Faculty.
- You can walk (about 20 mins) [Show
  map](https://www.openstreetmap.org/directions?engine=graphhopper_foot&route=43.3207%2C-8.4061%3B43.3329%2C-8.4115#map=15/43.3266/-8.4096&layers=N)
- By car (about 10 mins) [Show
  map](https://www.openstreetmap.org/directions?engine=osrm_car&route=43.3207%2C-8.4061%3B43.3329%2C-8.4113#map=15/43.3237/-8.4060&layers=N)

## From city center

- You can take [E Universidade or UDC (depending on the
  bus)](http://www.tranviascoruna.com/en/lineas-y-horarios/?linea=ESPECIAL%20UNIVERSIDAD)
bus line which stops in front of Computer Science Faculty.  
Frequency on July: From 7:40 to 15:00 every 10 minutes. From 15:00 to 23:00
every 20 minutes.

- You can use line number 24 every 30 minutes, get off in “Antonio Insua, 34”
  and walk 10 minutes to the Faculty.  
  Frequency: From 7:00 to 22:35 every 20 minutes.

- You can use line number
  [5](http://www.tranviascoruna.com/lineas-y-horarios/?linea=5) too, get off
  in the last stop “Espacio Coruña” and walk 10 minutes to the Faculty.  
  Frequency: From 7:05 to 22:25 every 25 minutes.

Note: Frequency provided on working days and it can change. Please check
[Tranvías Coruña website](http://www.tranviascoruna.com/).

## From train station

- Come down to Avenida Alfonso Molina.
- Go around the gas station.
- Take the University line towards the campus. [Line E Universidade or UDC
  (depending on the
bus)](http://www.tranviascoruna.com/en/lineas-y-horarios/?linea=ESPECIAL%20UNIVERSIDAD).
- If it's Sunday, line E Universidade will not be available, in that case take
  line number 24, every 30 minutes, get off in “Antonio Insua, 34” and walk 10
minutes to the Faculty

[More train information](http://www.renfe.com/)

## From bus station

- Walk out by the main entrance.
- Cross Avenida Alfonso Molina.
- Take the University line towards the campus. [Line E Universidade or UDC
  (depending on the
bus)](http://www.tranviascoruna.com/en/lineas-y-horarios/?linea=ESPECIAL%20UNIVERSIDAD).
- If it's Sunday, line E Universidade will not be available, in that case take
  line number 24, every 30 minutes, get off in “Antonio Insua, 34” and walk 10
minutes to the Faculty

[More bus station
information](http://www.coruna.es/servlet/Satellite?itemID=1149055911348&itemTemplate=Portal-Entidad-Detalle&c=Page&pagename=Portal/Page/Portal-SubportadaSeccion&itemType=Entidad&cid=1134487388083&argIdioma=es)

## By car

- Take Avenida Alfonso Molina.
- Exit "Coliseum, Centro Comercial, Polígono Pocomaco".
- Take the first exit to the right without entering in the roundabout.
- Change to the right lane.
- Take the first exit and go up the little hill to the bridge which crooses the
  avenue.
- Take the roundabout to the left (3rd exit).
- Take the next roundabout to the right (1st exit).
- The Computer Science Faculty is in front of the athletics stadium.

[Map to arrive by
car](https://graphhopper.com/maps/?point=43.333919,-8.401837&point=Facultad%20Inform%C3%A1tica,%2015191,%20A%20Coru%C3%B1a,%20Spain)

## From the airport

- [Line 4051 Puerta Real -
  Aeropuerto](http://autoscalpita.com/tarifas-horarios/asc-linea-4051-coruna-puerta-real-vilaboa-aeropuerto/).
(1,5 €)
- Get off in "Alfonso Molina Estación de Autobuses".
- Cross Avenida Alfonso Molina.
- Take the University line towards the campus. [Line E Universidade or UDC
  (depending on the
bus)](http://www.tranviascoruna.com/en/lineas-y-horarios/?linea=ESPECIAL%20UNIVERSIDAD).

[More airport
information](http://www.aena.es/csee/Satellite/Aeropuerto-A-Coruna/es/)

