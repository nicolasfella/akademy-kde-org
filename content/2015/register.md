---
title: Register
menu:
  "2015":
    weight: 3
---

Anyone can attend Akademy for free. But registration is required in order to
attend the event. Please register soon so that we can plan accordingly.

Akademy 2015 registration is based on your
[identity.kde.org](https://identity.kde.org/) username and
password. In order to sign in here, you must first create an account there, if
you don't already have one. After you have completed the process of creating
your KDE Identity account, please continue to the [Akademy Registration
System](https://conf.kde.org/en/Akademy2015/).

## Travel Support

KDE e.V. offers [travel
support](http://ev.kde.org/rules/reimbursement_policy.php) for community members
for whom travelling to
Akademy is financially difficult. We'd like to be an inclusive community and not
let money get in the way of participation, so please fill in the *Apply for
travel cost support* form in the Registration System if your financial situation
is the main reason you might not attend.

**The application period for travel support has now closed**

