---
title: "Social event: Terraza Party"
menu:
  "2015":
    weight: 38
    parent: program
---

![Picture of Espacio Coruña Terrace](/media/2015/IMG_20150723_131901.jpg)

On Tuesday 28, at 21:00 we are going to have a social event at Espacio Coruña
Terrace.

All Akademy attendees are invited to attend on Tuesday evening a social event
under the Galician sky.

There will be nice music and free food for all attendees.

The places are limited, and we need to confirm the number of attendees by
tomorrow evening, so if you want to attend the event, please sign up at
info-desk before Sunday 26, 19:15.

If you will not be attending at Akademy on Sunday but you want to come to the
event, please send an email to akademy-team@kde.org.

The [Espacio Coruña is
located](http://www.openstreetmap.org/?mlat=43.33654&mlon=-8.41092#map=17/43.33654/-8.41092) 
near the conference venue and needs a walk around
the railways. In order to get there by bus you can take either Rialta bus and
get off at the Carrefour stop or line 5 to the Espacio Coruña stop.

