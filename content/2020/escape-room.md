---
title: Escape Room
menu:
  "2020":
    weight: 26
    parent: details
---



Although this year's Akademy is virtual, that is not stopping the fun! On the
line-up for Akademy's social events is Escape Games. There will be 6 games
times held in total during the conference. 


![Cartoon with a magnifying glass in a hand, image credit: 
vecteezy.com](/media/2020/escapegame_image_cred_vecteezy_0.png)
<figcaption>Image Credit: vecteezy.com</figcaption>

You can find the schedule here: 

<table class="table">
<tr>
<th>Day</th>
<th><center>Escape Room Session 1<br />06:30-08:30 UTC</center></th>
<th><center>Escape Room Session 2<br />20:30-22:30 UTC</center></th>
</tr>
<tr>
<td>Mon 7th</td>
<td><center>Mansion Impossible<br />(Low/Med. Difficulty)</center></td>
<td><center>CSI: Grounded<br />(Medium-High Difficulty)</center></td>
</tr>
<tr>
<td>Tues 8th</td>
<td><center>Mainstage Mayhem<br />(Low/Med. Difficulty)</center></td>
<td><center>CSI: Strangehold<br />(Medium Difficulty)</center></td>
</tr>
<tr>
<td>Wed 9th</td>
<td><center>Sherlock Holmes<br />(Medium Difficulty)</center></td>
<td><center>CSI: Mafia Murders<br />(Medium Difficulty)</center></td>
</tr>
</table>

You will be able to play in teams to solve the games. The game themes include
mystery and crime, and take place through different eras of time. Each game is
filled with riddles, puzzles, and codes to test your thinking abilities all
while enjoying the company of other Akademy Attendees!

**Akademy and its social events are free to attend, however to help us plan,
please [register](https://meet.kde.org/b/register/escape2020) for the
additional social event you would like to attend.**
