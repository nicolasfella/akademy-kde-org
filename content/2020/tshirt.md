---
title: Akademy 2020 T-shirt
menu:
  "2020":
    weight: 27
    parent: details
---

<div style="float:right; text-align: center; padding: 1ex; margin: 1ex;
width: 300px; border: 1px solid grey;">
<a href="/media/2020/akademy2020-mock-attendees.png">
  <img src="/media/2020/akademy2020-mock-attendees-wee.png" 
  alt="A black t-shirt with the akademy 2020 design on it">
</a>
<figcaption>
Mockup of Akademy 2020 t-shirt by Jens Reuterberg
</figcaption>
</div>

Following on from previous years we have the option for buying an official
Akademy 2020 T-shirt. As Akademy is virtual this year we need to ship the
T-shirts to you and this is why the price is a bit higher than in the past.

The T-shirts cost 25€, which includes worldwide delivery. To keep the price the
same for everyone [KDE e.V.](https://ev.kde.org/) is subsidising the higher
delivery cost for those outside Europe.

Orders are being handled by Freewear, the same company who has been printing
our Akademy T-shirts for years.

~~To ensure you receive your T-shirt to wear during Akademy please place your
order before 28 July if you are quick and order by 30th July you will still get
it in time. You will receive the T-shirt by the end of August/early September
so please ensure you are able to receive the parcel then at the address you
give us.~~

~~Orders made after 30th July will be shipped in the second half of
September.~~

This year's T-shirts are the same model as the last two years. We offer them in
both a unisex and fitted style.

Freewear also carry a range of other [KDE & FLOSS
merchandise](https://www.freewear.org/kde) which you can order separately.

How this years T-shirt was printed:

<video width="750" controls="1"> <source
src="https://files.kde.org/akademy/2020/Akademy_2020_T-shirt_screenprinting_process.webm"
type="video/webm"/> Your browser does not support the video tag.<br /> <a
href="https://files.kde.org/akademy/2020/Akademy_2020_T-shirt_screenprinting_process.webm">Download
how the Akademy 2020 t-shirt was made</a><br /> </video>
