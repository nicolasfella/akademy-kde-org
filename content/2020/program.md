---
title: Program Overview
menu:
  "2020":
    weight: 21
    parent: details
---


**Akademy is free to attend however to help us plan, please [register]({{< ref
"/" >}}2020/register)**

<table class="table">
<tr>
<th>Day</th>
<th><center>Session 1<br />0900-1200 UTC</center></th>
<th><center>Session 2<br />1600-2000 UTC</center></th>
</tr>
<td>Fri 4th</td>
<td><center><a href="https://conf.kde.org/en/akademy2020/public/schedule/1">Trainings</a></center></td>
<td><center><a href="https://conf.kde.org/en/akademy2020/public/schedule/1">Trainings</a></center></td>

<tr>
<td>Sat 5th</td>
<td><center><a href="https://conf.kde.org/en/akademy2020/public/schedule/2">Talks Day 1</a></center></td>
<td><center><a href=""></a><a href="https://conf.kde.org/en/akademy2020/public/schedule/2">Talks Day 1</a></center></td>
</tr>
<tr>
<td>Sun 6th</td>
<td><center><a href="https://conf.kde.org/en/akademy2020/public/schedule/3">Talks Day 2</a></center></td>
<td><center><a href="https://conf.kde.org/en/akademy2020/public/schedule/3">Talks Day 2</a></center></td>
</tr>
<tr>
<td>Mon 7th</td>
<td><center><a href="https://community.kde.org/Akademy/2020/Monday">BoFs, Workshops &amp; Meetings</a><br /><a href="https://ev.kde.org/generalassembly/">KDE eV AGM</a> (1100-1300)</center></td>
<td><center><a href="https://community.kde.org/Akademy/2020/Monday">BoFs, Workshops &amp; Meetings</a></center></td>
</tr>
<tr>
<td>Tue 8th</td>
<td><center><a href="https://community.kde.org/Akademy/2020/Tuesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center><a href="https://community.kde.org/Akademy/2020/Tuesday">BoFs, Workshops &amp; Meetings</a></center></td>
</tr>
<tr>
<td>Wed 9th</td>
<td><center><a href="https://community.kde.org/Akademy/2020/Wednesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center><a href="https://community.kde.org/Akademy/2020/Wednesday">BoFs, Workshops &amp; Meetings</a></center></td>
</tr>
<tr>
<td>Thu 10th</td>
<td><center><a href="https://community.kde.org/Akademy/2020/Thursday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center><a href="https://community.kde.org/Akademy/2020/Thursday">BoFs, Workshops &amp; Meetings</a></center></td>
</tr>
<tr>
<td>Fri 11th</td>
<td><center><a href="https://community.kde.org/Akademy/2020/Friday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center><a href="https://conf.kde.org/en/akademy2020/public/schedule/4">Talks Day 3</a></center></td>
</tr>
</table>

**Social events at this year's Akademy will take place Monday - Thursday.  
More Information: [Escape Rooms]({{< ref "/" >}}2020/escape-Room) and
[Pub Quiz]({{< ref "/" >}}2020/pub-quiz)**

![Qt logo with additional text Desktop Days](/media/2020/cropped-logoDates2.png)

[Qt Desktop Days](https://www.qtdesktopdays.com/) is running alongside
Akademy this year, it is free to 
[attend](https://www.qtdesktopdays.com/sign-up/)

<table>
<tbody>
<tr>
<th>Timezone</th>
<th width="100"><center>Session 1</center></th>
<th width="100"><center>Session 2</center></th>
</tr>
<tr>
<td>Pacific Daylight Time (Canada, Mexico, United States)</td>
<td><center>0200-0500</center></td>
<td><center>0900-1300</center></td>
</tr>
<tr>
<td>Mountain Daylight Time (Canada, Mexico, United States)</td>
<td><center>0300-0600</center></td>
<td><center>1000-1400</center></td>
</tr>
<tr>
<td>Central Daylight Time (Canada, Mexico, United States)</td>
<td><center>0400-0700</center></td>
<td><center>1100-1500</center></td>
</tr>
<tr>
<td>Eastern Daylight Time (Canada, Mexico, United States)</td>
<td><center>0500-0800</center></td>
<td><center>1200-1600</center></td>
</tr>
<tr>
<td>UTC -5 (Colombia, Equador, Peru)</td>
<td><center>0400-0700</center></td>
<td><center>1100-1500</center></td>
</tr>
<tr>
<td>UTC -4 (Bolivia, Chile, Paraguay, Venezuela)</td>
<td><center>0500-0800</center></td>
<td><center>1200-1600</center></td>
</tr>
<tr>
<td>UTC -3 (Argentina, Brazil, Uruguay)</td>
<td><center>0600-0900</center></td>
<td><center>1300-1700</center></td>
</tr>
<tr>
<td> <strong>UTC</strong></td>
<td> <strong><center>0900-1200</center></strong></td>
<td> <strong><center>1600-2000</center></strong></td>
</tr>
<tr>
<td>Western European Summer Time (Canary Islands, Faroe Islands, Ireland, Portugal, United Kingdom)</td>
<td><center>1000-1300</center></td>
<td><center>1700-2100</center></td>
</tr>
<tr>
<td>Central European Summer Time (Albania, Andorra, Austria, Belgium, Bosnia and Herzegovina, Croatia, Czech Republic, Denmark, France, Germany, Gibraltar, Hungary, Italy, Kosovo, Liechtenstein, Luxembourg, Malta, Monaco, Montenegro, Netherlands, North Macedonia, Norway, Poland, San Marino, Serbia, Slovakia, Slovenia, Spain, Sweden, Switzerland, Vatican)</td>
<td><center>1100-1400</center></td>
<td><center>1800-2200</center></td>
</tr>
<tr>
<td>Eastern European Summer Time (Belarus, Bulgaria, Cyprus, Estonia, Finland, Greece, Israel, Jordan, Latvia, Lebanon, Lithuania, Moldova, Romania, Russia (Kaliningrad), Syria, Ukraine)</td>
<td><center>1200-1500</center></td>
<td><center>1900-2300</center></td>
</tr>
<tr>
<td>UTC +5:30 (India, Sri Lanka)</td>
<td><center>1430-1730</center></td>
<td><center>2130-0130 (+1)</center></td>
</tr>
<tr>
<td>UTC +7 (Cambodia, Laos, Thai, Vietnam, Western Indonesia, Western Mongolia)</td>
<td><center>1600-1900</center></td>
<td><center>2300-0300 (+1)</center></td>
</tr>
<tr>
<td>UTC +8 (Brunei, Central Indonesia, China, Eastern Mongolia, Hong Kong, Macao, Malaysia, Philippines, Singapore, Taiwan)</td>
<td><center>1700-2000</center></td>
<td><center>0000 (+1)-0400 (+1)</center></td>
</tr>
<tr>
<td>UTC +9 (Eastern Indonesia, Japan, Korea, Timor-Leste)</td>
<td><center>1800-2100</center></td>
<td><center>0100 (+1)-0500 (+1)</center></td>
</tr>
</tbody>
</table>
