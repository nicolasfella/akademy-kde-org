---
title: Updates
menu:
  "2020":
    weight: 29
    parent: details
---

To keep up to date on what is happening with Akademy we recommend subscribing to
the [Akademy
Attendees](https://mail.kde.org/cgi-bin/mailman/listinfo/akademy-attendees) list

If you have any questions or just want to chat about the event the IRC channel
is [#akademy on
freenode](https://mail.kde.org/cgi-bin/mailman/listinfo/akademy-attendees) which
is also bridged to [Matrix](https://webchat.kde.org/#/room/#akademy:kde.org) and
[Telegram](https://telegram.me/KDEAkademy). You are welcome to sit in the
channel or just pop in and ask questions. You may not always get a quick answer
as the channel is not monitored constantly so please stick around for an answer.

