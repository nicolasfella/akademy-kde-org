---
title: Register
menu:
  "2020":
    weight: 30
    parent: details
---

Akademy is the annual world summit of KDE, one of the largest Free Software
communities in the world. It is a free, non-commercial event organized by the
KDE Community. Although Akademy is normally an in-person event, this year it
will be 100% online. We hope this may also attract new audience members. The
schedule timing for Akademy has taken into consideration multiple timezones so
you can be sure not to miss a thing. You'll be able to enjoy trainings, talks,
workshops, and more from the comfort of your desktop while still being able to
engage with other KDE Community members and interact with presenters

The [conference](/2020/program) is expected to draw hundreds of attendees from
the global KDE Community to discuss and plan the future of the Community and its
technology. Many participants from the broader free and open source software
community and users will also attend

**Akademy is free to attend however you need to register to reserve your space**

*If you are new to KDE & Akademy, you will first need a [KDE identity
Account](https://identity.kde.org/)*

<button name="button" class="button"
onclick="https://meet.kde.org/b/register/akademy2020">Register</button>

*Akademy 2020 has a [Code of Conduct](https://akademy.kde.org/2020/code-conduct)
to ensure the event is an enjoyable experience for everyone. As a Community, we
value and respect all people, regardless of gender identity, sexual orientation,
race, ability, shape, size or preferred desktop environment. We will not
tolerate vilification, abuse or harassment in any form*

