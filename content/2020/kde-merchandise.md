---
title: KDE Merchandise
menu:
  "2020":
    weight: 28
    parent: details
---



Normally at Akademy we have a wide range of KDE and related merchandise
available from Freewear to purchase at the info desk

With this year being online you can still order from
[freewear.org/kde](https://freewear.org/kde)

![Image of KDE branded shirts and pillows from
Freewear](/media/2020/freewear.png)
