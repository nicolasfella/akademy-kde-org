---
title: BBB User Guide
menu:
  "2020":
    weight: 23
    parent: details
---

The platform being used during Akademy 2020 is BigBlueButton (BBB). This
program is generally used for teachers and students but will be our virtual
home for the next 8 days.

If you wish to watch a tutorial video, this one from BBB is quite informative:
https://youtu.be/uYYnryIM0Uw

As an attendee to Akademy and an audience member in the talk rooms you will be
a viewer in the BBB room you enter. For this instance, please follow the steps
below to ensure you and others have the best viewing quality:
1. Sign in to BBB using your KDE identity account.
2. Enter the BBB room you are wishing to watch and enter the access code.
3. You will be prompted to join the audio conference with a microphone or a
   headphone listen only option. Please choose the "Listen Only" option for all
   talks you attend.
4. Write any questions for the speaker into the "Shared Notes." This is located
   on the left side of your screen. To avoid asking the same question multiple
   times, please check to make sure your question has not already been asked.

During Trainings and BoFs where you will likely participate in the
conversation, after entering the BBB room and providing the access code, join
the audio conference with your microphone. You will be prompted to conduct an
echo test. This may take a minute or 2 but you should be able to hear any sound
you make when you see the green thumbs up and red thumbs down images.

Tips to keep in mind:

In BBB, regardless of how you join the audio conference, you are able to set an
emoji status on your username. To do this left-click once on your name and
choose "Set Status" then choose which emoji you would like. For example, if you
are happy about a point the speaker is making you can set your emoji to
"Applause" or "Happy"

On the left side of your screen when in a BBB room you can find:
1. A Public Chat where you can communicate with all of the other Akademy
   attendees.
2. A section titled Shared Notes where you are encouraged to ask questions for
   the speaker.
3. A list of all of the other users in the BBB room.

You can use the chat as an anonymous user or by using the same credentials as
in webchat.kde.org . Register there first if you don't have an account.

When you are in a BBB room with your microphone on, please mute it any time you
are not speaking. This will help avoid any feedback and let other people speak
and be heard more easily.

