---
title: Register
menu:
  "2022":
    weight: 20
    parent: details
---

Akademy is the annual world summit of [KDE](http://www.kde.org/), one of the
largest Free Software communities in the world. It is a free, non-commercial
event organized by the KDE Community. Akademy 2022 is a hybrid event that takes
place in-person in Barcelona, Spain and virtually. We are really excited to meet
you again

Akademy 2022 is taking place from Saturday 1st - Friday 7th October, with a
Welcome event on the Friday evening before

The [conference]({{< ref "/" >}}program) is expected to draw hundreds of
attendees from the global KDE Community to discuss and plan the future of the
Community and its technology.  Many participants from the broader free and open
source software community and users will also attend

## Code of Conduct

Akademy 2022 has a [Code of Conduct]({{< ref "/" >}}conduct) to ensure the event
is an enjoyable experience for everyone. As a Community, we value and respect
all people, regardless of gender identity, sexual orientation, race, ability,
shape, size or preferred desktop environment. We will not tolerate vilification,
abuse or harassment in any form

## Covid-19 Mitigation

For the safety of everyone there, those attending in person will need to wear an
FFP2/3 mask at all times and in all areas of the venue where we are having
Akademy. We will be providing some free masks as well.

This is our baseline level that we will not be going below so that you can plan
whether or not you feel it is safe to attend. If circumstances worsen we may
also need to put in place additional measures that we will notify as early as
possible. There may be some venue staff in the common areas that we don't have
control over who may or may not be wearing masks.

**Akademy is free to attend however you need to register to reserve your space**

*If you are new to KDE & Akademy, you will first need a KDE identity Account*

[Register](https://conf.kde.org/event/4/registrations/10/)

After registering, to participate online join the Akademy space on KDE's Matrix
server

[Interact & Watch the
Talks](https://go.kde.org/matrix/#/#attend-akademy:kde.org)


