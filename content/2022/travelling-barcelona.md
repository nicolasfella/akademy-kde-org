---
title: Travelling to Barcelona
menu:
  "2022":
    parent: travel
    weight: 30
---

Before planning your trip if you are coming from outside Spain please check the
[Spanish Covid Certificate checker](https://www.spth.gob.es/spain-validator)

You can travel to Barcelona by airplane, boat, train, bus or car.

### By Airplane To Josep Tarradellas Barcelona-El Prat Airport (BCN)

Barcelona-El Prat Airport has connections to many cities of the world. For more
detailed information about the airport check Barcelona-El Prat Airport website.
Barcelona-El Prat airport is a best destination instead of Girona-Costa Brava or
Reus-Tarragona, because both airports are at 100 km away from Barcelona.

From the airport you can travel to the city:

#### By metro (subway)

L9 South line connects Airport (T1 and T2) with other metro
lines. From/to the airport you need an special metro ticket
(€5.15 single journey). Operating hours:
https://www.tmb.cat/en/barcelona/operating-hours-metro-bus

#### By train 

R2 Nord (North) line connects Airport (only T2) with other suburban
train and metro lines. There is a 24-hour free shuttle service between
terminals. Main train stations in Barcelona are: Barcelona-Sants and
Barcelona-Passeig de Gràcia.  More information:
https://www.aena.es/en/josep-tarradellas-barcelona-el-prat/getting-there/trains.html
Price: from Airport (T2) to Barcelona-Sants or Barcelona-Passeig de Gràcia you
need a 1 zone ticket (€2.40). But will be more convenient a T-casual card with
10 journeys (metro, bus, tram or train) from €11.35 for 1 zone (Barcelona
metropolitan area).

#### By bus

Aerobus A1 (from Airport T1 to Plaça Catalunya) and Aerobus A2 (from
Airport T2 to Plaça Catalunya). Price €5.90 one way Line 46 from Airport to
Plaça Espanya. 1 zone ticket (€2.40) or T-casual (10 journeys from €11.35 for 1
zone.  Line N19 (night bus) from Airport to Plaça de Catalunya (Ronda
Universitat). 

#### By taxi

The total cost depends on your destination (there are
day/night/weekend/holidays fares). There is also a supplement for airport and
luggage. A typical ride from/to Airport to/from city center can be estimated
between €40-€60.

### By Sea

There are Sea links between Barcelona and:

    - Balearic Islands (https://www.balearia.com/en,
      https://www.trasmediterranea.es/en/routes, https://www.gnv.it/en)
    - Italy: Genoa (Liguria) (https://www.gnv.it/en), Civitaveccia or Porto
      Torres (Sardegna) (https://www.grimaldi-lines.com/en/) 
    - Morocco: Nador or Tangier (https://www.gnv.it/en)

### By Train

Barcelona’s Main Train Station (“Barcelona-Sants”) is the main hub for all
national and international trains. There are high-speed trains to Madrid (2h30m)
and Paris (6h30m), but also to other national and international cities.

From there you can continue using subway lines or urban and suburban trains.

We suggest to take a look at Seat61 website (https://www.seat61.com/Spain.htm).

### By bus

Barcelona is reachable by quite some bus companies, like Flixbus and Eurolines.

We have a central bus station at Barcelona Nord Bus Station. There are bus
routes to Barcelona from many national cities and several european countries
(France, Andorra, Italy, Romania, Germany, Switzerland, Portugal, Ukraine) and
Morocco. More information: https://barcelonanord.barcelona/en/routes

### By Car

Inbound from:

    West, A-2 or AP-2 direction Barcelona
    North, take AP-7/E-15 direction Barcelona.  
    South, take AP-7/E-15 direction Barcelona.

There are tolls in some highways (C-16 and C-32).  Be aware of the speed limits.
Parking your car will be quite difficult, but there are some car parks where you
can park your car for a few euros.
