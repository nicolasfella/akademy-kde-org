---
title: Akademy 2022 T-shirt
menu:
  "2022":
    weight: 25
    parent: details
---

Following on from previous years we have the option for buying an official
Akademy 2022 T-shirt. As Akademy is hybrid this year we have two options
depending on if you are attending in person or online

![Image of Akademy t-shirt](/media/2022/2022-attendee-mock-med.png)

This year's T-shirts are the same model as the last few years, though last
years & this years are slightly wider than previously. We offer them in both a
unisex and fitted style. The approximate sizes of the t-shirts are 

#### Unisex
![Image with table sizes](/media/2022/tu03t-sizes.png)
*4XL and 5XL are not available in the same colour range unfortunately, they will
be in a similar colour*

#### Fitted
![Image with table sizes](/media/2022/tw04t-sizes.png)
*3XL is not available in the same colour range unfortunately, they will be in a
similar colour* 

## Barcelona: in person

In person attendees can pre-order t-shirts for collection in Barcelona for 15€
(paid when collecting)

Orders close 30th August

## Online attendees

Online T-shirts cost 25€, which includes worldwide delivery. To keep the price
the same for everyone KDE e.V. is subsidising the higher delivery cost for
those outside Europe

Orders for the online attendees are being handled directly by Freewear, the
same company who has been printing our Akademy T-shirts for years The payment
processing fees for KDE eV if you pay with credit-card are far cheaper than
using Paypal

Orders close 30th August

*Freewear also carry a range of other [KDE & FLOSS 
merchandise](https://www.freewear.org/kde) which you can
order separately online*
