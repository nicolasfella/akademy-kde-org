---
title: Getting around Barcelona
menu:
  "2022":
    parent: travel
    weight: 31
---

Barcelona is currently the second largest city in Spain, and the capital of
Catalonia, with an history over 4.000 years. Barcelona offers a large variety of
buildings, monuments, museums, beaches and so on...

Getting around Barcelona is very simple, and you are able to reach every part of
the city quickly.

The Akademy venue is in Vertex building, North Campus of Universitat Politècnica
de Catalunya (UPC). Nearest metro (L3 line) and tram (T1, T2, T3 lines) station
is Palau Reial, and walking distance of 850 m (about 13 minutes) to Vertex
building.

The public transport system in Barcelona consists of:

    Metro
    Tram
    Buses
    Trains

An overview plan of Metro+Tram lines:

![Barcelona metro line schematic](/media/2022/mapa_xarxa_de_metro.jpg)

There is an integrated transport system, with zones, but you only need tickets
for 1 zone if you remain in the metropolitan area (Barcelona and neighboring
municipalities). A single ticket cost €2.40 (non-integrated 1 zone). But we
recommend you the "T-casual" card, with individual 10 integrated journeys for
€7.95 (1 zone). This card allows passengers change lines or mode of
transportation (metro, bus, tram, train) during 75 minutes between the first and
last validation.

You can get the tickets
[online](https://www.tmb.cat/en/barcelona-fares-metro-bus/single-and-integrated)
or on vending machines at metro or tram stations
(cash or debit/credit card). There are no ticket sales on buses: you must buy
them online or on vending machines before enter the bus and then validate your
ticket with validation machines on board (entering by front door).

Metro finishes service at midnight from Monday to Thursday, at 02.00 on Fridays,
and there is a continous service on Satuday night until Sunday midnight. [More
information](https://www.tmb.cat/en/barcelona/operating-hours-metro-bus).

You also can use some
[night buses](http://www.ambmobilitat.cat/Principales/BusquedaNitBus.aspx).

## Taxi

Taxis in the Barcelona Metropolitan Area are all painted black and yellow

Taxis can be hailed anywhere on the street if the green light on top of the car
is on

You can also find taxis at the various [taxi
ranks](https://taxi.amb.cat/en/taxista/parades)

## Renting

Everywhere in Barcelona you are able to rent a:

- [Bike](https://ajuntament.barcelona.cat/bicicleta/en)
- Electric scooter: Some of the motosharing services in Barcelona: [Acciona](https://movilidad.acciona.com/en_ES/barcelona/), [Cooltra](https://www.cooltra.com/en/rent-by-minutes-hours/), [Yego](https://www.rideyego.com/barcelona), and many others.


