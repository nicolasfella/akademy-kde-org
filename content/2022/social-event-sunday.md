---
title: Social Event - Sunday
menu:
  "2022":
    weight: 23
    parent: details
---


Host: Masia Museu Serra
([https://www.masiamuseuserra.com/gastronomia/](https://www.masiamuseuserra.com/gastronomia/))
Date: Sunday 2nd October
Time: 20:30 - 24:00
Address: Passeig de la Campsa, 1
City: Cornellà de Llobregat

OpenStreetMap:
[https://www.openstreetmap.org/node/1280316534](https://www.openstreetmap.org/node/1280316534)

The closest Metro station is [L1 Avinguda de
Carrilet](https://www.openstreetmap.org/relation/6656994), from there just walk up
Rambla Marina a bit, then turn left on Carrer Major for less than 10 minutes and
you'll be there
[https://www.openstreetmap.org/directions?engine=graphhopper_foot&route=41.35929%2C2.10250%3B41.35869%2C2.09549](https://www.openstreetmap.org/directions?engine=graphhopper_foot&route=41.35929%2C2.10250%3B41.35869%2C2.09549)

The [Major-Av. Àlvarez de Castro bus
stop](https://www.openstreetmap.org/node/5218282007) is just in front of the restaurant. It
is served by the [M12 bus line](https://www.openstreetmap.org/relation/11797165) (amongst others), if you're staying in the
recommended accommodation (ILUNION Les Corts Spa) or coming the Venue it can be
useful though on Sundays it only goes every 30 minutes.

Remember that on Sundays metro closes at midnight, so plan your route back
either via Nit Bus or taxi if you plan to stay until late.

There's 3 Nit bus lines passing close by the venue
[N13](https://www.openstreetmap.org/relation/7716603#map=16/41.3600/2.0957), [N15](https://www.openstreetmap.org/relation/7731696#map=16/41.3599/2.1004) and [N2](https://www.openstreetmap.org/relation/7522242#map=17/41.35905/2.10077) (this last
one should be quite good if you're in the recommended accommodation (ILUNION Les
Corts Spa))

There's 2 taxi ranks close by the venue: one [in Rambla de Marina with Enric Prat
de la Riba](https://www.openstreetmap.org/node/7464544168) and the other close to the [RENFE station of L'Hospitalet](https://www.openstreetmap.org/node/3228619570).

The buffet includes: (unless you specified some special dietary restriction or
alergy on registration, please do not take the 'alternatives')

    GAZPACHO SHOT
    ZUCCHINI CREAM SHOT (alternative: gluten-free, lactose-free)

    POTATO OMELETTE TACOS
    TOAST WITH ROASTED VEGETABLES (alternative: gluten-free, lactose-free, vegan)

    HUMMUS AND FALAFEL
    HUMMUS AND VEGETARIAN MEATBALL (alternative: gluten-free, lactose-free)

    CHERRY TOMATO SKEWER WITH MOZZARELLA AND PESTO
    CHERRY TOMATO SKEWER WITH MOZZARELLA WITHOUT PESTO (alternative: without traces of nuts)
    CHERRY TOMATO SKEWER WITH DICED AVOCADO (alternative: vegan, lactose-free)

    NACHOS WITH MELTED CHEESE AND GUACAMOLE
    NACHOS WITH GUACAMOLE AND TOMATO (alternative: vegan)
    CORN TOAST WITH GUACAMOLE AND TOMATO (alternative: lactose-free)

    TARTLET WITH SOBRASADA AND BRIE
    CHEESE TARTLET WITH PEPPERS (alternative: vegan and vegetarian)
    BREAD WITHOUT GLUTEN WITH SOBRASSADA AND HONEY (alternative: gluten-free, lactose-free)

    TOAST WITH PATÉS WITH BALSAMIC VINEGAR REDUCTION
    TOAST WITH AUBERGINE PATÉ (alternative: vegetarian, vegan, lactose-free, without traces of nuts)
    BREAD WITHOUT GLUTEN WITH PATÉ (alternative: gluten-free)

    STEW CROQUETTES VEGETARIAN CROQUETTES (alternative: vegetarian)
    VEGAN GLUTEN-FREE CROQUETTE (alternative: vegan, gluten-free)

    DICED RED TUNA MACERATED WITH SOY AND GINGER
    DICED OF MACERATED TOFU (alternative: vegan, vegetarian)

    FILO PASTRY STUFFED WITH SHRIMP
    VEGETABLE TEMPURA (alternative: vegetarian, vegan, lactose-free)

    MINI BURGERS
    MINI CORN AREPAS STUFFED WITH SOY SHAWARMA (vegetarian, vegan, gluten free, lactose free)

There will some be tickets given for drinks, there will be the possibility of
paying for more drinks, if so desired.


