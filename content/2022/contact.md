---
title: Contact
menu:
  "2022":
    weight: 6
---

![Akademy logo](/media/2022/akademy_logo_simple_0.svg)

## Contact Us

We would like to hear from you. Akademy provides a unique opportunity to connect
with an amazing Free Software community. We would love to count on you to
participate in our various Akademy channels and ask any questions you may have
about the event.

<div class="container">
<div class="row">
<div class="col-md-12" style="box-shadow: 0px 0px;">
<div class="card-group" style="margin: 0px;padding: 0px;">
<div class="card"><img class="card-img-top w-100 d-block" src='{{< ref "/" >}}media/2022/ftirc-online.svg' style="height: 150.664px;text-align: center;padding: 15PX;" />
<div class="card-body">
<h4 class="card-title" style="text-align: center;"><a href="https://web.libera.chat/%23akademy">Libera</a></h4>
</div>
</div>
<div class="card"><img class="card-img-top w-100 d-block" src='{{< ref "/" >}}media/2022/Matrix_logo.svg' style="height: 150.289px;text-align: center;padding: 30PX;" />
<div class="card-body">
<h4 class="card-title" style="text-align: center;"><a href="https://go.kde.org/matrix/#/#akademy-attendees:kde.org">Matrix</a></h4>
</div>
</div>
</div>
</div>
</div>
</div>



You are welcome to ask any questions or just sit in the channel as our team
works on many aspects of the event. Our volunteers are located worldwide, they
will get back to you as soon as they are available. 

## More ways to get in touch

To contact the Akademy organizing team, please email The [Akademy
Team](mailto:akademy-team@kde.org). For other matters, contact the [KDE e.V.
Board](mailto:kde-ev-board@kde.org).


