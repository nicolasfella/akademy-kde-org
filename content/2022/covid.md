---
title: Covid-19 Mitigation
menu:
  "2022":
    weight: 19
    parent: details
---

**For the safety of everyone there, those attending in person will need to wear
an FFP2/3 mask at all times and in all areas of the venue where we are having
Akademy** *With the exception of in the cafeteria, we recommend eating outside
if possible. There may be venue staff who do not wear masks in common areas.*

We want to make sure everyone feels safe at Akademy. We will not be going below
this stated standard. Please plan accordingly. Additional measures may be
necessary should safety measure not be kept. We will work to notify everyone as
early as possible.

Wearing a mask helps protect people and reduces your chances of catching
COVID-19 or the flu. This could cause you to miss your opportunity to attend
Akademy.

**We will provide free masks of various designs both FFP2 & FFP3, hand sanitiser
and lateral flow tests to attendees**

If you have a medical condition that makes wearing a mask hard please contact
the [KDE e.V. Board](mailto:kde-ev-board@kde.org) in advance of attending
Akademy so that we can make this as simple as possible and have you on a list we
can consult, to help differentiate you from anti-vaxers etc. Thanks

## Welcome Event & Social Event

For both events, we recommend that our attendees use the outdoor areas.

## Daytrip

Masks are required while on the bus to and from the day-trip. We will not have
control over safety measures while at Montserrat. We ask that you are
responsible in caring for yourself and others.

## Public transport

Masks are currently required on [Barcelona public
transport](https://www.tmb.cat/en/-/novetat-cuidem-nos). Police patrol checking
for mask wearing and remove people from the vehicle who do not have a mask with
them

