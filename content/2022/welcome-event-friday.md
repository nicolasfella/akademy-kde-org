---
title: Welcome Event - Friday
menu:
  "2022":
    weight: 22
    parent: details
---



Place: Cafeteria Centre Cívic Sarrià
Date: September 30th
Time: 18:00 - 20:00
Address: Jardins de Vil·la Cecilia

The place is inside the gardens, follow the marker in this map [https://www.openstreetmap.org/?mlat=41.39377&mlon=2.12152#map=19/41.39377/2.12152](https://www.openstreetmap.org/?mlat=41.39377&mlon=2.12152#map=19/41.39377/2.12152)

Metro L3, Trams T1, T2, T3 - Maria Cristina (10min walk)
V3 bus - Santa Amèlia (just outside)

You will receive a token for a drink, there will be some finger food available.

You will be able to check in and collect your badge and get to know other attendees

