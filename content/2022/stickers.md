---
title: Submit a new KDE sticker design
menu:
  "2022":
    weight: 27
    parent: details
---

Did you enjoy the stickers included as a gift in previous year’s Akademy T-Shirt
orders? Do you have ideas for what some of the stickers could look like this
year?

Submit a new KDE sticker design to be given to in person attendees and included
with this year's online T-Shirt sales

**Send submissions to the [Akademy Team](mailto:akademy-team@kde.org) before 18th July**

Requirements/Notes

- Submissions should ideally be in SVG format, if not they need to be 350dpi+
- The design needs to be able to work at a small size (~50mm x 50mm) and be
designed using Free Software tools.
- Licensed under a suitable Free License.
- Colours printed may vary slightly from those seen on screen.
- The stickers on the sheet will have a white background and any shape can be
cut out (see the pink cut line around the Konqis as an example of certain
limitations).
- Some designs may benefit from having a bleed.

*Designs used by the Akademy Team will receive a free Akademy 2022 T-Shirt and
copies of the printed stickers. This is not a competition.*

Previous sticker sheets for reference:
![Image of first sticker sheet from 2021](/media/2022/232-321-max.png)
![Image of second sticker sheet from 2021](/media/2022/232-322.png)
