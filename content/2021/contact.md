---
title: Contact
menu:
  "2021":
    weight: 6
---

Akademy is organized by [KDE e.V](http://ev.kde.org/).

To contact the Akademy organizing team, please email [The Akademy
Team](mailto:akademy-team@kde.org). For other matters, contact the [KDE e.V.
Board](mailto:kde-ev-board@kde.org).

The IRC channel for the event is [#akademy on
Libera](https://web.libera.chat/#akademy) which is also bridged to
[Matrix](https://go.kde.org/matrix/#/#attend-akademy:kde.org) and
[Telegram](https://telegram.me/KDEAkademy). You are welcome to sit in the
channel or just pop in and ask questions. You may not always get a quick answer
as the channel is not monitored constantly so please stick around for an
answer.

The best way to make sure you keep up to date with all the news about Akademy
is by subscribing to the [Akademy Attendees mailing
list](https://mail.kde.org/mailman/listinfo/akademy-attendees).

