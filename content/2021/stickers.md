---
title: Submit a new KDE sticker design
menu:
  "2021":
    weight: 28
    parent: details
---

Did you enjoy the stickers included as a gift in last year’s Akademy T-Shirt
orders? Do you have ideas for what some of the stickers could look like this
year?

Submit a new KDE sticker design to be included with this year's T-Shirts

Send submissions to the Akademy Team before 26th April.

Requirements/Notes

- Submissions should ideally be in SVG format, if not they need to be 350dpi+
- The design needs to be able to work at a small size (~50mm²) and be designed
  using Free Software tools.
- Licensed under a suitable Free License.
- Colours printed may vary slightly from those seen on screen.
- The stickers on the sheet will have a white background and any shape can be
  cut out (see the pink cut line around the flying konqi as an example of
certain limitations).
- Some designs may benefit from having a bleed.

*Designs used will receive a free Akademy 2021 T-Shirt and copies of the printed
stickers. This is not a competition*.

Image of Akademy 2020's sticker sheet for reference:

![Image of sticker sheet from 2020](/media/2021/232-321-max.png)
