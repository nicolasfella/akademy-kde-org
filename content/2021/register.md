---
title: Register
menu:
  "2021":
    weight: 23
    parent: details
---

![Image with an astronaut on Mars and the words "AKADEMY
TWENTYONE"](/media/2021/registration_banner.jpg)

Akademy is the annual world summit of [KDE](http://www.kde.org/), one of the
largest Free Software communities in the world. It is a free, non-commercial
event organized by the KDE Community. Akademy will be 100% online again this
year. We hope this may also attract new audience members. The schedule timing
for Akademy has taken into consideration multiple timezones so you can be sure
not to miss a thing.  You'll be able to enjoy trainings, talks, workshops, and
more from the comfort of your desktop while still being able to engage with
other KDE Community members and interact with presenters

Akademy 2021 is taking place from Friday 18th - Friday 25th June

The [conference]({{< ref "/" >}}2021/program) is expected to draw hundreds of
attendees from the global KDE Community to discuss and plan the future of the
Community and its technology.  Many participants from the broader free and open
source software community and users will also attend

**Akademy is free to attend however you need to register to reserve your space**

*If you are new to KDE & Akademy, you will first need a [KDE identity
Account](https://identity.kde.org/)*

[Register](https://conf.kde.org/event/1/registrations/1/)

*Akademy 2021 has a [Code of Conduct]({{< ref "/" >}}2021/code-conduct) to
ensure the event is an enjoyable experience for everyone. As a Community, we
value and respect all people, regardless of gender identity, sexual orientation,
race, ability, shape, size or preferred desktop environment. We will not
tolerate vilification, abuse or harassment in any form*
