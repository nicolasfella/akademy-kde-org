---
title: Badges
menu:
  "2021":
    weight: 29
    parent: details
---

Akademy banners are here!

This year's official banners for people going to Akademy were created by Paul
Brown and Jens Reuterberg.

Feel free to use it in your blog posts, on your website, as a header image on
your social media profiles, or wherever you want to promote Akademy and let
people know you'll be attending!

This year you can choose from 3 different designs, these are available in size
840x206

**If you use any of the banners on your site or blog, you can link it to the
[Akademy website]({{< ref "/" >}}2021) and help us attract more visitors**

![Image of an astronaut on Mars with the words "AKADEMYTWENTYTWENTYONE"](/media/2021/Akademy_banner_Mars_840x206.png)

![Image of a bullhorn megaphone with the words "AKADEMYTWENTYTWENTYONE"](/media/2021/Akademy_banner_speaker_840x206.jpg)

![Image of a microphone with the words "AKADEMYTWENTYTWENTYONE"](/media/2021/Akademy_banner_mike_840x206.jpg.png)
