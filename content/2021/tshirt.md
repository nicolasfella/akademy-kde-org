---
title: Akademy 2021 T-shirt
menu:
  "2021":
    weight: 27
    parent: details
---

![Mockup of Akademy 2021 t-shirt
by Jens Reuterberg](/media/2021/akademy2021t-shirt_attendee_mock.png)

Following on from previous years we have the option for buying an official
Akademy 2021 T-shirt. As Akademy is virtual again this year we need to ship the
T-shirts to you and this is why the price is a bit higher than in the past.

**The T-shirts cost 25€, which includes worldwide delivery**. To keep the price
the same for everyone [KDE e.V.](https://ev.kde.org/) is subsidising the higher
delivery cost for those outside Europe

Orders are being handled by Freewear, the same company who has been printing our
Akademy T-shirts for years

To ensure you receive your T-shirt to wear during Akademy please place your
order before 20th May to get it in time. **Orders will be open again at the end
of Akademy from 25th June — 1st July for those who missed this**

This year's T-shirts are the same model as the last three years, though the
unisex are slightly wider. We offer them in both a unisex and fitted style. See
below for size details

*The payment processing fees for KDE eV if you pay with credit-card are far
cheaper than using paypal*

[Buy your T-shirt](https://www.freewear.org/akademy/)

Freewear also carry a range of other [KDE & FLOSS
merchandise](https://www.freewear.org/kde) which you can order separately

![Image of KDE branded merchandise](/media/2021/FW_Akademy_2021_banner_crop.png)

The approximate sizes of the t-shirts are:

- **Unisex**
![Image with t-shirt sizes in a table](/media/2021/TU03T.jpg)
*4XL and 5XL are not available in the same range unfortunately, they will be in
a similar yellow colour*

- **Fitted**
![Image with t-shirt sizes in a table](/media/2021/TW04T.jpg)
*3XL is not available in the same range unfortunately, they will be in a similar
yellow colour*
