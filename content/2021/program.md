---
title: Program Overview
menu:
  "2021":
    weight: 21
    parent: details
---

![Image with a clock above the words "SCHEDULE AKADEMY
TWENTYTWO"](/media/2021/schedule_banner_0.jpg)

Akademy 2021 is on from Friday 18th - Friday 25th June

**Akademy is free to attend however to help us plan, please [register]({{< ref
"/" >}}2021/register)**

<table class="table">
<tr>
<th><center>Day<br />UTC</center></th>
<th><center>Session 1<br />08:00-12:00 UTC</center></th>
<th><center>Break<br />12:20-17:00 UTC</center></th>
<th><center>Session 2<br />17:00-21:00 UTC</center></th>
</tr>
<tr>
<td>Fri 18th</td>
<td><center><a href="https://conf.kde.org/event/1/timetable/#20210618">Trainings</a></center></td>
<td><center> </center></td>
<td><center><a href="https://conf.kde.org/event/1/timetable/#20210618">Trainings</a></center></td>
</tr>
<tr>
<td>Sat 19th</td>
<td><center><a href="https://conf.kde.org/event/1/timetable/#20210619">Talks Day 1</a></center></td>
<td><center><a href="https://meet.kde.org/b/akademy-officehours-collabora">Collabora Office Hour</a> (12:30)</center></td>
<td><center><a href="https://conf.kde.org/event/1/timetable/#20210619">Talks Day 1</a></center></td>
</tr>
<tr>
<td>Sun 20th</td>
<td><center><a href="https://conf.kde.org/event/1/timetable/#20210620">Talks Day 2</a></center></td>
<td><center> </center></td>
<td><center><a href="https://conf.kde.org/event/1/timetable/#20210620">Talks Day 2</a></center></td>
</tr>
<tr>
<td>Mon 21st</td>
<td><center><a href="https://community.kde.org/Akademy/2021/Monday">BoFs, Workshops &amp; Meetings</a><br /><a href="https://ev.kde.org/generalassembly/">KDE e.V. AGM</a> (11:00-13:00)</center></td>
<td><center> </center></td>
<td><center><a href="https://community.kde.org/Akademy/2021/Monday">BoFs, Workshops &amp; Meetings</a></center></td>
</tr>
<tr>
<td>Tue 22nd</td>
<td><center><a href="https://community.kde.org/Akademy/2021/Tuesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center><a href="https://wiki.qt.io/Qt_Contributors_Summit_2021_-_Program">Qt Contributors Summit</a></center></td>
<td><center><a href="https://community.kde.org/Akademy/2021/Tuesday">BoFs, Workshops &amp; Meetings</a></center></td>
</tr>
<tr>
<td>Wed 23rd</td>
<td><center><a href="https://community.kde.org/Akademy/2021/Wednesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center><a href="https://wiki.qt.io/Qt_Contributors_Summit_2021_-_Program">Qt Contributors Summit</a></center></td>
<td><center><a href="https://community.kde.org/Akademy/2021/Wednesday">BoFs, Workshops &amp; Meetings</a></center></td>
</tr>
<tr>
<td>Thu 24th</td>
<td><center><a href="https://community.kde.org/Akademy/2021/Thursday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center><a href="https://meet.kde.org/b/akademy-officehours-fedora">Fedora Office Hour</a> (12:00)<br /><a href="https://meet.kde.org/b/akademy-officehours-mbition">MBition Office Hour</a> (13:00)<br /><a href="https://meet.kde.org/b/akademy-officehours-gitlab">GitLab Office Hour</a> (14:00)</center></td>
<td><center><a href="https://community.kde.org/Akademy/2021/Thursday">BoFs, Workshops &amp; Meetings</a></center></td>
</tr>
<tr>
<td>Fri 25th</td>
<td><center><a href="https://community.kde.org/Akademy/2021/Friday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center> </center></td>
<td><center><a href="https://conf.kde.org/event/1/timetable/#20210625">Talks Day 3</a></center></td>
</tr>
</table>


<table>
<tbody>
<tr>
<th>Timezone</th>
<th width="100"><center>Session 1</center></th>
<th width="100"><center>Session 2</center></th>
</tr>
<tr>
<td>Pacific Daylight Time (Canada, Mexico, United States)</td>
<td><center>0100-0500</center></td>
<td><center>1000-1400</center></td>
</tr>
<tr>
<td>Mountain Daylight Time (Canada, Mexico, United States)</td>
<td><center>0200-0600</center></td>
<td><center>1100-1500</center></td>
</tr>
<tr>
<td>Central Daylight Time (Canada, Mexico, United States)</td>
<td><center>0300-0700</center></td>
<td><center>1200-1600</center></td>
</tr>
<tr>
<td>Eastern Daylight Time (Canada, Mexico, United States)</td>
<td><center>0400-0800</center></td>
<td><center>1300-1700</center></td>
</tr>
<tr>
<td>UTC -5 (Colombia, Equador, Peru)</td>
<td><center>0300-0700</center></td>
<td><center>1200-1600</center></td>
</tr>
<tr>
<td>UTC -4 (Bolivia, Chile, Paraguay, Venezuela)</td>
<td><center>0400-0800</center></td>
<td><center>1300-1700</center></td>
</tr>
<tr>
<td>UTC -3 (Argentina, Brazil, Uruguay)</td>
<td><center>0500-0900</center></td>
<td><center>1400-1800</center></td>
</tr>
<tr>
<td> <strong>UTC</strong></td>
<td> <strong><center>0800-1200</center></strong></td>
<td> <strong><center>1700-2100</center></strong></td>
</tr>
<tr>
<td>Western European Summer Time (Canary Islands, Faroe Islands, Ireland, Portugal, United Kingdom)</td>
<td><center>0900-1300</center></td>
<td><center>1800-2200</center></td>
</tr>
<tr>
<td>Central European Summer Time (Albania, Andorra, Austria, Belgium, Bosnia and Herzegovina, Croatia, Czech Republic, Denmark, France, Germany, Gibraltar, Hungary, Italy, Kosovo, Liechtenstein, Luxembourg, Malta, Monaco, Montenegro, Netherlands, North Macedonia, Norway, Poland, San Marino, Serbia, Slovakia, Slovenia, Spain, Sweden, Switzerland, Vatican)</td>
<td><center>1000-1400</center></td>
<td><center>1900-2300</center></td>
</tr>
<tr>
<td>Eastern European Summer Time (Belarus, Bulgaria, Cyprus, Estonia, Finland, Greece, Israel, Jordan, Latvia, Lebanon, Lithuania, Moldova, Romania, Russia (Kaliningrad), Syria, Ukraine)</td>
<td><center>1100-1500</center></td>
<td><center>2000-0000 (+1)</center></td>
</tr>
<tr>
<td>UTC +5:30 (India, Sri Lanka)</td>
<td><center>1330-1730</center></td>
<td><center>2230-0230 (+1)</center></td>
</tr>
<tr>
<td>UTC +7 (Cambodia, Laos, Thai, Vietnam, Western Indonesia, Western Mongolia)</td>
<td><center>1500-1900</center></td>
<td><center>0000 (+1)-0400 (+1)</center></td>
</tr>
<tr>
<td>UTC +8 (Brunei, Central Indonesia, China, Eastern Mongolia, Hong Kong, Macao, Malaysia, Philippines, Singapore, Taiwan)</td>
<td><center>1600-2000</center></td>
<td><center>0100 (+1)-0500 (+1)</center></td>
</tr>
<tr>
<td>UTC +9 (Eastern Indonesia, Japan, Korea, Timor-Leste)</td>
<td><center>1700-2100</center></td>
<td><center>0200 (+1)-0600 (+1)</center></td>
</tr>
</tbody>
</table>
