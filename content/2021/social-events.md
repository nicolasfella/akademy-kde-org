---
title: Social Events
menu:
  "2021":
    weight: 24
    parent: details
---

We have a great social program lined up for you throughout the conference. The
social program is kindly sponsored by MBition.

![Mbition logo](/media/2021/mbition_1.png)

### Origami

Wednesday 23rd: 7:00 & 21:00 UTC
Did you always want to learn Origami, but never
had the opportunity? You're in luck! Sensei Tomaz Canabrava will show you what
the art of folding paper is all about. To join in the fun, be sure to prepare
three pieces of paper beforehand. If you want, set up your webcam so that it
points at your hands and show everyone how you're progressing!

Link: https://meet.kde.org/b/akademy-social-games

### Pub Quiz

Thursday 24th: 21:00 UTC
Welcome to the Pub Quiz, where the rules are arbitrary
and the points don't matter. We'll be looking at photos of Akademy attendees
desks and work-spaces, trying to figure out the owners, voting for the nicest
setup and generally having fun with the pictures you submitted. Want to join in
the fun? Use this link to submit a photo of your desk/workspace:
https://share.kde.org/s/XtARaQigbHFwMms . Please name the file as your name or
nickname, so we know which one is yours!

Link: https://meet.kde.org/b/akademy-social-games

### Games Time!

Monday 21st: 7:00 & 21:00 UTC
Tuesday 22nd: 7:00 & 21:00 UTC
Thursday 24th: 7:00 UTC
Friday 25th: 7:00 UTC

Join your fellow Akademy attendees in 3 games in which you will compete against
them, but also have a chance to know each other.

This year we'll be playing: 

- **SuperTuxKart**

A competitive open-source racing game, featuring characters from many FOSS
projects. To prepare for the game, register a user at
https://online.supertuxkart.net/register.php (unless you already have an
account). Then install supertuxkart (either from your distribution or from
https://supertuxkart.net/Download), run it and go to Online->Login and enter the
user/password. Server name and password for Akademy attendees will be shared in
the BBB room. Once you have that you go to Online->Global Networking->Find
Server.

Link: https://meet.kde.org/b/akademy-social-games

- **skribbl.io**

One person draws a prompt, everyone else needs to guess what it is! Gather
points by being the fastest to guess, and try for the highest score at the end
of the round. During a round, everyone gets the chance to draw. Since this is a
browser game, no preparations needed. A link to a lobby for Akademy attendees
will be shared in the BBB room.

Link: https://meet.kde.org/b/akademy-social-games-2

- **garticphone.com**

Don't care about first places and points? Try this game, where you try your best
to draw a picture of a given prompt. After you're done, some other player sees
your picture (but not the prompt) and tries to describe it. Likewise, you will
see someones picture, and your job is to describe it the best you can for the
next person. Then a new round starts in which the descriptions are the prompts
for the next player. Each following round has less and less time, so the
pictures and descriptions become chaotic very fast! When all the rounds are
done, sit back and enjoy a presentation of how each idea evolved over the
iterations. There are no winners or losers here, just fun! A link to a lobby for
Akademy attendees will be shared in the BBB room.

Link: https://meet.kde.org/b/akademy-social-games-3
