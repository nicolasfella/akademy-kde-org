---
title: Call for Papers
menu:
  "2013":
    weight: 52
    parent: program
---

**The Call for Papers is now closed.**

Akademy is the KDE Community conference. If you are working on topics relevant
to KDE or Qt, this is your chance to present your work and ideas at the
Conference from 13-19 July in Bilbao. The days for talks are Saturday and
Sunday, 13 and 14 July. The rest of the week will be
[unconference](http://en.wikipedia.org/wiki/Unconference) sessions and
workshops.

If you think you have something important to present, please tell us about it.
If you know of someone else who should present, please nominate them. The
proposal guidelines and a link to the Call for Papers are further down on this
page.

## What we are looking for

We are asking for talk proposals on topics relevant to KDE and Qt including:

- Collaboration between KDE, the Qt Project and other projects that use KDE or
  Qt
- Porting to Qt 5 and KDE Frameworks 5
- Mobile/embedded applications, use cases and frameworks
- Increasing our reach through efforts such as accessibility, promotion,
  translation and localization
- Presentation of new applications; new features and functions in existing
  applications
- Improving our governance and processes

*Don't let this list restrict your ideas though. You can submit a proposal even
if it doesn't fit the list of topics as long as it is relevant to KDE. To get an
idea of talks that were accepted previously, check out the program [from Akademy
2012](/2012/program).*

## What we offer

Many creative, interesting KDE, Qt and Free Software contributors and supporters
- your attentive and receptive audience. An opportunity to present your
application, share ideas and best practices, or gain new contributors. A cordial
environment with people who want you to succeed. This is your chance to make a
big splash!

We have limited travel funds available, so please apply for them. Akademy travel
costs may be reimbursed based on the [KDE e.V. reimbursement
policy](http://ev.kde.org/rules/reimbursement_policy.php). An online
request form will be available soon.

## Who we are

Meet the [Akademy 2013 Program Committee](/2013/program-committee).

## Proposal guidelines

Provide the following information on [the proposal
form](https://conf.kde.org/en/Akademy2013/cfp):

- Title—the title of your session/presentation
- Abstract—a brief summary of your presentation
- Description—information about your topic and its potential benefits for the
  audience
- A short bio—including anything that qualifies you to speak on your subject

Submit your proposal by **March 15th, 23:59:59 CET**.

Akademy attracts people from all over the world. For this reason, **all talks
are in English**. Please don't let this requirement stop you. Your ideas and
commitment are what your audience will want to know about.

Presentations are 30 minutes long, including time for Q&A. Lightning talks run
for 7 minutes. Akademy has a tight schedule, so beginning and ending times are
enforced. If your presentation must be longer than the standard time, please
provide reasons why this is so.

Akademy is upbeat, but it is not frivolous. Help us see that you care about your
topic, your presentation and your audience. Typos, sloppy or all-lowercase
formatting and similar appearance oversights leave a bad impression and may
count against your proposal.

There's no need to overdo it. If it takes more than two paragraphs to get to the
point of your topic, it's too much and should be slimmed down. The quicker you
can make a good impression, the better.

We are looking for originality. Akademy is intended to move KDE forward. Having
the same people talking about the same things doesn't accomplish that goal.
Thus, we favor original and novel content. If you have presented on a topic
elsewhere, please add a new twist, new research, or recent development,
something unique. Of course, if your talk is plain awesome as is, go for that.

Everyone submitting a proposal will be notified on April 15th, whether or not
their proposal is accepted for the Akademy 2013 program.

## Rights

All talks will be recorded and published on the Internet for free, along with a
copy of the slide deck, live demo or anything else associated with the
presentations. This benefits the larger KDE Community and those who can’t make
it to Akademy. If you are uncomfortable in any way with this, please tell the
[Program Committee](mailto:Akademy-talks@kde.org), and we will work things out
with you.

You will retain full ownership of your slides, recording and other materials, we
request that you to make your materials available explicitly under the [CC-BY-SA
license](https://creativecommons.org/licenses/by-sa/2.0/). ([Contact 
us](mailto:Akademy-talks@kde.org) if this won't work for any reason.)

## Thanks

Thank you to the team from [jsconf.eu](http://2012.jsconf.eu/) for their exemplary
[Call for Presentations](https://github.com/jsconf/2012.jsconf.eu/blob/gh-pages/_posts/2012-06-19-call-for-presentations.md),
which we borrowed heavily from.

