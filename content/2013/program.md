---
title: Program Overview
menu:
  "2013":
    weight: 42
    parent: program
---

<table border="1">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<tr>
<td>Thu 11th</td>
<td colspan="2"><center>
<a href="http://kde-espana.es/akademy-es2013">Akademy-ES</a>
</center></td>
<td> </td>
</tr>
<tr>
<td>Fri 12th</td>
<td colspan="2"><center>
<a href="http://kde-espana.es/akademy-es2013">Akademy-ES</a><br />
<a href="http://ev.kde.org/generalassembly/">KDE e.V. AGM</a>
</center></td>
<td><a href="../pre-reg">Pre-registration &amp; Welcome</a></td>
</tr>
<tr>
<td>Sat 13th</td>
<td colspan="2"><center>
<a href="https://conf.kde.org/en/Akademy2013/public/schedule/2013-07-13">
Akademy Main Talks</a>
</center></td>
<td colspan="1"><center>
<a href="../akademy-party-saturday">Party</a>
</center></td>
</tr>
<tr>
<td>Sun 14th</td>
<td colspan="2"><center>
<a href="https://conf.kde.org/en/Akademy2013/public/schedule/2013-07-14">
Akademy Main Talks</a>
</center></td>
<td> </td>
</tr>
<tr>
<td>Mon 15th</td>
<td colspan="2"><center>
<a href="https://community.kde.org/Akademy/2013/Monday">
Akademy BoFs, Workshops &amp; Meetings</a> + 
<a href="http://qt-project.org/groups/qt-contributors-summit-2013/wiki">QtCS</a>
</center></td>
<td><center>
<a href="../qtcs-party-monday">Party</a>
</center></td>
</tr>
<tr>
<td>Tue 16th</td>
<td colspan="2"><center>
<a href="https://community.kde.org/Akademy/2013/Tuesday">
Akademy BoFs, Workshops &amp; Meetings</a> + 
<a href="http://qt-project.org/groups/qt-contributors-summit-2013/wiki">QtCS</a>
</center></td>
<td> </td>
</tr>
<tr>
<td>Wed 17th</td>
<td colspan="1"><center>
<a href="https://community.kde.org/Akademy/2013/Wednesday">
Akademy BoFs, Workshops &amp; Meetings</a>
</center></td>
<td colspan="2"><center>
<a href="../daytrip">Akademy Daytrip</a>
</center></td>
</tr>
<tr>
<td>Thu 18th</td>
<td colspan="2"><center>
<a href="https://community.kde.org/Akademy/2013/Thursday">
Akademy BoFs, Workshops &amp; Meetings</a>
</center></td>
<td> </td>
</tr>
<tr>
<td>Fri 19th</td>
<td colspan="2"><center>
<a href="https://community.kde.org/Akademy/2013/Friday">
Akademy BoFs, Workshops &amp; Meetings</a>
</center></td>
<td> </td>
</tr>
</table>
