---
title: Sponsors
menu:
  "2013":
    weight: 7
---

## Sponsorship Opportunities

**A big thank you to our sponsors who help to make this event happen!**

## Sponsors for Akademy 2013

### Platinum


{{< sponsor homepage="https://digia.com" img="/media/2013/digia_0.png" >}}

<p id="digia"><strong>Digia</strong>
is responsible for all worldwide Qt activities including product development,
commercial and open source licensing working together with the Qt Project
under the open governance model. Digia has in-depth Qt expertise and experience
from demanding mission-critical development projects and hundreds of in-house
certified Qt developers. Together with our licensing, support and services
capabilities, we operate with the mission to work closely with developers to
ensure that their Qt projects are deployed on time, within budget and with a
competitive advantage.</p>

{{< /sponsor >}}

### Gold


{{< sponsor homepage="https://blue-systems.com" img="/media/2013/bluesystems_0.png" >}}

<p id="bluesystems"><strong>Blue Systems</strong>
is a company investing in Free/Libre Computer Technologies. It sponsors several
KDE projects and distributions like Kubuntu and Netrunner. Their goal is to
offer solutions for people valuing freedom and choice.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://www.blackberry.com" img="/media/2013/blackberry.png" >}}

<p id="blackberry"><strong>BlackBerry</strong>:
A global leader in wireless innovation, *BlackBerry®* revolutionized the mobile
industry when it was introduced in 1999. Today, BlackBerry aims to inspire the
success of our millions of customers around the world by continuously pushing
the boundaries of mobile experiences. Founded in 1984 and based in Waterloo,
Ontario, BlackBerry operates offices in North America, Europe, Asia Pacific
and Latin America. Research In Motion announced that effective January 30,
2013, the Company would operate around the world under the iconic name
BlackBerry. The legal name of the company has not changed, but the
Company will do business as BlackBerry until shareholders vote for the
official change at the Company's Annual General Meeting later in 2013.
Effective Monday, February 4, 2013, the Company commenced trading under
its new ticker symbols "BB" on the Toronto Stock Exchange and "BBRY" on
the NASDAQ.</p>

{{< /sponsor >}}

### Silver


{{< sponsor homepage="https://developers.google.com/opensource" img="/media/2013/google_0.png" >}}

<p id="google"><strong>Google</strong>
is a proud user and supporter of open source software and development
methodologies. Google contributes back to the Open Source community in
many ways, including more than 20 million lines of source code, project
hosting on Google Code, projects for students including Google Summer
of Code and the Google Code-in Contest, and support for a wide variety
of projects, LUGS, and events around the world.</p>

{{< /sponsor >}}

### Bronze


{{< sponsor homepage="https://froglogic.com" img="/media/2013/froglogic_0.png" >}}

<p id="froglogic"><strong>froglogic GmbH</strong>
is a software company based in Hamburg, Germany. Their flagship product is
Squish, the market-leading automated cross-platform testing tool for GUI
applications based on Qt, QML, KDE, Java AWT/Swing and SWT/RCP, JavaFX,
Windows MFC and .NET, Mac OS X Carbon/Cocoa, iOS Cocoa Touch, Android and
for HTML/Ajax or Flex-based Web applications running in various Web
browsers.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://www.ics.com" img="/media/2013/ics.png" >}}

<p id="ics"><strong>ICS</strong>:
As the largest independent Qt services firm in North America, with a growing
European presence, ICS has vast experience solving development challenges for
Qt and Qt Quick (QML) applications. ICS supports the global Qt community and
offers a suite of professional services — from training to full product
development and delivery.</p>

{{< /sponsor >}}

### Social Program


{{< sponsor homepage="" img="/media/2013/bilbaobizkaia.png" >}}

<p id="bilbaobizkaia"><strong>Bilbao Bizkaia Be Basque</strong></p>

{{< /sponsor >}}

{{< sponsor homepage="https://www.bilbao.eus" img="/media/2013/bilbo.png" >}}

<p id="bilbo">The <strong>Council of Bilbao</strong>
is proud to contribute in the success of Akademy, as an event that promotes and
spreads the identity of Bilbao as a city that wellcomes a free software world
meeting, which has a significative impact in the local economy, not only as
tourism but also in the industry of IT.</p>

{{< /sponsor >}}

### Supporters


{{< sponsor homepage="https://openinventionnetwork.com" img="/media/2013/oin_1.png" >}}

<p id="oin"><strong>Open Invention Network (OIN)</strong>
is a collaborative enterprise that enables Linux through a patent non-aggression
community of licensees. OIN also supports Linux Defenders, which helps the open
source community defend itself against poor quality patents by crowdsourcing
defensive publications and prior art.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://www.redhat.com" img="/media/2013/redhat_0.png" >}}

<p id="redhat"><strong>Red Hat</strong> is the world's
leading provider of open source solutions, using a community-powered approach
to provide reliable and high-performing cloud, virtualization, storage,
Linux, and middleware technologies. Red Hat also offers award-winning support,
training, and consulting services. Red Hat is an S&amp;P company with more
than 70 offices spanning the globe, empowering businesses everywhere.</p>

{{< /sponsor >}}

### Venue


{{< sponsor homepage="https://www.ehu.eus/" img="/media/2013/ehu.png" >}}

<p id="ehu">
<strong>The University of the Basque Country, UPV/EHU</strong>, is the 
biggest public university in the Euskadi region. The Akademy team is
grateful that UPV/EHU is generously hosting Akademy at Bizkaia Aretoa
and ETSI (the Faculty of Engineering).</p>

{{< /sponsor >}}

### Transport


{{< sponsor homepage="https://www.metrobilbao.eus/" img="/media/2013/metrobilbao.png" >}}

<p id="metrobilbao"><strong>Metro Bilbao</strong>
offers a transport service whose high quality can be vouched for by the fact that
over 300,000 people use it every day to move around the city of Bilbao and the
outlying towns along the banks of the Nervión river estuary. Its underground
stations were designed by renowned architect Sir Norman Foster, who gave the
system an urbane yet cutting-edge look that has proved extremely attractive
to film-makers and advertising agencies. The high domed ceilings, glass lifts,
escalators, moving walkways, tracks and trains form an original backdrop for
conjuring up fascinating metropolitan images. Metro Bilbao has permitted the
use of its facilities for the making of films. Renowned advertising agencies
have also chosen it as a luxury setting for photo-shoots that have appeared
in some of the most prestigious fashion magazines, such as Colors (Benetton),
Vogue and Elle. Insurers, airlines, cosmetics companies and a great many
company brochure producers have photographed its metropolitan landscape of
steel, cement and glass.</p>

{{< /sponsor >}}

