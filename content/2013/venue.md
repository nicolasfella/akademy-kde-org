---
title: Venue
menu:
  "2013":
    weight: 5
---

## Bizkaia Aretoa

Akademy 2013 will take place in two different venues. The weekend conference
(13-14 July) of talks and presentations will take place at the [Bizkaia Aretoa
building](http://www.bizkaia.ehu.es/p209-sharethm/en/) which is part of the
University of the Basque Country, [UPV/EHU](http://www.ehu.es/p200-shenhm/en) .
The building is located on the Ría de Bilbao river, close to the famous
Guggenheim Museum.

*Address:  
Bizkaia Aretoa  
Avenida Abandoibarra, 3  
48009 Bilbao*

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox=-2.939272,43.266027,-2.935168,43.269667&amp;layer=mapnik&amp;marker=43.26854,-2.93748" style="border: 1px solid black"></iframe><p><small><a href="http://www.openstreetmap.org/?lat=43.267847&amp;lon=-2.93722&amp;zoom=17&amp;layers=M&amp;mlat=43.26854&amp;mlon=-2.93748">View Larger Map</a></small></p>


The University of the Basque Country has 45,000 students and an academic staff
of 5,000 people. The Bilbao campus is one of the three campuses of the
University. The others are located in Vitoria-Gasteiz and San Sebastian.

## The Faculty of Engineering (ETSI)

From Monday to Friday (15-19 Juy), workshops and BoF sessions will talk place in
the [Faculty of Engineering at
UPV/EHU](http://www.ingeniaritza-bilbao.ehu.es/p224-hmencont/en/contenidos/informacion/indice_etsingenieros/en_escuela/presentacion.html). The Faculty is located in the middle of
the City, close to the San Mámes football stadium. ETSI was founded in 1897, and
offers classes taught in Spanish, Basque, English and French.

*Address:  
Alameda Urquijo s/n.  
48013 - Bilbao  
Main entrance at Luis Briñas St.*  

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox=-2.950537,43.260542,-2.946433,43.264183&amp;layer=mapnik&amp;marker=43.26228,-2.94836" style="border: 1px solid black"></iframe><p><small><a href="http://www.openstreetmap.org/?lat=43.2623625&amp;lon=-2.948485&amp;zoom=17&amp;layers=M&amp;mlat=43.26228&amp;mlon=-2.94836">View Larger Map</a></small></p>

