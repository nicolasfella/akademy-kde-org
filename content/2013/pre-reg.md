---
title: Pre-Registration Event
menu:
  "2013":
    weight: 48
    parent: program
---

For all attendees that arrive on Friday, 12 July or earlier that week there is a
chance to pick up their badges at the early registration get-together on Friday,
12 July at the Hika Ateneo bar from **18:00 to 22:00**. This way, you'll be able to
avoid the Saturday morning crowd.

You can pick up your badge there, meet fellow Akademy attendees, have a drink
and listen to some live music. This bar is located in the old part of Bilbao
(the "Casco Viejo"), close to the river ("ría") and the bridge of Saint Anton.
The address is Muelle de Ibeni, 1, 48006 Bilbao. ([Google maps
link](http://goo.gl/maps/Mnt9A)).

Don't worry if you arrive later! There will be a registration/info desk open all
day from Saturday to Tuesday and also during the rest of the week where you can
pick up your badge, buy some KDE merchandising or get information about the
conference.

