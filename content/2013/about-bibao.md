---
title: About Bilbao
menu:
  "2013":
    weight: 62
    parent: travel
---

<div style="float: right; padding: 1ex; margin: 1ex; width: 320px; ">
<img src="/media/2013/320px-Bilbao_.jpg" /><br />
<figcaption>
From <a href="http://commons.wikimedia.org/wiki/File:Bilbao_.jpg">
Wikimedia Commons</a> <br>
<small>Patricia Del Sol, CC-BY-SA</small>
</figcaption>
</div>


Bilbao is situated in the north of Spain, on the Bay of Biscay. It is the
capital of the region of Biscay (or Bizkaia in the Basque language) and is part
of the autonomous community of the Basque Country. The city proper has a
population of over 370,000. The greater Bilbao area is home to more than a
million people. The main languages are Spanish and Basque, but many people speak
English.

Bilbao has oceanic climate, which means that there is a lot of rain - even in
summer. So, *bring your umbrella or rain jacket!* The average temperature in
summer is 20° Celsius, although is is usually a bit higher in July (25°C).

Bilbao is home to a big port with a history of over 700 years and has profited
from being an iron and steel industry hub in the 20th century. The city has
several museums, such as the famous [Guggenheim
Bilbao](http://www.guggenheim-bilbao.es/en/), that are worth a visit.
The city's architecture was revamped in the 1990s. There are many buildings
designed by renowned international architects and designers such as Frank Gehry
and Phillipe Starck. Sir Norman Foster designed the stations of [Metro Bilbao](http://www.metrobilbao.net/en/using-the-underground). Do
not miss out on the Old Town of Bilbao. In this area, called Zazpikaleak (in
Basque) or Las Siete Calles (in Spanish), there are a lot of nice pubs,
restaurants, shops and the [largest indoor market of
Europe](http://www2.bilbao.net/bilbaoturismo/ingles/dcomprar/dcomprar3.htm), Mercado de la
Ribera. Try the famous [Pintxos](http://en.wikipedia.org/wiki/Pincho) in the various bars of Old Town.

More information about **recommended hotels and hostels** can be found
[here](https://akademy.kde.org/2013/accommodation).
Information about the nearest airports and public transport can be found
[here](/2013/travel).
More information about Bilbao's sights can be found
[here](http://en.wikivoyage.org/wiki/Bilbao#See).

Spain is part of the Schengen Visa area. If you need to apply for a visa to
travel to Spain, please email [The Akademy Team](mailto:akademy-team@kde.org)
for invitation letters and other visa requirements. Please provide complete
information about your involvement with KDE and why you wish to attend the event.
