---
title: Register
menu:
  "2013":
    weight: 3
---

Anyone can attend Akademy for free. But registration is required in order to
attend the event. Please register soon so that we can plan accordingly.

Akademy 2013 registration is based on your
[identity.kde.org](https://identity.kde.org/) username and
password. In order to sign in here, you must first create an account there.
After you have completed the process of creating
your KDE Identity account, please continue to the [Akademy Registration
System](https://conf.kde.org/en/Akademy2013/). Please use your identity.kde.org
username and password to log in and register.


