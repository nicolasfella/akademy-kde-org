---
title: Akademy Party on Saturday
menu:
  "2013":
    weight: 49
    parent: program
---

You are invited to the Akademy party on Saturday at [Kafe 
Antzokia](http://www.kafeantzokia.com/web/). Kafe Antzokia
is located in downtown Bilbao in a beautiful former cinema. Today, it is a
concert and party venue.

Especially for our party, two bands will be playing, "Las Dos D" with
traditional Cuban music and then "Adarrots", a well-known Basque folk group.

Doors open at 21:30. There is no food served at the party, so make sure to grab
dinner before the event.

Address: Calle San Vicente 2, 48001 Bilbao  
[Google
maps](https://maps.google.de/maps?q=kafe+antzokia&oe=utf-8&client=firefox-a&channel=fflb&fb=1&gl=de&hq=kafe+antzokia&cid=0,0,3021330675821639776&t=m&z=16&iwloc=A)

![Image of interior of Cafe Antzioka](/media/2013/kafe-antzokia.jpg)
