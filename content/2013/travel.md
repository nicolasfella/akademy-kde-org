---
title: Travel
menu:
  "2013":
    weight: 63
    parent: travel
---

Akademy attracts people from around the world. Travel and accommodation are
important considerations for the Akademy Team and for people who are attending
Akademy. The first part of July is busy in Bilbao; there is a [big music 
festival](http://www.bilbaobbklive.com/2013/en/)
taking place from 11-13 July. **We strongly encourage you to plan your trip to
Akademy 2013 and book your hotel as soon as possible**.

KDE e.V. may reimburse Akademy travel costs according to the [KDE e.V.
reimbursement policy](http://ev.kde.org/rules/reimbursement_policy.php). An 
online request form will be available soon.

## Traveling by Plane

The [Bilbao Airport](http://www.aena-aeropuertos.es/csee/Satellite/Aeropuerto-Bilbao/en/)
is located approximately 12 km from the city center. There is
service to the Bilbao bus station regularly (one bus every 20 minutes
approximately and the trip is about 20 minutes).  There are direct flights from
many major European airports, such as Amsterdam, Brussels, Frankfurt, Munich,
London (Heathrow and Stansted), Paris and Rome, and, of course, Madrid and other
Spanish airports. The [Santander
Airport](http://www.aena-aeropuertos.es/csee/Satellite/Aeropuerto-Santander/en/)
is also near Bilbao (approximately 1 hour by bus).

## By Train

Bilbao is not very well connected to the rest of the world by train. If you
really want to take a train, the best idea is taking the TGV Paris - Hendaya and
then a bus from Alsa or Pesa to Bilbao (one hour and a half to Bilbao).

However, there is a geek option here. If you have plenty of time and like the
old fashioned travelling, you can take the FEVE train from Santander (3 hours),
Oviedo (via Santander, 6 hours) or Leon (8 hours) to Bilbao. These trains use
the old narrow gauge and travel through some of the most beautiful places in the
area.

## By Bus

Bilbao has a good set of bus connections with Europe. Alsa is the company with
the most (if not all) international routes. Of course, you can also travel by
bus from almost every city of Spain to Bilbao.

## By Boat

It is even possible to take come by boat. There are ferries from Portsmouth to
Santurce (5 min to Bilbao by subway) or from Plymouth, Portsmouth and Poole to
Santander. From there you can take a bus to Bilbao.

## By bicycle or foot

For the really, really geeky people, there is the option to bike to Bilbao or
even walk: just take the [St. James Way](http://en.wikipedia.org/wiki/Way_of_St._James). 
The [Northern
Way](http://en.wikipedia.org/wiki/Way_of_St._James_(route_descriptions)#The_Northern_Way)
will take you from Irún
in France through Bilbao, so you can attend Akademy 2013 and then continue the
pilgrimage to Santiago de Compostela. The local committee will treat anyone to
[pintxos](http://en.wikipedia.org/wiki/Pincho) and potes (food and drink) who
comes to Akademy this way :)

## Getting around in Bilbao

Bilbao has two metro lines and several bus and tram services. You can find a map
and timetable for Metro Bilbao
[here](http://www.metrobilbao.net/en/using-the-underground). The website for the
tram services is only available in [Spanish](http://www.euskotren.es/), but the
trams are an easy way to get around and will take you to our [weekend 
venue](/2013/venue), for instance. More information about the bus services
can be found
[here](http://www2.bilbao.net/bilbaoturismo/ingles/mciudad/mciudad3.htm).

### Getting from the airport to various places

There is a bus from the airport to the bus station (in front of San Mamés
Engineering School) that stops also in Moyua Square (Bizkaia Aretoa). It runs
every 30 minutes, and costs 1.30 €. Tickets can be bought from the driver, but
he might not take bills that are more than 20 €. The buses run during the hours
the airport is open. Check routes and timetables
[here](http://apli.bizkaia.net/apps/danok/tq/index.html?Idioma=ES). More
information about how to get to and from the airport can also be found on
the [website of the Bilbao Tourist
Information](http://www2.bilbao.net/bilbaoturismo/ingles/cllegar/cllap3.htm).

### Public transport tickets 

#### Are there multi-journey tickets? If so how much and where to buy?

Creditrans is a travel card system for the Bilbao region. The tickets can be
used on the metro (underground), buses and trams. Creditrans tickets are prepaid
cards (for 5, 10 or 15 €) that can be purchased or recharged in every metro or
tram station and also at some bus stops and kiosks. You can combine public
transport journeys with that card and also get a considerable discount if you
use the card instead of buying tickets from the driver. Creditrans cards cannot
be purchased at the airport.  Sometime prior to Akademy, the Creditrans cards
will be replaced with a contactless ticket (Barik) with the same functionality;
you will have to spend 3 € to get the card.

#### Can you change vehicles for the same fare? (i.e. are they time based or single journey)

Yes in the metro system (there are only two lines and they share almost half of
the journey). But if you combine different public transport options (every bus
line counts as different) and you have used the Creditrans or Barik cards, there
will be a low fare (even free if you switch vehicles in less that 45 minutes).

#### Can you buy tickets on the bus?

Yes. However, if you plan to use the tram you CANNOT buy tickets inside, you
MUST buy and VALIDATE at the stop!

