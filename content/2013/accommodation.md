---
title: Accommodation
menu:
  "2013":
    weight: 61
    parent: travel
---

Akademy attracts people from around the world. Travel and accommodation are
important considerations for the Akademy Team and for people who are attending
Akademy. The first part of July is busy in Bilbao; there is a big music festival
taking place from 11-13 July. We strongly encourage you to plan your trip to
Akademy 2013 and book your hotel as soon as possible.

The KDE e.V. AGM starts on the morning of Friday 12th July, so KDE e.V. members
should plan to arrive on Thursday 11th July, unless they are travelling only a
short distance.

The period for applying for reimbursement of travel costs from KDE e.V. has now
closed and the budget is all allocated.  Alternatives to accommodation in Bilbao

If you are still trying to find somewhere to stay for the first few days of
Akademy, it might be worth looking at the villages connected by Metro to Bilbao
(no more than 45 min journey into the centre of Bilbao).

Ordered by proximity the villages are: Etxebarri, Basauri, Cruces, Barakaldo,
Astrabudua, Leioa, Lamiako, Areeta, Getxo, Sestao, Portugalete and Santurtzi.
Search for "pensión" which is the local name for an inn cheaper than a hotel.

## Hostels

### [Bilbao Central Hostel](http://bilbaocentralhostel.com/)

Address:Fernández del Campo 24

- 1 mixed room for 4 people
- 6 mixed rooms for 6 people each
- Modern and fully-equipped communal kitchen for customers to use
- Comfortable dining-room for breakfasts and meals
- Comfortable and pleasant sitting-room with sofas and TV
- Spacious bathroom with large individual showers. Adapted for disabled.
- Computer room with internet access
- Coin laundry service
- Free Wi-Fi
- Air-conditioning in the whole hostel
- On the occasion of the inauguration of Bilbao Central Hostel they have low
  prices: from Sunday to Thursday the price is 18€ per person/day and on Friday
or Saturday, 19 € per person/day.
- 15min walk to both venues

### ["Miguel de Unamuno" Residence](http://www.ehu.es/p200-content/es/contenidos/informacion/colegiomayor_residenciasupv/es_cole_res/colegiomayor_residenciasupv.html#unamuno)

Address: Avda. Lehendakari Aguirre, 140. 48015 Bilbao

- Internet connection: wired connection in rooms, wifi in shared areas
- Kitchen: there are some (few) microwave ovens in the building
- Price: 30 € with breakfast. Additionally, 8 € for dinner
- Gym
- Metro station less that 5 minutes away
- Bus station just in front of the residence
- To book a room, please send an email to Maximino Guisasola Larrinaga and
  please reference Akademy in your email

### (Fully Booked) ["Blas de Otero" Residence](http://www.ehu.es/p200-content/es/contenidos/informacion/colegiomayor_residenciasupv/es_cole_res/colegiomayor_residenciasupv.html#blas_otero)

Address: C/ Las Cortes, 38, 48002 Bilbao.

- This is the main hostel for the event. It has meeting rooms and areas for
  late-night hacking.
- Available places: 30 two-bed rooms, can also be used as single rooms
- Internet connection: wired in rooms, WiFi in shared areas
- Kitchen: in each room
- En-suite shower and toilet in each room
- No catering
- Prices:
  - For a stay of 7 nights min.:
    - Twin room (two separate beds): € 18,25 per night, per person
    - Single room: € 28,50 per night
  - For a stay of less than 7 nights:
    - Twin room (two separate beds): € 25,50 per night, per person
    - Single room: € 37,50 per night
- Prices include bed linen and towels
- City center location
- Route to [Bizkaia
  Aretoa](http://openrouteservice.org/index.php?start=-2.928996,43.2556138&end=-2.9374275,43.2683579&pref=Pedestrian&lang=cz&noMotorways=false&noTollways=false)
and
[ETSI](http://openrouteservice.org/index.php?start=-2.928996,43.2556138&end=-2.9482129,43.2621869&pref=Pedestrian&lang=es&noMotorways=false&noTollways=false)
on foot
- Route to venues by Public Transport: The best option to both of them is to
  take the tram at the [Rivera
stop](http://openrouteservice.org/index.php?start=-2.928996,43.2556138&end=-2.9244087,43.2558186&pref=Pedestrian&lang=cz&noMotorways=false&noTollways=false)
and get off at the Guggenheim stop if you are going to Bizkaia Aretoa or at San
Mamés stop when going to ETSI
- **This hostel is now fully booked during Akademy week.**

## [BBK Bilbao Good Hostel](http://www.bbkbilbaogoodhostel.com/)

Address: Avda Miraflores 16 (Bolueta)

- Proximity to venue: walking +30 minutes, underground/bus 5 minutes
- 104 beds. Dorm rooms of 4, 6, 8 and 10 beds. Private double rooms
- Breakfast included. Dinner is outsourced to catering company. Ask for special
  dietary needs in advance. Kitchen for guests with microwaves only, no
stove/oven
- Common areas with TV and computers, terraces, washing machine & dryer
  available
- 6 parking places
- WiFi all over the hostel
- Prices: Dorms from €17.50 to €20 / Private doble rooms €22.50 (including
  breakfast)
- Route to [Bizkaia
  Aretoa](http://openrouteservice.org/index.php?start=-2.9110216,43.2468091&end=-2.9373751,43.2680153&pref=Pedestrian&lang=cz&noMotorways=false&noTollways=false)
and
[ETSI](http://openrouteservice.org/index.php?start=-2.928996,43.2556138&end=-2.9482129,43.2621869&pref=Pedestrian&lang=es&noMotorways=false&noTollways=false)
on foot
- Route to venues by Public Transport: The best option is taking the metro
  system at Bolueta Station and exit, in the case of going to Bizkaia Aretoa, at
Moyua Station (and then walk) or, in the case of going to ETSI, at San Mamés
Station (ETSI is just in front if you take the elevator)

### (Full Saturday) [Bilbao Akelarre Hostel](http://www.bilbaoakelarrehostel.com/ing/site/albergue.asp)

Address: Morgan 4-6 (Deusto)

- Proximity to venue: walking 20 minutes, underground/bus 10 minutes
- 38-40 beds
- Prices: €16.50 to €19
- breakfast, sheets and blankets, wi-fi, common computer, coffee & tea all day
- Route to [Bizkaia
  Aretoa](http://openrouteservice.org/index.php?start=-2.9553298,43.2688139&end=-2.9373751,43.2680153&pref=Pedestrian&lang=cz&noMotorways=false&noTollways=false)
and
[ETSI](http://openrouteservice.org/index.php?start=-2.9553298,43.2688139&end=-2.9482035,43.2622935&pref=Pedestrian&lang=cz&noMotorways=false&noTollways=false)
on foot
- Route to venues by Public Transport: The best option is taking the metro
  system at Deusto Station and exit, in the case of going to Bizkaia Aretoa, at
Moyua Station (and then walk) or, in the case of going to ETSI, at San Mamés
Station (ETSI is just in front if you take the elevator)

### [Botxo Gallery](http://botxogallery.com/)

Address: Avda Universidades 5

- Proximity to venue: walking 15 minutes, bus 5 minutes
- 39 beds
- Prices: max €25 (including breakfast)
- Bizkaia Aretoa is just in front of this hostel. To get to ETSI onf foot you
  should take this [route](/2013/accommodation)
- Route to venues by Public Transport: The best option is crossing the Parre
  Arrupe bridge (the wooden bridge) and getting in the tram at Guggenheim stop
and getting of at San Mamés stop

### (Fully Booked) [Ganbara Hostel](http://www.ganbarahostel.com/)

Address: Prim 13 (Casco Viejo)

- Proximity to venue: walking 15 minutes, underground 3 minutes
- 62 beds in 13 rooms for 4, 6 or 8 people each
- Washing machine & dryer available
- well equipped kitchen available for residents
- WiFi in common areas
- Prices: €17.50 to €19 (week or weekend, including breakfast and coffee & tea
  all day)

## Hotels

### [Ria de Bilbao](http://www.riabilbao.com/)

Address: Ribera de Deusto 32

- Proximity to venue: walking +20 minutes, bus 5 minutes
- 40 double rooms
- Prices: €52 per double room (including breakfast)

### [Sirimiri](http://www.hotelsirimiri.es/)

Address: Plaza Encarnacion 3 (Atxuri)

- Proximity to venue: walking +20 minutes, start of tram line, bus 1 minute,
  underground 10 minutes
- 58 double rooms
- Prices: €70/80 (including breakfast)

### [Ibis Bilbao](http://www.ibis.com/es/hotel-6141-ibis-bilbao-centro/index.shtml)

Address: General Concha 28

- Proximity to venue: (walking) 20 minutes to Bizkaia Aretoa , 15 minutes to the
  Engineering School of Bilbao
- 75 rooms
- Prices:
  - 13-14 July : 90€ twin or single room without breakfast (high rate due to the
    music festival)
  - 15-19 July: 68€ twin or single room without breakfast


