---
title: Program Committee
menu:
  "2013":
    weight: 53
    parent: program
---

The Akademy 2013 Program Committee consists of:

- **Dario Freddi** maintains several KDE components, and represents KDE as a
  speaker or promoter at events worldwide.
- **Kevin Krammer** has been a KDE contributor for over a decade and is
  primarily active in user support and developer mentoring.
- **Lydia Pintscher** is on the Board of Directors of KDE e.V. She is a member
  of KDE's Community Working Group and one of the admins of KDE's mentoring
programs.
- **Marta Rybczyńska** can be usually found in embedded and kernel development.
  She helps create the [KDE Commit Digest](http://commit-digest.org/) and
  coordinates KDE Polish translation efforts.
- **Thiago Macieira** started serious open source work with KDE back in 2001. He
  has been a full-time Qt developer for 7 years and is currently the Qt Core
Maintainer in the Qt Project.

If you have any questions about the Call for Presentations, you can [email the
Program Committee](mailto:Akademy-talks@kde.org). For all other Akademy-related
questions, please contact the [Akademy organizing team](mailto:Akademy-team@kde.org).

