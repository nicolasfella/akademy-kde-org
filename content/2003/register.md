---
title: Registration
menu:
  "2003":
    weight: 3
---
The organisers would like to encourage advance registration and payment, so the fees
are lower if paid before 29 July. After this date all registrations will
be charged an on-site surcharge. There are additional
reductions for registrations made before 10 July. Please take advantage of
the lowest fees by completing the online form and sending payment to
arrive no later than 10 July. 

### Fee summary

<table cellpadding="3" cellspacing="0" border="0" width="100%">
<tr>
<td bgcolor="#cccccc" width="40%"> </td>
<td colspan="2" align="center" bgcolor="#cccccc" width="30%"><strong>Business fees</strong></td>
<td colspan="2" align="center" bgcolor="#cccccc" width="30%"><strong>Community fees</strong></td>
</tr>
<tr>
<td bgcolor="#cccccc" width="40%"> </td>
<td align="center" bgcolor="#cccccc" width="15%">EUR</td>
<td align="center" bgcolor="#cccccc" width="15%">USD</td>
<td align="center" bgcolor="#cccccc" width="15%">EUR</td>
<td align="center" bgcolor="#cccccc" width="15%">USD</td>
</tr>
<tr>
<td bgcolor="#cccccc">Payment received by 10 July</td>
<td align="center">800</td>
<td align="center" bgcolor="#eeeeee"></td>
<td align="center">0</td>
<td align="center" bgcolor="#eeeeee">0</td>
</tr>
<tr>
<td bgcolor="#cccccc">Onsite fees</td>
<td align="center">1000</td>
<td align="center" bgcolor="#eeeeee"></td>
<td align="center">0</td>
<td align="center" bgcolor="#eeeeee">0</td>
</tr>
</table>

The fees include attendance at all conference sessions.

Registration period over.

**Who is a business participant?**

Large companies are welcome to send employees to our conference. The business
fee will be used to finance bursaries for community members who could not
afford to attend otherwise. If you are a community member, but also an
employee, you can choose which fee you take. Please ask your employer if he
is willing to sponsor our conference by paying the fee for you.

**Payments can be made in the following ways:**

 * by credit card (Visa or MasterCard only) in GBP
 * by cheque drawn on a Euro bank account in EUR
 * by cash (in CZK or EUR for onsite payment only)

### Accommodations

Affordable accommodations and boarding are available in the castle, the
Monastry, hotels and boarding houses in the town. Expect to pay about 15-20
Euros per day for full board and lodging. You will be asked to pay the rooms
with Czech Crown at your checkin, so please take care that you have the money
in the correct currency available. We have reservations for 150 community
members in the castle and Monastry. If you are a business participant, you
can contact us for information about available hotels.

The castle rooms are similar to those you would find in a student hostel,
with simple furniture and two or three beds. Each floor provides bathroom
facilities (shower and toilet) as well as a small kitchen (suitable for
preparing tea and small meals). The monastry is more stylish and has larger
rooms, mostly doubles and includes two five-bed rooms. It is still in
operation, so all you code-monks will fit right in. Showers and toilets are
also on each floor. The monastry is 200 meters away from the castle.
Breakfast, lunch and dinner is served for everyone in the Refektarium of the
monastry. 
