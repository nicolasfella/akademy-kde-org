---
title: Program
menu:
  "2003":
    weight: 4
---
<section>
<table style="text-align: left; width: 100%;" border="1" cellspacing="2" cellpadding="2">
<tbody>
<tr>
<td style="vertical-align: top; font-weight: bold;">Time<br />
      </td>
<td style="vertical-align: top; text-align: center;" rowspan="1" colspan="2">
<h3>Saturday</h3>
</td>
<td style="vertical-align: top; text-align: center;" rowspan="1" colspan="2">
<h3>Sunday</h3>
</td>
</tr>
<tr>
<td style="vertical-align: top;"><br />
      </td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255); font-weight: bold;">Development<br />
      </td>
<td style="vertical-align: top; background-color: rgb(204, 204, 204); font-weight: bold;">Usability, Accessibility &amp; Misc<br />
      </td>
<td style="vertical-align: top; font-weight: bold; background-color: rgb(153, 255, 255);">Development<br />
      </td>
<td style="vertical-align: top; background-color: rgb(255, 204, 102); font-weight: bold;">New Horizons<br />
      </td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">8:30-8:45<br />
      </td>
<td style="vertical-align: top;" rowspan="1" colspan="2">Opening by Dr. Dalibor Stys (Director, Institute of Physical Biology, University of South Bohemia), Prof. Proell (FH Hagenberg), and Matthias Kalle Dalheimer (President, KDE e.V.)
      </td>
<td colspan="2"></td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">8:45-9:00<br />
      </td>
<td style="vertical-align: top;" rowspan="1" colspan="2">Practical Information (laptop rental, location, food, etc.); Dr. Rudi Ettrich, Acadamic and University Center, Nové Hrady
      </td>
<td colspan="2"></td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">9:05-10:05<br />
      </td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/kdevelop.tar.bz2">Harald Fernengel, Trolltech AS: KDevelop</a>
      </td>
<td style="vertical-align: top; background-color: rgb(204, 204, 204);">
      </td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/Makefile.am.tar.bz2">David Faure, Klarälvdalens Datakonsult AB: How to write a Makefile.am</a>
      </td>
<td style="vertical-align: top; background-color: rgb(255, 204, 102);"><a href="http://files.kde.org/akademy/2003/presentations/knot-zeroconf.kpr">Tim Jansen: The Knot -  A SLP-based Zeroconf solution</a>; <a href="http://files.kde.org/akademy/2003/presentations/knot-zeroconf.kwd">Paper</a></td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">10:05-11:05<br />
      </td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/dp-talk-nh2k3.pdf">Marc Mutz, University of Bielefeld: Design Patterns</a> (<a href="http://files.kde.org/akademy/2003/presentations/dp-talk-nh2k3.pdf.sig">sig</a>), <a href="http://files.kde.org/akademy/2003/presentations/dp-talk-nh2k3.tex">TeX source</a> (<a href="http://files.kde.org/akademy/2003/presentations/dp-talk-nh2k3.tex.sig">sig</a>) </td>
<td style="vertical-align: top; background-color: rgb(204, 204, 204);">
      </td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/unsermake-talk.sxi">Stephan Kulow, SuSE AG: The KDE Build System</a><br />
      </td>
<td style="vertical-align: top; background-color: rgb(255, 204, 102);"><a href="http://files.kde.org/akademy/2003/presentations/kaxul.kpr">George Staikos, Zack Rusin: KaXUL &amp; uXUL</a></td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">11:10-11:40</td>
<td style="vertical-align: top;" rowspan="1" colspan="2">Coffee Break</td>
<td style="vertical-align: top;" rowspan="1" colspan="2">Coffee Break</td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">11:40-12:40</td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/n7y-kabc.tar.bz2">Tobias König, University of Technology Dresden: Programming with libkabc</a>
      </td>
<td style="vertical-align: top; background-color: rgb(204, 204, 204);"><a href="http://files.kde.org/akademy/2003/presentations/KDEandAccessibility.pdf">Gunnar Schmidt, University of Paderborn: KDE and Accessibility</a> (<a href="http://files.kde.org/akademy/2003/presentations/KDEandAccessibility.pdf.sig">sig</a>)
      </td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);">
      </td>
<td style="vertical-align: top; background-color: rgb(255, 204, 102);"><a href="http://files.kde.org/akademy/2003/presentations/aosd-kde.sxi">Carsten Pfeiffer, Fraunhofer Institute: Aspect Oriented Technologies: Ready for adoption?</a></td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">12:40-14:00</td>
<td style="vertical-align: top;" colspan="2" rowspan="1">Break</td>
<td style="vertical-align: top;" colspan="2" rowspan="1">Break</td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">14:00-15:00</td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);">Holger Schroeder, Automatix GmbH: KDE-Cygwin</td>
<td style="vertical-align: top; background-color: rgb(204, 204, 204);"><a href="http://files.kde.org/akademy/2003/presentations/AccessibilityAndInteroperability.pdf">Gunnar Schmidt: Accessibility and Interoperability</a> (<a href="http://files.kde.org/akademy/2003/presentations/AccessibilityAndInteroperability.pdf.sig">sig</a>)
      </td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/kcachegrind.sxi">Josef Weidendorfer, University of Technology Munich: Performance Analysis</a>
      </td>
<td style="vertical-align: top; background-color: rgb(255, 204, 102);"> <a href="http://ometer.com/novehrady_dbus_pres/">Havoc Pennington, RedHat, Inc.: D-BUS</a></td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">15:05-16:05
      </td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/Trader.tar.bz2">DavidFaure, Klarälvdalens Datakonsult AB: The KTrader mechanism</a>
      </td>
<td style="vertical-align: top; background-color: rgb(204, 204, 204);"><a href="http://files.kde.org/akademy/2003/presentations/dynamic-desktop.tar.gz">Josef Spillner, University of Technology Dresden: The dynamic desktop</a>
      </td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/Presentation_Andreas_Brand_KDE_Nove_Hrady_24-09-2003.pdf">Andreas Brandt, University of Frankfurt: My research about KDE</a></td>
<td style="vertical-align: top; background-color: rgb(255, 204, 102);"><a href="http://files.kde.org/akademy/2003/presentations/Debugging.tar.bz2">Matthias Kalle Dalheimer &amp; David Faure, Klarälvdalens Datakonsult AB: OpenSource Memory Debugging</a>
      </td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">16:05-16:35</td>
<td style="vertical-align: top;" rowspan="1" colspan="2">Coffee Break</td>
<td style="vertical-align: top;" rowspan="1" colspan="2">Coffee Break</td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">16:35-17:35</td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"> <a href="http://www.sourcextreme.com/kastle/">Ian Geiser, sourceXtreme: Scripting Applications with DCOP</a></td>
<td style="vertical-align: top; background-color: rgb(204, 204, 204);"><a href="http://files.kde.org/akademy/2003/presentations/usability.sxi">Eva Brucherseifer, basysKom GbR: A KDE Usability Study</a></td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/KIO.tar.bz2">David Faure, Klarälvdalens Datakonsult AB: How to write good network transparent code</a>
      </td>
<td style="vertical-align: top; background-color: rgb(255, 204, 102);"><a href="http://files.kde.org/akademy/2003/presentations/kwallet.kpr">George Staikos, Staikos Computing Services Inc.: KWallet</a>; <a href="http://files.kde.org/akademy/2003/presentations/kwallet-kastle-2003.ps">Paper</a></td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">17:40-18:40</td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/TaskJuggler-Project-Management.kpr">Chris Schläger, SuSE AG: TaskJuggler</a></td>
<td style="vertical-align: top; background-color: rgb(204, 204, 204);"><a href="http://ometer.com/novehrady_freedesktop_pres/">Havoc Pennington, RedHat, Inc.: freedesktop.org</a></td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"></td>
<td style="vertical-align: top; background-color: rgb(255, 204, 102);"><a href="http://files.kde.org/akademy/2003/presentations/Kontact.kpr">Daniel Molkentin, University of Technology Chemnitz: Kontact the world!</a>, <a href="http://files.kde.org/akademy/2003/presentations/kontact.txt">Abstract</a></td>
</tr>
<tr>
<td style="vertical-align: top; font-weight: bold;">19:00-20:00</td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);" colspan="2" rowspan="1">Social Event; includes: Matthias Ettrich, Tolltech AS: Qt: past - present - future (<a href="http://dot.kde.org/1061880936/">dot story</a>)</td>
<td style="vertical-align: top; background-color: rgb(153, 255, 255);"><a href="http://files.kde.org/akademy/2003/presentations/kastle-history/">Matthias Ettrich, Matthias Kalle Dalheimer: The History of the KDE Project</a>; <a href="http://files.kde.org/akademy/2003/presentations/kastle-history-images.tar.bz2">Pictures</a> (25MB)</td>
<td style="vertical-align: top; background-color: rgb(255, 204, 102);"></td>
</tr>
</tbody>
</table>

<p><span style="color: rgb(255, 102, 102); font-weight: bold;"></span></p>
<table border="1" width="100%" cellpadding="2" cellspacing="2">
<tbody>
<tr>
<td>
<h3>Monday:</h3>
</td>
</tr>
<tr>
<td style="background-color: rgb(153, 255, 153);"><span style="font-weight: bold;">Workshop:</span> KDE-Qt integration</td>
</tr>
<tr>
<td style="background-color: rgb(153, 255, 153);"><span style="font-weight: bold;">Workshop:</span> KDE System Configuration</td>
</tr>
<tr>
<td style="vertical-align: top; background-color: rgb(153, 255, 153);"><span style="font-weight: bold;">Workshop:</span> Scripting Applications with DCOP</td>
</tr>
<tr>
<td style="vertical-align: top; background-color: rgb(153, 255, 153);">PGP Keysigning Party</td>
</tr>
<tr>
<td style="vertical-align: top; background-color: rgb(153, 255, 153);"><span style="font-weight: bold;">Note:</span> For a maximum of flexibility, workshop time and details will be announced at short notice</td>
</tr>
</tbody>
</table>

<table border="1" style="width: 100%; height: 100%;">
<tbody>
<tr bgcolor="#eeeeee">
<td>Talks about Accessibility</td>
<td>Gunnar Schmidt</td>
<td><a href="mailto:gunnar@schmi-dt.de">&lt;gunnar@schmi-dt.de&gt;</a></td>
</tr>
<tr>
<td colspan="3"><br />Talk 1: KDE and Accessibility<br />This talk covers both the current state of the KDE Accessibility Project and some general view on accessibility issues within KDE. It will also contain an outline of kttsd and word completion for usage as KDE-wide technologies.<br />Talk 2: Accessibility and Interoperability<br />This talk will show an outline of an architecture for adding AT-SPI support(Assistive Technology Service Provider Interface) to Qt/KDE. As there could be applications and assistive technologies both within and without KDE this architecture needs to interoperate well with GNOME's Corba based assistive technology framework.</td>
</tr>
<tr bgcolor="#eeeeee">
<td>Aspect Oriented Technologies: Ready for adoption?</td>
<td>Carsten Pfeiffer</td>
<td><a href="mailto:%3Cpfeiffer@kde.org%3E">&lt;pfeiffer@kde.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">Object Orientation offers strong concepts for functional decomposition of systems. There is a number of requirement types though, that can not be properly encapsulated into classes or functions. The reason for that is, that these often non-functional concerns crosscut the system's structure, leading to a scattered and tangled implementation.<br />
Aspect Orientation aims to provide a solution for this problem by offering a new modularity concept, that allows the extraction of scattered and tangled code fragments into first class entities called aspects [1]. However, the gain of modularity comes at the price of increased complexity. Dedicated tool support to reduce the complexity is therefore crucial for successful emplyoing of aspect-oriented concepts.<br />
In this paper, the concepts of Aspect Orientation are introduced and an overview of state-of-the-art aspect-oriented technologies for C++ is given. A case study will reveal whether these technologies are already suitable for adoption in real-world projects.<br />
References:<br />
[1] Gregor Kiczales, John Lamping, Anurag Menhdhekar, Chris Maeda,<br />
Cristina Lopes, Jean-Marc Loingtier and John Irwin.<br />
Aspect Oriented Programming. In Proceedings European Conference<br />
on Object-Orientated Programming, volume 1241, pages 220-242.<br />
Springer Verlag, 1997.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>Talks</td>
<td>David Faure</td>
<td><a href="mailto:faure@kde.org">&lt;faure@kde.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">
<ul>
<li> Tips and tricks for debugging (gdb, test programs, valgrind, cachegrind...)</li>
<li>How to write a Makefile.am (based on the short talk I gave at the last FOSDEM)</li>
<li>The trader mechanism (usefulness, services and servicetypes, querying, debugging).</li>
<li>How to write good network transparent code (KURL, KIO, jobs vs NetAccess).</li>
</ul>
</td>
</tr>
<tr bgcolor="#eeeeee">
<td> The dynamic desktop </td>
<td>Josef Spillner</td>
<td><a href="mailto:%3Cjosef@ggzgamingzone.org%3E">&lt;josef@ggzgamingzone.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">Many KDE applications are extensible in form of plugins, additional texts or images or audio files, or other means. Up to now each application has to define an own way on how to access these additional files. This ranges from requiring the user to download contents over predefined download sites (like kde-look.org) to embedded mechanisms as used in KOrganizer.<br />
In 2002, a project named KDEShare was proposed, and an initial implementation was started, which worked essentially but had some design issues, and since some timing issues were involved as well the development on it stopped.<br />
Yet it clearly shows that a unified way of updating contents is needed:
<ul>
<li> many applications, like those in kde-edu, come with a default set of data but offer additional sets (sounds, vocabulary files)</li>
<li>some applications rely on fixed host names for their updates, yet a fault tolerant transport is needed, which could be ensured using metaservers embedded in a general resource framework (example: KDE internet radio frontend)</li>
</ul>
<p>This talk is intended to regain interest in the topic, and to present specific development goals.  Application-specific integration will be presented using KParts, application invocation and DCOP calls. Access to metaservers (and how to administrate them) will be part of the presentation. The collaboration with a typical 3rd party KDE project will be shown as well.
      </p></td>
</tr>
<tr bgcolor="#eeeeee">
<td>Kontact</td>
<td>Daniel Molkentin</td>
<td><a href="mailto:molkentin@kde.org">&lt;molkentin@kde.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">
In times where KGX is hitting the corporate desktop, it is important to have a groupware enabled personal information management (PIM) client that is able to interact with common groupware solutions such as Kolab and Exchange 2000.<br />
Kontact, a collaborative effort of the KMail and PIM team tries to serve this demand.This talk will give a summary on what has been achieved and what still remains to do. It includes a general presentation of Kontact's features and addresses technical and conceptional details, ideas and plans.<br />
Also Kontact will later on superseed the Kolab client and therefor inherit all its features.<br />
This talk addresses two primary parties:
<ul>
<li>Developers interested in joining the KDE PIM team.</li>
<li>Companies that seek out on a native KDE integrated client for Kolab or </li>
</ul>
<p>Exchange and want to get involved with Kontact development or support the project.<br />
A workshop will be appended on demand, Kontact hacking will take place during the hackfest.<br />
Web Reference: <a href="http://kontact.org">http://kontact.org</a>
      </p></td>
</tr>
<tr bgcolor="#eeeeee">
<td>Programming with libkabc</td>
<td>Tobias Koenig</td>
<td><a href="mailto:%3Ctokoe@kde.org%3E">&lt;tokoe@kde.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">
The KDE address book library<br />
------------------------------
<ul>
<li> General concept of libkabc</li>
<li> The API</li>
<li>Small example program</li>
<li>Advanced usage</li>
<li>Implementing resources</li>
</ul>
</td>
</tr>
<tr bgcolor="#eeeeee">
<td> Be More Rational: Open Source Memory Debugging and Profiling Tools </td>
<td>Matthias Kalle Dalheimer</td>
<td><a href="mailto:kalle@klaralvdalens-datakonsult.se">&lt;kalle@klaralvdalens-datakonsult.se&gt;</a></td>
</tr>
<tr>
<td colspan="3">
(Together with David Faure):<br />
This talk will be similar to the LinuxTag talk with the same name, but have a stronger emphasis on KDE development. We will present tools like Valgrind and KCachegrind and show real-life examples and to debug and optimize KDE programs in a time-efficient way.
<p>The History of the KDE Project<br />
=======================<br />
(together with Matthias Ettrich)<br />
This talk, given by two participants from "Day One", will present newer KDE developers with some cultural background of "how it all started": Matthias' original Usenet posting, the first pieces of code, the first package, the first CVS server, the famous c't article that got many of<br />
the German developers to KDE, the first KDE conference "KDE One" in Arnsberg, etc.
      </p></td>
</tr>
<tr bgcolor="#eeeeee">
<td>kde-cygwin</td>
<td>Holger Schroeder</td>
<td><a href="mailto:holger-kde@holgis.net">&lt;holger-kde@holgis.net&gt;<br />
      </a></td>
</tr>
<tr>
<td colspan="3">
The kde-cygwin project makes KDE available on Microsoft Windows Operating Systems. Currently the only way to do this is to use Cygwin (<a href="http://www.cygwin.com">http://www.cygwin.com</a>) and to use a X-server in Cygwin. This already works fine, but it has some  drawbacks: All kde windows are inside a big X window, and not directly on the Windows desktop, and only the X window appears in the Windows-"taskbar". Also the X-server eats performance. So i started to dig into the kde and qt sources and looked for parts, that were X-dependant. Then i started porting the GPL'ed qt for x11 to use native Windows gdi calls. This project can be found at <a href="http://kde-cygwin.sourceforge.net/qt2-win32/">http://kde-cygwin.sourceforge.net/qt2-win32/</a>. Then i got a commercial Qt license for x11 and for Windows, so i am<br />
not allowed to continue working on the qt-win32 port. i guess i finished about half of it. All the drawing stuff works, only parts of the rest are missing.<br />
So now i am able to take the KDE sources and replace the X-dependant stuff by something that works under Windows or simply comment it out. There is only little functionality in kde that depends on X directly instead of the qt routines. it is mainly the handling of keyboard input (global shortcuts, ...), querying for screen properties ( x11AppDpiX() ... ) and the NETWM stuff.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>KDevelop 3.0 presentation</td>
<td>Harald Fernengel</td>
<td><a href="mailto:harry@kdevelop.org">&lt;harry@kdevelop.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">
KDevelop started as an ambitious project to bring an easy to master integrated development environment (IDE) to KDE. The upcomming release of KDevelop is a big milestone for the project. The code is a total rewrite and features a plugin-based language-independent infrastructure. A lot of work has been put into the new version making it one of the largest KDE application currently available.<br />
The presentation will feature developing with KDevelop as well as developing on KDevelop. Main points reach from project generation to handling the project management, version control, debugging as well as handling and customizing the IDE. The focus lies on C++/KDE development, but also touches scripting projects (perl, php, python) and other supported programming languages like ada, java or pascal. A brief overview of KDevelop's structure illustrates how easy it is to extend the IDE with additional features.<br />
Arrangement:
<ul>
<li>A glance at the IDE</li>
<li>Kicking off a new project</li>
<li>Project management with automake, qmake or ant</li>
<li>Handling version control systems</li>
<li>Debugging</li>
<li>Using the documentation browser</li>
<li>Extending the IDE</li>
</ul>
<p>Harald Fernengel<harry>, assisted by Roberto Raggi<br />
      <roberto><br />
      </roberto></harry> </p></td>
</tr>
<tr bgcolor="#eeeeee">
<td>The Knot -  A SLP-based Zeroconf solution</td>
<td>Tim Jansen</td>
<td><a href="mailto:%3Ctim@tjansen.de%3E">&lt;tim@tjansen.de&gt;</a></td>
</tr>
<tr>
<td colspan="3">
Zero Configuration Networking allows computers and other devices to communicate without any manual configuration. This means that computers and devices on the same network must assign themselves IP addresses, without the presence of a DHCP server. Systems must  broadcast their names on the network, and they must be able to announce the services that they offer.<br />
SLP (RFC 2608) is a proposed IETF-standard for a extendable and scalable service location protocol. It can publish information about network services in the form of a URL bundled with a set of attributes. In small networks SLP can work without any servers as a peer-to-peer system. In larger networks one or more servers need to be deployed. Clients will discover and use these servers automatically.<br />
The Knot is an implementation of SLP with a focus on security and extendability. It is implemented in C++ using Qt/TinyQ and comes with a powerful KDE API. Several small extensions allow to use it for naming as well as File Sharing with existing protocols like NFS and<br />
SFTP.<br />
This talk gives an introduction to the concepts of Zeroconf and its implementations with a focus on SLP and presents the Knot. It will be assumed that the listener has basic TCP/IP knowledge.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>KWallet</td>
<td>George Staikos</td>
<td><a href="mailto:staikos@kde.org">&lt;staikos@kde.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">
KDE 3.2 will include a new subsystem for storing sensitive data such as passwords, web forms and certificates in strong encryption containers known as "wallets".  This behavior mirrors the behavior of implementations such as Mozilla's Wallet, Apple's Key Ring, and other similar subsystems.  It also extendes these concepts by implementing generic storage mechanisms and user  interfaces, allowing more complex data to be stored and shared amongst applications as well as allowing the user to easily manage the stored data and control which<br />
applications may access it.  This paper will outline the architecture of KWallet as well as discuss how and why application developers should take advantage of what KWallet has to offer.  It will include examples of applications presently using KWallet.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>Performance Analysis of GUI applications on LINUX</td>
<td>Josef Weidendorfer</td>
<td><a href="mailto:%3CJosef.Weidendorfer@in.tum.de%3E">&lt;Josef.Weidendorfer@in.tum.de&gt;</a></td>
</tr>
<tr>
<td colspan="3">
Optimizing performance in a LINUX application is a difficult task using the legacy profiling tools like gprof. Especially in the scope of GUI applications using shared libraries and dynamically opened plugins, these tools are difficult to use and often can't help at all. The result is, that in most cases, performance optimization doesn't happen at all. If there's an obvious performance problem, one has to estimate the location of the bottleneck or resort to handcrafted meassurement routines.<br />
Last year, a tool for instrumenting binary code on the fly was released for LINUX under the GPL: Valgrind. Using its technique first as a memory debugging, its astonishing features and ease of use made it quickly the number one tool in the open-source LINUX community. In contrast, Valgrinds usefullness for program execution analysis is less known, but likewise powerful. It's based on the cache simulation extension "cachegrind", delivering flat profiling on a source line level. But to get a real overview of the performance characteristics of your program, the  measured costs have to be attributed to functions or even libraries. Also, you want to see the cost of a function including the cost of all the functions called from there. This is especially important for programs doing the real work in third party libraries with long call chains like GUI applications. Thus, call tracing was added to "cachegrind", resulting in the tool "calltree".<br />
To make use of the huge amount of data delivered by this tool, one needs a powerful visualization, allowing for quick browsing at different granularities ranging from costs of instructions to summed up costs of a whole shared library. A tool delivering these features is KCachegrind,  likewise released under the GPL.<br />
In the paper, first I will give an overview of techniques and problems of performance profiling in general, the availability of tools for LINUX, and how the "calltree" extension of Valgrind comes into play here. Then, I will show the requirements for a visualization tool to enable successful performance analysis, together with the featureset of KCachegrind. The paper finishes by showing the typical use case of the toolchain calltree/KCachegrind together with a standard KDE application, and the steps involved for practical use in performance optimization.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>Accessing the internals of QObjects</td>
<td>Richard Moore</td>
<td><a href="mailto:rich@xmelegance.org">&lt;rich@xmelegance.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">QObjects form the core of Qt and KDE and provide a number of powerful facilities including the signal/slot mechanism, a navigable tree and properties. While the public APIs provided by Qt are enough for many applications, they are not sufficient for all purposes forcing us to use undocumented features to implement facilities such as the Qt-DCOP bridge and the KJSEmbed QObjectProxy class. This paper discusses the undocumented features that were used to create the QObjectProxy, and the difficulties that were faced. It will further discuss what would be required to create a reusable library that conceals these undocumented techniques behind a maintainable API making it practical to use them more widely in creating bindings to other scripting languages or communication systems.
      </td>
</tr>
<tr>
    </tr>
<tr bgcolor="#eeeeee">
<td>Qt - past - present - future</td>
<td>Matthias Ettrich</td>
<td><a href="mailto:ettrich@trolltech.com">&lt;ettrich@trolltech.com&gt;</a></td>
</tr>
<tr>
<td colspan="3">In my talk I am going to give an overview on how Qt developed in the past, what we at Trolltech have been doing lately and what our plans for the upcoming years are. The focus will be on topics relevant for KDE, such as performance, flexibility, and productivity.      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>My research about KDE</td>
<td>Andreas Brand</td>
<td><a href="mailto:A.Brand@em.uni-frankfurt.de">&lt;A.Brand@em.uni-frankfurt.de&gt;</a></td>
</tr>
<tr>
<td colspan="3">
Respearcher Andreas Brandt from the University of Frankfurt will present his results of a community study condicted at LinuxTag 2002.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>Development and Use of Scripting Interfaces for KDE Applications with DCOP</td>
<td>Ian Reinhart Geiser</td>
<td><a href="mailto:geiseri@kde.org">&lt;geiseri@kde.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">For almost as long as Unix has existed the shell script has been a integral part of the system.  These shell scripts automate and control the system and allow users to encode complex tasks into a simple program.  Over the course of evolution of the Unix Desktop the GUI (Graphical User Interface) has made scripting Unix applications much more difficult.  One solution to this problem in the KDE (K Desktop Environment)1 project was to use DCOP (Desktop Communications Object Protocol)2.  DCOP allows developers to export functionality of their application to be controlled from external applications.   This can allow users to automate GUI applications without the need of mouse event recording and custom event recorders.<br />
The two main focuses of this paper will consist of the development of useful scripting interfaces for automation and the use of them from the system shell and python.  The first portion will deal with using proxy classes in KDE Libraries to export complex functionality from widgets to the outside world.  It will also deal with issues of how to effectively provide interfaces for scripters to use.  The second portion will cover how to effectively use dcop interfaces from shell and python.  It will also discuss the use of KScriptInterface3 script engines to provide macro facilities to applications.<br />
1) More information can be found at <a href="http://www.kde.org">http://www.kde.org</a><br />
2) DCOP Information can be found at <a href="http://developer.kde.org/documentation/library/kdeqt/dcop.html">http://developer.kde.org/documentation/library/kdeqt/dcop.html</a><br />
3) KScriptInterface Developer information can be found at <a href="http://developer.kde.org/documentation/library/3.1-api/classref/interfaces/KScriptInterface.html">http://developer.kde.org/documentation/library/3.1-api/classref/interfaces/KScriptInterface.html</a>
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>TaskJuggler</td>
<td>Chris Schlaeger</td>
<td><a href="mailto:cs@suse.de">&lt;cs@suse.de&gt;</a></td>
</tr>
<tr>
<td colspan="3">The talk is an introduction to the TaskJuggler Project Management Software. It will describe the concept and ideas behind TaskJuggler and then continue with a brief overview of the project description language. Basic topics like task hierachies, resource definitions, resource allocations, vactions, and shifts will be covered.<br />
Using a made-up software development project as an example, the whole workflow of project planing, project tracking and profit and loss analysis will illustrated. Finally report generation and XML exporting will be explained.<br />
The talk is intended for everybody that is involved with planing and tracking of projects. More info can be found at <a href="http://www.taskjuggler.org">http://www.taskjuggler.org</a>.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>The KDE Build system</td>
<td>Stephan Kulow</td>
<td><a href="mailto:%3Ccoolo@kde.org%3E">&lt;coolo@kde.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">
As technologies like <a href="http://distcc.samba.org">distcc</a> and <a href="http://www.trolltech.com/products/teambuilder">TeamBuilder</a> came up, it became clear that automake hinders us in getting the best ouf ot these tools. The other problem with automake we have is the need for am_edit to get our developers to code instead of fiddling with Makefiles.<br />
My talk will <span style="font-style: italic;">shortly</span> introduce the way our build system is set up and then I'll go on giving you an idea why unsermake is such a great replacement for automake and why do you want to use it even on single machines.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>A KDE Usability Study -- Goals, Results and Future Challenges</td>
<td>Eva Brucherseifer</td>
<td><a href="mailto:%3Ceva@basyskom.de%3E">&lt;eva@basyskom.de&gt;</a></td>
</tr>
<tr>
<td colspan="3">
KDE's rich and continously growing set of applications as well as its exemplary stability and performance offer the chance for traditional Windows users in administrative and business environments to migrate to Linux not only in the server but especially also in the desktop area.<br />
At the moment we face the beginning of such a transition process and there are already a number of promising examples for it.<br />
However, this situation raises the demand for an in depth comparison of usability concepts between KDE and traditional Windows desktops. Our paper describes activities and results from a task-based study, which tried to gain new insights in this domain. The test was designed, prepared, carried out and analyzed in June/July 2003 with the german company relevantive AG as project lead and basysKom GbR as technical partner who took specific care for the KDE specific configuration aspects.<br />
The paper discusses the intentions of a test with a significant number of "traditional Windows users" who got the chance to solve a number of standard office tasks on a KDE system as well as on a Windows XP system. The users behavior was filmed and analyzed afterwards.<br />
We show the way we have configured the KDE desktop to meet the expected demands in a proper way, explain our intentions and rationales and evaluate the results of this study. The paper raises a number of questions that we consider worth observing for future development and<br />
indicates the great challenges but especially opportunities that result from the migration of large installations from Windows to Linux systems in business environments.  A general trend in this direction can be expected for the near future.<br />
Web Reference: <a href="http://www.relevantive.de/linux">linux usability study</a>
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>KStreamer in KDE-4.0</td>
<td>Ronald Bultje </td>
<td><a href="mailto:%3Crbultje@ronald.bitfreak.net%3E">&lt;rbultje@ronald.bitfreak.net&gt;</a></td>
</tr>
<tr>
<td colspan="3">
GStreamer is a full-featured media framework, currently included in the stable Gnome-2.2 desktop series. KStreamer is the nickname for the KDE/Qt bindings for GStreamer, currently used in KDE-applications like JuK. This paper will aim to give a demo/overview of several things that are possible with this powerful framework, how GStreamer is technically built up, and how KStreamer could become the basis of a new media framework in KDE-4.0.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>Design Patterns</td>
<td>Marc Mutz </td>
<td><a href="mailto:mutz@kde.org">&lt;mutz@kde.org&gt;</a></td>
</tr>
<tr>
<td colspan="3">Design pattern have greatly influenced the way object-oriented programmers talk about application designs. In this paper, we will present examples of design pattern usage in and around KDE. Starting with well-known Factory and Singleton patterns, we will also cover more recently formulated design patterns such as Null Object and less well-known, but oft-used patterns such as State/Strategy and Observer.<br />
Target audience: Everyone from the "I know what a Factory is, but what the heck is a Null Object?" department. ;-)<br />
References:<br />
Erich Gamma,  Richard Helm, Ralph Johnson, John, Vlissides<br />
"Design Patterns - Elements of Reusable Object-Oriented Software",<br />
416 pages, Addison-Wesley, 1997<br />
  </td>
</tr>
<tr bgcolor="#eeeeee">
<td>Intel's Hyper-Threading Technology for Linux</td>
<td>Heinz Bast</td>
<td><a href="mailto:heinz.bast@intel.com">&lt;heinz.bast@intel.com&gt;</a></td>
</tr>
<tr>
<td colspan="3">End of 2002, Intel® introduced Hyper-Threading Technology (HT) as a new  hardware implementation for Simultaneous Multi-Threading (SMT) - two  threads can share physical processor resources at the very same time. This  is now available in all high-end desktop Pentium®-4  and all XEON® processors.<br />
 The talk will start by a brief update on Intel's desktop processor roadmap  including the soon to be released Prescott CPU. Next the Hyper-Threading Technology will be presented in detail focusing on generic implications for developers interested in porting and tuning applications for the new features offered. For the Linux Operating System, the enhancements and optimizations in the scheduler already available in current releases and those planned for future releases (kernel 2.5.x/2.6) will be discussed. Additionally the Linux APIs to detect support for HT and to facilitate programming for HT will be introduced.<br />
 The second part of the talk will give an overview on Intel's tools to support developing for Linux focusing on the support for Hyper-Threading. This will include the Intel Linux compilers, the Vtune Performance Analyzer and the Intel Performance Libraries.
      </td>
</tr>
<tr bgcolor="#eeeeee">
<td>freedesktop.org/GNOME</td>
<td>Havoc Pennington</td>
<td><a href="mailto:hp@redhat.com">&lt;hp@redhat.com&gt;</a></td>
</tr>
<tr>
<td colspan="3">This talk will be about building infrastructure (including  specifications and implementation) that's common to all desktops,  toolkits, and applications for the X Window System. What progress has been made?  What's in progress? What *should* be in progress? Why should we care - when does it matter to users? freedesktop.org has been a center for work in this area.<br />
To build common infrastructure, first we have to understand the various toolkits and applications that currently make up the open source desktop community. Thus, part of this talk will describe the state of the GNOME desktop infrastructure, including both the positives and the negatives. Where would GNOME developers be most interested in making improvements? What lessons can we learn from GNOME's experiences, about what to do and what not to do?<br />
This talk will have some technical content, but is primarily a high-level overview.</td>
</tr>
<tr bgcolor="#eeeeee">
<td>D-BUS</td>
<td>Havoc Pennington</td>
<td><a href="mailto:hp@redhat.com">&lt;hp@redhat.com&gt;</a></td>
</tr>
<tr>
<td colspan="3">
D-BUS is a message system, a simple way for applications to talk to one another. D-BUS supplies both a secure system daemon (for events such as "new hardware device added" or "printer queue changed") and a per-login-session daemon (for IPC among user applications). The message bus daemon is built on top of a general one-to-one message passing framework, which can be used by any two apps to communicate directly without involving the daemon.  D-BUS can even be used for secure IPC across an intranet or the Internet.<br />
This talk will present the technical details of D-BUS as it currently stands. Hopefully, it will inspire discussion about the potential for using D-BUS in the KDE Project, and how to improve D-BUS with that goal in mind.
      </td>
</tr>
</tbody>
</table>
</section>
