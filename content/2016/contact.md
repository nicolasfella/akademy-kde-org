---
title: Contact
menu:
  "2016":
    weight: 4
hideSponsors: true
---

Akademy is organized by [KDE e.V](http://ev.kde.org/).

To contact the Akademy organizing team, please email [The Akademy
Team](mailto:akademy-team@kde.org). For other matters, contact the [KDE e.V.
Board](mailto:kde-ev-board@kde.org).

The IRC channel for the event is [#akademy on
freenode](https://webchat.freenode.net/?channels=%23akademy&uio=d4) or via our
[Akademy Attendees Telegram group](https://telegram.me/KDEAkademy) which are
bridged together. You are welcome to sit in the channel or just pop in and ask
questions. You may not always get a quick answer as the channel is not
monitored constantly so please stick around for an answer.
