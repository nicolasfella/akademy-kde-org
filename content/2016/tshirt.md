---
title: Order Akademy 2016 T-shirt
menu:
  "2016":
    weight: 3
hideSponsors: true
---

<div style="float:right; text-align: center; padding: 1ex; margin: 1ex;
width: 250px; border: 1px solid grey;">
<a href="/media/2016/akademy2016-tshirt.png">
<img src="/media/2016/akademy2016-tshirt_wee.png" /></a><br />
<img src="/media/2016/akademy2016-tshirt-sleeve.png" /><br />
<a href="/media/2016/akademy2016-hoodie.png">
<img src="/media/2016/akademy2016-hoodie_wee.png" /></a><br />
<em>Mockup of Akademy t-shirt &amp; hoodie</em>
</div>

<p>This year's design was created by Jens Reuterberg and is inspired by the 
<a href="https://en.wikipedia.org/wiki/Trabant">Trabant</a></p>
<p>The t-shirt also has  a small print on the sleeve saying Akademy 2016 while
the hoodies don't</p>
<!--<p><strong>Ordering is no longer available. However there will be a small 
number of t-shirts available for anyone who has not ordered.</strong></p>-->

<p>Following on from previous years we have the option for buying an official
Akademy 2016 t-shirt. Attendees can order a t-shirt, pick it up at Akademy and
pay a reduced price.</p>

<p>It's hard for the team to guess how many of each size shirt to order.
Sometimes people aren't able to get the right size because they have sold out,
and we often end up with leftovers of other sizes.</p>

<p>If you are definitely coming to QtCon &amp; Akademy 2016, you can order the
size that you want. Then pick it up during the conference.</p>

<p>Order your t-shirts now for a reduced price of 18€, normally 20€. While the
hoodies are 35€ or 40€ at the conference.</p>
<p>To <strike><strong><a href="https://merch.qtcon.org/" target="_blank">
order your t-shirt and or hoodie</a></strong></strike>, select the option in
the registration system.</p>

<p><strong>Orders are open till the 28th of July</strong></p>

<p><em>For those of you who would incur high transaction fees purchasing via
card please email the <a href="mailto:akademy-team@kde.org">Akademy Team</a>
to reserve items instead.</em></p>

<p>If you don't know whether or not you will be at Akademy, or if you don't
want to pre-order, there will be a limited number of t-shirts available to
buy at the event for 20€.</p>

<p>The fitted style is suitable for some women, while others may prefer to
wear the unisex style.</p>

<p>This years t-shirts are slightly smaller models than last year, so some
people may want to order the next size up. The approximate sizes of the
t-shirts are:</p>

<p style="clear:both"><strong>Unisex</strong><br />
<img src="/media/2016/TMD70.jpg" width="90%"/>
</p>

<p><strong>Fitted</strong><br />
<img src="/media/2016/TWD71.jpg" width="90%"/>
</p>

<p><strong>For the hoodies as we expect to only have a smaller quantity
ordered they are only available currently in M or XL</strong><br />
<img src="/media/2016/WMD24.jpg" width="90%"/>
</p>
