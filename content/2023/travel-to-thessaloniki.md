---
title: Travel to Thessaloniki
menu:
  "2023":
    parent: travel
    weight: 1
---
Before planning your trip if you are coming from outside Greece please check the *[Greek Government Travel website](https://travel.gov.gr/ "https\://travel.gov.gr/")* for more information.

You can travel to Thessaloniki by airplane, boat, train, bus or car.

### By Plane

Frequent flights by many airlines land at the Macedonia Airport (IATA code is SKG).

Thessaloniki-Macedonia Airport has connections to many cities of the world. For more detailed information about the airport check out *[https://www.skg-airport.gr/en/](https://www.skg-airport.gr/en/ "https\://www.skg-airport.gr/en/")*

From the airport you can travel to the city:

* **By Bus:**

  Line 01X (from Airport to Intercity Bus Terminal (via city centre): *[https://oasth.gr/#en/routeinfo/list/61/12/73/](https://oasth.gr/#en/routeinfo/list/61/12/73/ "https\://oasth.gr/#en/routeinfo/list/61/12/73/")*). The ticket costs 2.00€.

* **By Taxi:**

  The total cost depends on your destination (there are day/night/weekend/holidays fares). There is also a supplement for airport and luggage. A typical ride from/to the Airport to/from the city centre can be estimated between 30-40€ (we advise you to share rides).

### By Car (within Europe)

Be aware of the speed limits. Parking your car will be quite difficult, but there are some car parks where you can park your car for a few euros.

### By Boat (harbor Thessaloniki)

### By Bus (from neighboring countries)

* Intercity Bus Terminal *[https://ktelmacedonia.gr/en/routes/intmap/](https://ktelmacedonia.gr/en/routes/intmap/ "https\://ktelmacedonia.gr/en/routes/intmap/")*
* Flixbus *[https://global.flixbus.com/bus/thessaloniki](https://global.flixbus.com/bus/thessaloniki "https\://global.flixbus.com/bus/thessaloniki")*
* Getbybus: *[https://getbybus.com/en/](https://getbybus.com/en/ "https\://getbybus.com/en/")*

### By Train

Check the website *[https://www.hellenictrain.gr/en](https://www.hellenictrain.gr/en "https\://www.hellenictrain.gr/en")*
