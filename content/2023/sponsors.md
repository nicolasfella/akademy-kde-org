---
title: Sponsors
menu:
  "2023":
    weight: 5
---
## Sponsorship Opportunities

A big thank you to our sponsors who help to make this event happen! There are still sponsorship opportunities available.
If you are interested, please see [Sponsoring Akademy](/2023/sponsoring) for more information, including valuable sponsor benefits.

## Sponsors for Akademy 2023

### Platinum

{{< sponsor homepage="https://www.qt.io" img="/media/2023/Qt-Group-logo-white.png" >}}

<p id="tqc"><strong>Qt Group</strong> is responsible for Qt development, productization and licensing under commercial and open-source licenses. Qt is a C++ based framework of libraries and tools that enables the development of powerful, interactive and cross-platform applications and devices. Used by over a million developers worldwide, Qt’s support for multiple desktop, embedded and mobile operating systems allows users to save significant time related to application and device development by simply reusing one code. Qt and KDE have a long history together, something Qt Group values and appreciates. Code less. Create more. Deploy everywhere.<p>

<https://www.qt.io> and <https://qt-project.org>

{{< /sponsor >}}

{{< sponsor homepage="https://www.codethink.co.uk" img="/media/2023/codethink.png" >}}

<p id="codethink"><strong>Codethink</strong> specialises in system-level Open Source software infrastructure to support advanced technical applications, working across a range of industries including finance, automotive, medical, telecoms. Typically we get involved in software architecture, design, development, integration, debugging and improvement on the deep scary plumbing code that makes most folks run away screaming.<p>

<https://www.codethink.co.uk>

{{< /sponsor >}}

### Silver

{{< sponsor homepage="https://www.kdab.com/" img="/media/2020/kdab3.png" >}}

<p id="kdab"><strong>KDAB</strong> is the world's leading software consultancy for architecture, development and design of Qt, C++ and OpenGL applications across desktop, embedded and mobile platforms. The biggest independent contributor to Qt, KDAB experts build run-times, mix native and web technologies, solve hardware stack performance issues and porting problems for hundreds of customers, many among the Fortune 500. KDAB’s tools and extensive experience in creating, debugging, profiling and porting complex, great looking applications help developers worldwide to deliver successful projects. KDAB’s global experts, all full-time developers, provide market-leading training with hands-on exercises for Qt, OpenGL and modern C++ in multiple languages.<p>

<https://www.kdab.com>

{{< /sponsor >}}

{{< sponsor homepage="https://www.canonical.com" img="/media/2022/ubuntu.png" >}}

<p id="canonical"><strong>Canonical</strong> is the publisher of Ubuntu, the OS for most public cloud workloads as well as the emerging categories of smart gateways, self-driving cars and advanced robots. Canonical provides enterprise security, support and services to commercial users of Ubuntu. Established in 2004, Canonical is a privately held company.</p>

<https://www.canonical.com>

{{< /sponsor >}}

{{< sponsor homepage="https://collabora.com" img="/media/2022/collabora2022.png" >}}

<p id="collabora"><strong>Collabora</strong>  is  a leading global consultancy specializing in delivering the benefits of Open Source software to the commercial world. For over 15 years, we've helped clients navigate the ever-evolving world of Open Source, enabling them to develop the best solutions – whether writing a line of code or shaping a longer-term strategic software development plan. By harnessing  the potential of community-driven Open Source projects, and re-using existing components, we help our clients reduce time to market and focus on creating product differentiation.<p>

<https://collabora.com>

{{< /sponsor >}}

### Bronze

{{< sponsor homepage="https://www.opensuse.org" img="/media/2015/opensuse.png" >}}

<p id="suse"><strong>The openSUSE project</strong> is a worldwide effort that promotes the use of Linux everywhere. openSUSE creates one of the world's best Linux distributions, working together in an open, transparent and friendly manner as part of the worldwide Free and Open Source Software community. The project is controlled by its community and relies on the contributions of individuals, working as testers, writers, translators, usability experts, artists and ambassadors or developers. The project embraces a wide variety of technology, people with different levels of expertise, speaking different languages and having different cultural backgrounds.

<https://www.opensuse.org>

{{< /sponsor >}}

### Supporter

{{< sponsor homepage="https://haute-couture.enioka.com/en" img="/media/2023/enioka.svg" >}}

**enioka Haute Couture**: Businesses of all kinds have become highly dependent on their IT systems. Yet, many of them have lost control of this essential part of their assets. Enioka Haute Couture specializes in mastering complex software development, whether it's due to the domain, organization, or timing of the project. We work closely with your teams to build software tailored to your needs. If we're developing it ourselves, we go the extra mile to ensure you keep control of it. We also help your teams adopt the right organization, technologies, and tools. Finally, whenever appropriate, we can help you set up free and open-source software projects or interact with existing ones. With Enioka Haute Couture, you have a trusted provider who facilitates your development needs while keeping full control of your systems. No more vendor or service provider lock-in.

<https://haute-couture.enioka.com/en>

{{< /sponsor >}}

{{< sponsor homepage="https://kde.slimbook.es" img="/media/2022/slimbook2022.png" >}}

**SLIMBOOK** has been in the computer manufacturing business since 2015. We build computers tailored for Linux environments and ship them worldwide. Our main goal is to deliver quality hardware with our own apps combined with an unrivaled tech support team to improve the end-user experience. We also firmly believe that not everything is about the hardware. SLIMBOOK has been involved with the community from the beginning, taking on small local projects to bring the GNU/Linux ecosystem to everyone. We have partnered with state-of-the-art Linux desktops like KDE, among other operating systems. And of course, we have our very own Linux academy, "Linux Center," where we regularly impart free Linux/FOSS courses for everyone. If you love Linux and need quality hardware to match, BE ONE OF US.

<https://kde.slimbook.es>

{{< /sponsor >}}

{{< sponsor homepage="https://www.tuxedocomputers.com" img="/media/2023/tuxedo.png" >}}

**TUXEDO**: The name TUXEDO Computers unites both the demands and the product range: while it's name is a tailor-made suit, it also contains the name of the Linux mascot Tux. TUXEDO Computers are not only Linux hardware in a tailor-made suit, they are immediately recognizable as such by their name. Only where TUXEDO is written on it, there is Linux hardware in a tailor-made suit inside! They have been producing individual computers and laptops for nearly two decades now - all pre-installed with Linux! TUXEDO Computers offers their in house built, maintained and developed Linux distribution TUXEDO OS with KDE Plasma as its desktop environment. This offer is complemented by gratis technical support and services around Linux. TUXEDO Computers select all components suitable for the operation with Linux and assemble the devices in Germany. Afterwards all TUXEDOs are delivered in a way that you are ready to start immediately!

<https://www.tuxedocomputers.com>

{{< /sponsor >}}

{{< sponsor homepage="https://pine64.org" img="/media/2021/pine2021.png" >}}

**PINE64** is a community driven project that creates Arm and RISC-V devices for open source enthusiasts and industry partners. Perhaps best known for the PinePhone, our Linux-only smartphone, and the Pinebook range of laptops, we strive to deliver devices that you want to use and develop for. Rather than applying business to a FOSS setting, we allow FOSS principles to guide our business.

<https://pine64.org>

{{< /sponsor >}}

{{< sponsor homepage="https://extenly.com" img="/media/2023/extenly.png" >}}

**Extenly** was founded in 2019 and since then it counts several happy customers and successful products in the market. We are Qt experts focusing on embedded UI and cross-platform apps development providing top quality re-usable code with best performance using open source tools and frameworks. Among our services are, UI/UX Design, Qt/QML UI development, rapid prototyping, consultancy, Qt/QML trainings. We can teach and/or help our clients to create scalable UI applications which can be easily alternated and used on top of different systems and platforms saving them time and money.

<https://extenly.com>

{{< /sponsor >}}

### Media Partners

{{< sponsor homepage="https://bit.ly/Linux-Update" img="/media/2018/linuxmagazine.png" >}}

**Linux Magazine** is your guide to the world of Linux and open source. Each monthly issue includes advanced technical information you won't find anywhere else including tutorials, in-depth articles on trending topics, troubleshooting and optimization tips, and more! Subscribe to our free newsletters and get news and articles in your inbox.

https://bit.ly/Linux-Update

{{< /sponsor >}}

### Hosted by

{{< sponsor homepage="https://www.uom.gr/en" img="/media/2023/UOM_logo_new_ENG300.JPG" >}}

**University of Macedonia**  
https://www.uom.gr/en

{{< /sponsor >}}

{{< sponsor homepage="https://opensource.uom.gr/" img="/media/2023/open-source-pamak-logo4.png" >}}

**Open Source UoM**  
https://opensource.uom.gr/

{{< /sponsor >}}

### KDE Patrons
[KDE Patrons](http://ev.kde.org/supporting-members.php) also support the KDE Community throughout the year.
