---
title: Welcome event - Friday
menu:
  "2023":
    parent: program
    weight: 2
---

**Place:** Mponmponella, Leof. Kon/nou Karamanli 21, Thessaloniki 546 38, Greece <br>
**Date:** Friday, 14 July 2023 <br>
**Time:** 20:00 - 0:00 <br>

Meet fellow attendees in a relaxed cute bar to get ready for Akademy. You will be able to collect your name badge and get to know other attendees.

You will receive a token for a drink (refreshment, beer, wine or Vodka/Gin). There will be some burgers & baguettes available.

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Event",
  "endDate": "2023-07-15T00:00:00+03:00",
  "location": {
    "@type": "Place",
    "address": {
      "@type": "PostalAddress",
      "addressCountry": "GR",
      "addressLocality": "Θεσσαλονίκη",
      "postalCode": "546 38",
      "streetAddress": "Λεωφόρος Κωνσταντίνου Καραμανλή 21"
    },
    "geo": {
      "@type": "GeoCoordinates",
      "latitude": 40.6201735,
      "longitude": 22.9627749
    },
    "name": "Μπομπονέλλα"
  },
  "name": "KDE Akademy 2023 Welcome Event",
  "startDate": "2023-07-14T20:00:00+03:00",
  "url": "https://akademy.kde.org/2023/welcomeevent/"
}
</script>
