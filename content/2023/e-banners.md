---
title: E-Banners
menu:
  "2023":
    parent: details
    weight: 4
---
### Akademy E-Banners are here!

This year's official banners for people going to #Akademy2023 were created by
Andy B.

Feel free to use it in your blog posts, on your website, as a header image on
your social media profiles, or wherever you want to promote Akademy and let
people know you'll be attending!

This year you can choose from 4 different sizes.

If you use any of the banners on your site or blog, you can link it to the
Akademy website and help us attract more visitors.

<div style="float: left; padding: 1ex; margin: 1ex; width: 728px;">
  <a href="/media/2023/Akademy2023728x90.png">
    <img src="/media/2023/Akademy2023728x90.png"
    alt="Banner of size 728 pixels wide by 90 pixels high with the words
         'GOING TO AKADEMY Thessaloniki, Greece' on the left of a drawing
         of a wing">
  </a>
</div>

<div style="float: left; padding: 1ex; margin: 1ex; width: 600px;">
  <a href="/media/2023/Akademy2023600x160.png">
  <img src="/media/2023/Akademy2023600x160.png"
  alt="Banner of size 600 pixels wide by 160 pixels high with the words
      'GOING TO AKADEMY 2023 Thessaloniki, Greece' on the right of a 
       drawing of a face">
  </a>
</div>

<div style="float: left; padding: 1ex; margin: 1ex; width: 300px;">
  <a href="/media/2023/Akademy2023300x500.png">
  <img src="/media/2023/Akademy2023300x500.png"
  alt="Banner of size 300 pixels wide and 500 pixels high with the words
  'GOING TO AKADEMY 2023 Thessaloniki, Greece' below a drawing of a face">
  </a>
</div>

<div style="float: left; padding: 1ex; margin: 1ex; width: 110px;">
  <a href="/media/2023/Akademy2023110x600.png">
  <img src="/media/2023/Akademy2023110x600.png"
  alt="Banner of size 110 pixels wide and 600 pixels high with the words
  'GOING TO AKADEMY 2023' next to a drawing of a tower">
  </a>
</div>
