---
title: Sponsors
menu:
  "2019":
    weight: 6
---

## Sponsorship Opportunities

**A big thank you to our sponsors who help to make this event happen! There
are still sponsorship opportunities.  
If you are interested, please see [Sponsoring Akademy 2019](/2019/sponsoring)
for more information, including valuable sponsor benefits.**

## Sponsors for Akademy 2019

### Platinum

{{< sponsor homepage="https://www.qt.io/" img="/media/2018/tqtc_0.png" >}}

<p id="tqtc"><strong>The Qt Company</strong> (NASDAQ
 OMX Helsinki: QTCOM) is responsible for Qt development, productization 
and licensing under commercial and open-source licenses. Qt is a C++ 
based framework of libraries and tools that enables the development of 
powerful, interactive and cross-platform applications and devices.
<p>Used by over a million developers worldwide, Qt is a C++ based 
framework of libraries and tools that enables the development of 
powerful, interactive and cross-platform applications and devices. Qt’s 
support for multiple desktop, embedded and mobile operating systems 
allows developers to save significant time related to application and 
device development by simply reusing one code. Qt and KDE have a long 
history together, something The Qt Company values and appreciates. Code 
less. Create more. Deploy everywhere.<p>

<p>The Future is Written with Qt</p>

<https://www.qt.io/>

{{< /sponsor >}}

### Gold

{{< sponsor homepage="https://blue-systems.com/" img="/media/2019/bluesystems2.png" >}}

<p id="bluesystems"><strong>Blue Systems</strong>
 is a company investing in Free/Libre Computer Technologies. It sponsors
 several KDE projects and distributions like Netrunner. Their goal is to
 offer solutions for people valuing freedom and choice.</p>

{{< /sponsor >}}

### Silver

{{< sponsor homepage="https://www.cannonical.com" img="/media/2018/ubuntu_transparent.png" >}}

<p id="canonical"><strong>Canonical</strong> is
 the company behind Ubuntu, the leading OS for cloud operations. Most 
public cloud workloads use Ubuntu, as do most new smart gateways, 
switches, self-driving cars and advanced robots. Canonical provides 
enterprise support and services for commercial users of Ubuntu. 
Established in 2004, Canonical is a privately held company.</p>

<https://www.canonical.com/>

{{< /sponsor >}}

### Bronze

{{< sponsor homepage="https://www.kdab.com" img="/media/2019/kdab2.png" >}}

<p id="kdab"><strong>KDAB</strong> is the world's 
leading software consultancy for architecture, development and design of
 Qt, C++ and OpenGL applications across desktop, embedded and mobile 
platforms. The biggest independent contributor to Qt, KDAB experts build
 run-times, mix native and web technologies, solve hardware stack 
performance issues and porting problems for hundreds of customers, many 
among the Fortune 500. KDAB’s tools and extensive experience in 
creating, debugging, profiling and porting complex, great looking 
applications help developers worldwide to deliver successful projects. 
KDAB’s global experts, all full-time developers, provide market leading 
training with hands-on exercises for Qt, OpenGL and modern C++ in 
multiple languages.</p>

<https://www.kdab.com/>

{{< /sponsor >}}

{{< sponsor homepage="https://www.opensuse.org" img="/media/2015/opensuse.png" >}}

<p id="opensuse"><strong>The openSUSE project</strong>
 is a worldwide effort that promotes the use of Linux everywhere. 
openSUSE creates one of the world's best Linux distributions, working 
together in an open, transparent and friendly manner as part of the 
worldwide Free and Open Source Software community. The project is 
controlled by its community and relies on the contributions of 
individuals, working as testers, writers, translators, usability 
experts, artists and ambassadors or developers. The project embraces a 
wide variety of technology, people with different levels of expertise, 
speaking different languages and having different cultural backgrounds. 
Learn more at:</p>

<https://www.opensuse.org/>

{{< /sponsor >}}

{{< sponsor homepage="https://gitlab.com" img="/media/2019/gitlab.png" >}}

<p id="gitlab"><strong>GitLab</strong> is a DevOps
 platform built from the ground up as a single application for all 
stages of the DevOps lifecycle enabling Product, Development, QA, 
Security, and Operations teams to work concurrently on the same project.
 GitLab provides teams a single data store, one user interface, and one 
permission model across the DevOps lifecycle allowing teams to 
collaborate and work on a project from a single conversation, 
significantly reducing cycle time and focus exclusively on building 
great software quickly. Built on Open Source, GitLab leverages the 
community contributions of thousands of developers and millions of users
 to continuously deliver new DevOps innovations. More than 100,000 
organizations from startups to global enterprise organizations, 
including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network and 
Comcast trust GitLab to deliver great software at new speeds.</p>

<https://gitlab.com>

{{< /sponsor >}}

### Supporters

{{< sponsor homepage="https://www.codethink.co.uk" img="/media/2018/codethink.png" >}}

<p id="codethink">In <strong>Codethink</strong>
 we specialise in system-level Open Source software infrastructure to 
support advanced technical applications, working across a range of 
industries including finance, automotive, medical, telecoms. Typically 
we get involved in software architecture, design, development, 
integration, debugging and improvement on the deep scary plumbing code 
that makes most folks run away screaming.</p>

<https://www.codethink.co.uk/>

{{< /sponsor >}}

{{< sponsor homepage="https://slimbook.es" img="/media/2018/slimbook.png" >}}

<p id="slimbook"><strong>Slimbook</strong> is a 
computer brand from Spain with Free Software vocation. Our products have
 consistently been designed and tested to run GNU/Linux and, through 
cooperation, provided among the best experience we can find in the 
market. In this fashion, we collaborated with KDE and created the KDE 
Slimbook laptop to make sure we have a good laptop that is today an 
example of what we can achieve working together. See you at Akademy!</p>

<https://slimbook.es/>

{{< /sponsor >}}

{{< sponsor homepage="https://pine64.org/" img="/media/2018/pine.png" >}}

<p id="pine64"><strong>PINE64</strong> is a 
community driven project striving to deliver open source ARM64 devices 
to tinkerers, developers as well as education and business sectors. 
PINE64’s history began with a successful Kickstarter campaign 2015 of 
its original PINE A64(+) single board computer. The project has since 
grown in both scope and scale, and this year will introduce the Pinebook
 Pro, an ARM daily-driver laptop, as well as the PinePhone - a 
Linux-only smartphone. The core purpose of PINE64 is to provide the 
community with affordable and well-documented devices that everyone can 
use, develop for and apply in a multitude of scenarios.</p>

<https://pine64.org/>

{{< /sponsor >}}

{{< sponsor homepage="https://openinventionnetwork.com" img="/media/2019/openinventionnetwork.svg" >}}

<p id="oin"><strong>Open Invention Network</strong> 
is the largest patent non-aggression community in history and supports 
freedom of action in Linux as a key element of open source software. 
Launched in 2005 and funded by Google, IBM, NEC, Philips, Red Hat, Sony,
 SUSE, and Toyota, OIN has more than 2,000 community members and owns 
more than 1,100 global patents and applications. The OIN patent license 
and member cross-licenses are available royalty free to any party that 
joins the OIN community. Joining OIN helps provide community members 
with patent freedom of action in Linux and adjacent open source 
technologies, free of charge or royalties.</p>

<https://openinventionnetwork.com>

{{< /sponsor >}}

{{< sponsor homepage="https://mycroft.ai" img="/media/2018/mycroft_0.png" >}}

<p id="mycroft"><strong>Mycroft</strong> The 
world's first Open Source Voice Assistant. We're building a voice AI 
that runs anywhere and interacts exactly like a person. Mycroft has 
products for developers, consumers, and enterprises who demand their 
technology be Private, Customizable, and Open.</p>

<https://mycroft.ai/>

{{< /sponsor >}}

{{< sponsor homepage="https://mbition.io" img="/media/2019/mbition.png" >}}

<p id="mbiton"><strong>MBition</strong> is a 100% 
owned subsidiary of Daimler AG and focuses on Infotainment-Software, 
Navigation-Software, Cloud-Software and UI-Software. By combining the 
spirit and flexibility of a startup and the resources of a global active
 OEM, MBition develops advanced digital solutions. MBition's innovative 
products lead to disruptive changes in the automotive industry!</p> 

<https://mbition.io/en/home/>

{{< /sponsor >}}

### VPN Provider

{{< sponsor homepage="https://www.privateinternetaccess.com" img="/media/2019/pia.png" >}}

<p id="pia"><strong>Private Internet Access (PIA)</strong>
 is the leading no-log VPN service provider in the world. PIA believes 
that access to an open internet is a fundamental human right and donates
 effusively to causes such as EFF, ORG, and FFTF to promote privacy 
causes internationally. PIA has over 3,200 servers in 25 countries that 
provide reliable, encrypted VPN tunnel gateways for whatever the use 
case.</p>

<https://www.privateinternetaccess.com>

{{< /sponsor >}}

### Hosted by

{{< sponsor homepage="https://www.unimib.it" img="/media/2019/unimib.png" >}}

<p id="unimib"><strong>University of Milano-Bicocca</strong></p>

<https://www.unimib.it>

{{< /sponsor >}}

{{< sponsor homepage="https://github.com/unixMiB" img="/media/2019/unixmib.png" >}}

<p id="unixmib"><strong>unixMiB</strong></p>

<https://github.com/unixMiB>

{{< /sponsor >}}

### Patrons of KDE

[KDE's Patrons](http://ev.kde.org/supporting-members.php) also support the KDE
Community through out the year

