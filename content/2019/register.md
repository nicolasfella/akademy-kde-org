---
title: Register
menu:
  "2019":
    weight: 2
---

Akademy is the annual world summit of [KDE](https://www.kde.org), one of the
largest Free Software communities in the world. It is a free, non-commercial
event organized by the KDE Community.

The [conference](2019/program) is expected to draw hundreds of attendees from
the global KDE Community to discuss and plan the future of the Community and its
technology.  Many participants from the broad free and open source software
community, local organizations and software companies will also attend.

**Akademy is free to attend however you need to
[register](https://events.kde.org) to reserve your space**

*Akademy 2019 has a [Code of Conduct](code-conduct) to ensure the event is an
enjoyable experience for everyone. As a Community, we value and respect all
people, regardless of gender identity, sexual orientation, race, ability, shape,
size or preferred desktop environment. We will not tolerate vilification, abuse
or harassment in any form*

