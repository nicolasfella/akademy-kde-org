---
title: Getting around Milan
menu:
  "2019":
    weight: 33
    parent: travel
---

***Effective from the 15th July 2019, a new Integrated Fare System (STIBM) will
come into force replacing the previous Integrated Fare System of the Milanese
Area (SITAM). We invite you to look at
[New Integrated Fare System](https://nuovosistematariffario.atm.it/en/) page.***

Milan has always been a rich and important city. It has always been a place full
of various famous artists and offers a particular assortment of churches,
buildings and monuments. There was a change of culture and art in the
Renaissance with big a contribution in the period of the neoclassicism. Milan
offers a big variety of buildings, monuments, museum and so on...

Getting around Milan is very simple, and you are able to reach every part of the
city quickly.

The Akademy [Venue](/2019/venue) is in Bicocca

The public transport system in Milan consists of:

- Metro
- Trains
- Trams
- Buses

An overview plan of subway lines:
![Map of Milan metro network](/media/2019/schema_rete_metro.jpg)


Getting around Milan at night represent the best way to enjoy the city. Even if
subways close at midnight, they will be replaced by alternative buses.  
For more information about nightlines check [ATM
nightlines](https://www.atm.it/en/ViaggiaConNoi/Pages/NightService.aspx).

## Renting

Everywhere in Milan you are able to rent a:

- [Share&Go car](https://site.sharengo.it/en/)
- [DriveNow car](https://www.drive-now.com/it/en/milan)
- [CarToGo car](https://www.car2go.com/IT/en/)
- [Enjoy Eni car](https://enjoy.eni.com/en)

or ride a bike using:
- [ATM bikes](https://www.atm.it/en/ViaggiaConNoi/Bici/Pages/BikeMi.aspx) (you can find them in their hub)
- [Mobike car](https://mobike.com/it/)

or if you want to rent an electric scooter, you can use eCooltura reaching
unexplored but magical parts of Milan.

