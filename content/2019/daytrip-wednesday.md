---
title: Daytrip - Wednesday
menu:
  "2019":
    weight: 28
    parent: details
---

This years daytrip will be to Lake Como, we will be leaving after the mornings
BoFs etc.

Buses (free) will depart from near Milano Greco Pirelli Station at 1300 and drop
you off at via dell'Imbarcadero in Varenna (near the ferries).

The plan is for people to have a bit of an explore around Varenna, with an
optional walk up to Vezio Castle (4€) where as well as info about the history of
the area there are great views up and down the lake. Then you can go on the
ferry (4.60€ each way bought at the ticket office) across the lake, also with
great views, to Bellagio where you can take self guided tours, swin in the lake,
relax and have some food before getting the ferry back to Varenna to the buses.

The buses will return at 2230 from via dell'Imbarcadero to Milano Greco Pirelli
Station

*For those with low income (no proof of that required, just be honest. no shame
in this) we can help with the ferry tickets as well speak to Kenny D (in person,
seaLne on irc/tg or kenny:kde.org on matrix)*

