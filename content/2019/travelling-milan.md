---
title: Travelling to Milan
menu:
  "2019":
    weight: 31
    parent: travel
---

***Effective from the 15th July 2019, a new Integrated Fare System (STIBM) will
come into force replacing the previous Integrated Fare System of the Milanese
Area (SITAM). We invite you to look at [New Integrated Fare
System](https://nuovosistematariffario.atm.it/en/) page.***

You can travel to Milan [by airplane](#by-airplane), [by train](#by-train),
[by bus](#by-bus) and [by car](#by-car).  

## By Airplane
### To Milan Malpensa Airport (MXP)

Milan Malpensa Airport has connections to every part of the world. For more
detailed information about the airport check Milan Malpensa Airport website. It
represent the best destination instead of Linate (it will be close) and Orio al
Serio, cause it has a good transport network and it is easy to reach Milan.

From the airport you can travel to the city :

- by train 
  - Malpensa Express with destination of Central Milan (“Milano Centrale”).
    - You can take M3 subway to reach Duomo (4 stops)
    - You can take M3 subway to Zara (2 stops) and then M5 subway to Bicocca
(4 stops) if you want to reach the University of Milan - Bicocca.
  - Malpensa Express with destination of Central Milan (“Milano Centrale”) 
stopping at Milan Porta Garibaldi (“Milano Porta Garibaldi”).
    - You can take M5 subway to Bicocca (6 stops) if you want to
reach the University of Milan - Bicocca.
    - You can take any train with destination:
      - Paderno Robbiate (R14)
      - Lecco (S8)
      - Chiasso (S11)
      - Como S.G (S11)
Every train with this destination are 1 stop away from Milan Greco - Pirelli
Station which is few meters away from the University of Milan - Bicocca.
  - Price:
    - Malpensa Express ticket price starts from 8€ for the shortest
route. If you set the destination as Milan you can use it as an urban ticket
(valid on trains). For info check [Malpensa Express 
site](http://www.malpensaexpress.it/en).
    - Trenord urban ticket price is 1,50€ and it is valid only in the urban
area of Milan. Price will change for suburban areas. For infos check [Trenord 
website](http://www.trenord.it/en/).
    - ATM subway ticket price is 1,50€. It is valid only in the urban area of
Milan and also for trains (as always in the urban area of Milan). Price will
change for suburban areas. For infos check [ATM 
site](https://www.atm.it/en/Pages/default.aspx).
- by taxi:
  - fixed tariff per trip (including all surcharges, any number of people): 95€

## To International Airport of Orio al Serio (BGY)

Orio al Serio, also known as Milan Bergamo, is a little airport in Bergamo (50
km away from Milan). Even if some low-cost agency decide to land only there we
suggest to use Malpensa Airport for his big transport network. If you decide to
land in Orio al Serio you can take:

- OrioShuttle, a bus will take you to Central Milan Railway Station where:
  - You can take M3 subway to reach Duomo (4stops)
  - You can take M3 subway to Zara (2 stops) and then M5 subway to Bicocca
(4 stops)
- Price :
  - OrioShuttle tickets to Central Milan Railway Station cost 6€ and it takes 50
minutes. You can buy it on
[OrioShuttle website](http://www.orioshuttle.com/_eng/).

## By Train

Milan’s Main Train Station (“Milano Centrale”) and Milano Porta Garibaldi and
Milano Cadorna are the main hubs for trains. All international and national
trains stop there.

From there you can continue using subway lines or urban and suburban trains.

We suggest to take a look at [Seat61
website](https://www.seat61.com/international-trains/trains-from-Milan.htm).

## By bus

Milan is reachable by quite a few bus companies, like Flixbus and Eurolines.

We have some central bus station like:

- Milan St. Donato (“Milano San Donato”), where you can find also the M3
subway.
- Lampugnano near the M1 subway.
- Central Milan (“Milano Centrale”), near the central railway station and
the M3 subway.
- Lambrate, near the M2 subway and Lambrate railway station.
- Farmagosta, near the M2 subway.

## By Car

Inbound from:

- West, take A4 till Sesto S. Giovanni.
- East, take A4 till Sesto S. Giovanni.
- North, take A9 or A8 or A26 and then A4 till Sesto S.Giovanni.
- South, take A7 or A1.

Take care about the toll system, traffic and so on using the [Autostrade per
l’Italia website](https://www.autostrade.it/en/home).  
Be aware of the speed limits.  
Parking your car will be quite difficult, but there are some car parks where
you can park your car for a few euros.

