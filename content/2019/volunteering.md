---
title: Volunteering
menu:
  "2019":
    weight: 28
    parent: details
---

Are you attending Akademy 2019? Help make it unforgettable, and get an exclusive
Akademy 2019 t-shirt in the bargain (to ensure we have the right size please do
this before the 9th). Please consider being a
[volunteer](https://volunteers.akademy.kde.org/).

Help is needed for various tasks. Training is provided where necessary or
requested. Some of the tasks where you can help are:

- Registration, pre-registration, information desk and merchandise sales
- Prepare badges prior to the event
- Help with video recording of sessions
- Be a Session Chair; introduce speakers and manage talk rooms
- Set up conference rooms and other infrastructure
- Runners
- Other jobs that make Akademy run smoothly

Please pick some tasks on [Volunteer
system](https://volunteers.akademy.kde.org/).

If you are arriving early, there are opportunities to help during some of the
days before Akademy.

Everybody attending the conference is welcome to be an Akademy 2018 Volunteer.
It doesn't matter where you are from, your age, your skills, whether you have
ever attended a Free Software conference, or have ever been to Vienna before. As
long as you can communicate in English, you can contribute as a volunteer. For
most people, being a volunteer is one of the high points of Akademy. This is an
opportunity for you to make a difference for the KDE Community.

You should expect that your help will be needed for at least a couple of hours
on a few days. The Team will work with you to make volunteer tasks fit with the
rest of your Akademy experience.

The KDE Community and the Akademy Team say, “Thank you to all volunteers in
advance!”

If you have any questions, please contact [the Akademy
team](mailto:akademy-team@kde.org) or join us in
#akademy on freenode IRC.

