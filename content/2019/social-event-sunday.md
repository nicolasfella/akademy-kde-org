---
title: Social Event - Sunday
menu:
  "2019":
    weight: 26
    parent: details
---

Host: Fuorimano (https://fuorima.no)  
Time: 19:30-02:00  
Via Roberto Cozzi, 3, 20125 Milano MI

From U1, U2, U3 or U4, go towards the train station and take Viale
dell'Innovazione towards right (towards the city center, or University buildings
U5 and U9). Keep going straight for about 5 minutes, pass the roundabout and
arrive at a semaphore. From there, cross the street and keep going straight. The
street will turn slightly to the left, at the turn you will see signs for
"Fuorimano" pointing to an internal courtyard. Follow them and you will have
arrived.

The place is known to cure their sourdough since 1946, they will bake pizzas and
cook mini-burgers for us. Additional typical finger food (stuffed olives,
locally grown soy beans, ...) makes up for a complete dinner. Standard drinks
include wine, beer, typical international cocktails (spritz, moscow mule, gin
tonic, ...). Their barista is famous for a cocktails selection inspired by the
works of the futurist literature, inspired by the mixology and tastes of the
1920s. If you want to try those throughout the night, you are welcome to do so,
but as they require very special ingredients you will be able to purchase them
for a surplus on your token of a couple of euros.

The buffet includes:

- A pizza with whole flours Neapolitan style with sourdough baked at the
moment every 2 people.
- 2 Chianina and/or vegetarian homemade mini-burgers.
- Beautiful fresh olives from Cerignola.
- Salted toasted peanuts.
- Sweet paprika corn.
- Onion rings battered with beer.
- Olives stuffed to the ascolana.
- Soya beans and salted vegetables grown in the Po Valley.
- Two standard drinks per person.
- Welcome drink in a flûte

There will be the possibility of having more drinks, if so desired.

