---
title: Call for Papers
menu:
  "2017":
    weight: 24
    parent: program
---

**The 2017 CfP has now closed and is no longer accepting submissions**

Akademy is the KDE Community conference. If you are working on topics relevant
to KDE or Qt, this is your chance to present your work and ideas at the
Conference from 22nd-27th July in Almería, Spain. The days for talks are
Saturday and Sunday, 22nd and 23rd July. The rest of the week will be BoFs,
unconference sessions and workshops.

If you think you have something important to present, please tell us about it.
If you know of someone else who should present, please encourage them.

## What we are looking for

We are asking for talk proposals on topics relevant to KDE and Qt including:

- Design and Usability in KDE;
- Advanced C++ topics;
- QML and declarative programming;
- OpenGL, Vulkan and Qt 3D;
- Overview of what is going on in the various areas of the KDE community;
- KDE in action: use cases of KDE technology in real life; be it mobile, desktop
  deployments and so on;
- Mobile/embedded applications, use cases and frameworks;
- Presentation of new applications; new features and functions in existing
  applications;
- Collaboration between KDE and other Free Software projects;
- Improving our governance and processes, community building;
- Increasing our reach through efforts such as accessibility, promotion,
  translation and localization;
- 3rd Party libraries / frameworks relevant to KDE.

*Don't let this list restrict your ideas though. You can submit a proposal even
if it doesn't fit the list of topics as long as it is relevant to KDE. To get an
idea of talks that were accepted previously, check out the program from previous
years.*

## What we offer

Many creative, interesting KDE, Qt and Free Software contributors and 
supporters - your attentive and receptive audience. An opportunity to present
your application, share ideas and best practices, or gain new contributors. A
cordial environment with people who want you to succeed. This is your chance to
make a big splash!

Some travel funds are available, if your financial situation is the main reason
you might not attend please apply for them. Akademy travel costs may be
reimbursed based on the KDE e.V. reimbursement policy. You can apply via
[reimbursements.kde.org](https://reimbursements.kde.org/events).

## Proposal guidelines

Provide the following information on the proposal form:

- Title—the title of your session/presentation;
- Abstract—a brief summary of your presentation;
- Description—information about your topic and its potential benefits for the
  audience;
- A short bio—including anything that qualifies you to speak on your subject;
- Test partner—we encourage you to rehearse your presentation well. If you have
  someone to test your talk on or would like to help practicing your
presentation, you can ask the Program Committee.

**[Submit](https://conf.kde.org/en/akademy2017/cfp/session/new) your proposal by
April 10th, 23:59:59 CEST.**

Akademy attracts people from all over the world. For this reason, all talks are
in English. Please don't let this requirement stop you. Your ideas and
commitment are what your audience will want to know about.

- Workshops and technical talks are 30 minutes to two hours
- Fast track talks are 10 minutes long. They are featured in a single morning
  track.
- Lightning talks are 5 minutes long

Workshops, technical talks and Fast track talks include time for Q&A. Lightning
talks do not have Q&A.

Akademy has a tight schedule, so beginning and ending times are enforced.

If your presentation must be longer than the standard time, please provide
reasons why this is so. Longer talks may be turned into workshops or special
sessions later in the week, outside of the main conference tracks. In that case,
we suggest a lightning-talk format teaser to promote the workshop. For lightning
talks, we will be collecting all of the presentations (we suggest a maximum of
three slides) the day before the lightning track. All of the presentations will
be set-up and ready to go at the start of the lightning track.

Akademy is upbeat, but it is not frivolous. Help us see that you care about your
topic, your presentation and your audience. Typos, sloppy or all-lowercase
formatting and similar appearance oversights leave a bad impression and may
count against your proposal. There's no need to overdo it. If it takes more than
two paragraphs to get to the point of your topic, it's too much and should be
slimmed down. The quicker you can make a good impression, the better.

We are looking for originality. Akademy is intended to move KDE forward. Having
the same people talking about the same things doesn't accomplish that goal.
Thus, we favor original and novel content. If you have presented on a topic
elsewhere, please add a new twist, new research, or recent development,
something unique. Of course, if your talk is plain awesome as is, go for that.

Everyone submitting a proposal will be notified mid April, as to whether or not
their proposal was accepted for the Akademy 2017 Program.

**Space in the schedule will be reserved for lightning talks on new and emerging
topics closer to Akademy. A call for those will be made at the end of June.**

All talks will be recorded and published on the Internet for free, along with a
copy of the slide deck, live demo or anything else associated with the
presentations. This benefits the larger KDE Community and those who can't make
it to Akademy. You will retain full ownership of your slides and other
materials, we request that you make your materials available explicitly under
the CC-BY license.

## Who we are

Meet the Akademy 2017 Program Committee:

**Albert Astals Cid**, KDE Applications Release Manager, KDE developer for more
than 10 years.  
**Ivan Cukic**, KDE Plasma (and surrounding libraries) developer for 10
years.  
**Jos van den Oever**, KDE community member, contributor on Strigi and 
Calligra  
**Mirko Boehm**, KDE contributor for 20 years  
**Adriaan de Groot**, KDE contributor for 15 years  

If you have any questions you can email the [Program
Committee](mailto:akademy-talks@kde.org)

