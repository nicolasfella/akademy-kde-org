---
title: Sponsoring
---
<div style="float: right; padding: 1ex; margin: 1ex; width: 320px;text-align:center; ">
<a href="/media/2017/Akademy2017_Sponsorship_Opportunities.pdf">
<img src="/media/2017/sponsorship-brochure-icon.png" width="320"/></a><br />
<em>Sponsorship Brochure</em>
</div>
<p>By sponsoring this event, you will receive substantial exposure and reach
top technical people from around the world.
</p>
<p>We expect around 200 attendees, including Free Software professionals and
enthusiasts such as developers, translators, designers, and other community
members, company representatives, journalists, students and educators from
around the world.
</p>
<p>Akademy is an event where many bright people meet in an open and creative
atmosphere, work hard, solve immediate challenges and come up with new ideas
and solutions for the future.
</p>
<p><strong>Your benefits as a sponsor of Akademy:</strong></p>
<ul>
<li><strong>Meet key people</strong> – Meet key KDE and Qt contributors,
upstream and downstream maintainers, and users of one of the world's foremost
User Interface technology platforms.</li>
<li><strong>Hire top talent</strong> – KDE attracts some of the best technical
people in the world. Sponsors have the opportunity to recruit or gain
mindshare.
</li>
<li><strong>Meet other leading players</strong> – Besides the KDE community,
many other Free Software projects and companies participate in Akademy.
</li>
<li><strong>Get inspired</strong> – Akademy provides an excellent environment
for collecting feedback on new products, and for brainstorming new ideas.
</li>
<li><strong>Influence future product plans</strong> – At Akademy, the KDE project
sets direction for the upcoming months. This is your chance to be among the first
to hear of those plans, to discuss and influence them.
</li>
<li><strong>Be visible</strong> – A sponsorship gives you valuable promotional
possibilities such as advertising, prominent talks, and international press
coverage.
</li>
<li><strong>Encourage Free software technologies</strong> – You support the
development of new applications taking place in Akademy hackfests and intensive
coding sessions.<
/li>
<li><strong>Support Free software</strong> – You will be recognized and enjoy
publicity as a sponsor of the event. As importantly, you will also be
supporting Free and Open Source Software, the KDE project, and the ideals that
they are based on.
</li>
<li><strong>Be part of the KDE Community</strong> – KDE is one of the largest
Free and Open Source Software communities in the world. It is cooperative,
committed and dynamic, fun-loving, creative and hard working. To people
associated with KDE, it is the ultimate expression of community. You will have
direct access to the magic sauce that makes KDE so effective.</li>
</ul>
<p>Join us to help make this event a success!</p>
<p>Please see the <strong>
<a href="/media/2017/Akademy2017_Sponsorship_Opportunities.pdf">
Sponsorship Opportunities Brochure</a>
</strong> for full details of the options available.
</p>
<p>If you would like more information or want to discuss sponsoring, please
contact the
<a href="mailto:akademy-sponsoring@kde.org">Akademy Sponsoring Team</a>.
</p>
<p>Sponsors are critical to the success of Akademy.</p>
