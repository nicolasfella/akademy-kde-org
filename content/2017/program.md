---
title: Program Overview
menu:
  "2017":
    weight: 22
    parent: program
---

<table border="1" style="border-top-style: solid; border-top-color: #ccc; 
                         border-top-width: 2px;">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<tr>
<td>Thu 20th</td>
<td colspan="2">
  <center><a href="https://www.kde-espana.org/akademy-es2017">Akademy-es</a></center></td>
<td> </td>
</tr>
<tr>
<td>Fri 21st</td>
<td colspan="2"><center><a href="http://ev.kde.org/generalassembly/">KDE e.V. AGM</a> (eV members only)<br /><a href="https://www.kde-espana.org/akademy-es2017">Akademy-es</a></center></td>
<td>20:00 - Welcome Party at<br /> <a href="http://osm.org/go/b7g0GCah8--?layers=N&amp;m=">Civitas</a> (food &amp; drinks)</td>
</tr>
<tr>
<td>Sat 22nd</td>
<td colspan="2"><center><a href="https://conf.kde.org/en/akademy2017/public/schedule/2017-07-22">Main Talks</a></center></td>
<td> </td>
</tr>
<tr>
<td>Sun 23rd</td>
<td colspan="2"><center><a href="https://conf.kde.org/en/akademy2017/public/schedule/2017-07-23">Main Talks</a></center></td>
<td> </td>
</tr>
<tr>
<td>Mon 24th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2017/Monday">BoFs, Workshops &amp; Meetings</a></center></td>
<td> </td>
</tr>
<tr>
<td>Tue 25th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2017/Tuesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center></center></td>
</tr>
<tr>
<td>Wed 26th</td>
<td colspan="1"><center><a href="https://community.kde.org/Akademy/2017/Wednesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td colspan="2"><center>Daytrip</center></td>
</tr>
<tr>
<td>Thu 27th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2017/Thursday">BoFs, Workshops &amp; Meetings</a></center></td>
<td> </td>
</tr>
<tr>
<td>Fri 28th</td>
<td colspan="3">Conference finished with most attendees leaving today or doing tourist activities</td>
</tr>
</table>
