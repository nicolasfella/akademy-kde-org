---
title: Travel To Almería
menu:
  "2017":
    weight: 31
    parent: travel
---

## Almería Airport (LEI)

Almería Airport has frequent connections to Madrid (Iberia) and almost daily
ones to Barcelona (Vueling). It also has flights to other European airports such
as London–Gatwick, Amsterdam, Brussels, etc.

You can check all destinations and airlines at [the Wikipedia page about the
airport](https://en.wikipedia.org/wiki/Almer%C3%ADa_Airport#Airlines_and_destinations)
or in this [unofficial airport page](http://aeropuertoalmeria.info/verano-2017/)
that contains the days the flights happen(L=Monday, M=Tuesday, X=Wednesday,
J=Thusday, V=Friday, S=Saturday, D=Sunday).

Once you are at the airport you can go to the city:

- By bus: (Bus line: 22)
  - Price: 1,05€ (1,30€ if you want to take another bus in the next hour)
  - Airport - Almería: 07:25, 08:35, 09:45, 10:55, 12:05, 13:15, 14:25, 16:35,
    17:45, 18:55, 20:05, 21:15, 22:25
  - Almería - Airport: 06:45, 07:55, 09:05, 10:15, 11:25, 12:35, 13:45, 14:55,
    17:05, 18:15, 19:25, 20:35, 21:45
- By taxi:
  - Estimate price for city center: 20€
  - Estimate price for University: 15€

## Málaga Airport (AGP)

Málaga is the fourth busiest airport in Spain and thus has flights to most of
major European airports and some non-European ones (Montreal, New York,
Casablanca, Tel Aviv).

You can check all the destinations at [the Wikipedia page about the
airport](https://en.wikipedia.org/wiki/M%C3%A1laga_Airport#Airlines_and_destinations).

### How to arrive from Málaga Airport to Almería #### Direct bus

There's two daily direct buses operated by ALSA, a non stop one that takes
around 3h and one with two stops that takes 3.5 hours.

- Price: Around 20€ one way
- Málaga Airport - Almería: 14:15 and 15:30
- Almería - Málaga Airport: 9:30 and 15:30

You can get a ticket by using the
[ALSA](https://www.alsa.es/en/our-destinations/airports/malaga-costa-del-sol)
web page and filling in Almeria as destination (you may want to click the
"switch destinations" icon since it defaults to from "somewhere" to Málaga
Airport and most probably you want it the other way around.

#### Bus to Málaga and then a train or bus to Almería

There's a bus service available that links Málaga Airport with Málaga, and it
stops both at the main bus station and train station of Málaga.

There are many daily direct [ALSA](https://www.alsa.es/) buses from Málaga to
Almería, taking between 3 and 5 hours depending on the intermediate stops.

There are no direct trains between Málaga and Almería, you have to transfer at
the Antequera-Santa Ana station. Travel time is around 30 minutes between
Málaga-María Zambrano and Antequera-Santa Ana and 4 hours between
Antequera-Santa Ana and Almería. You can check train timetables at Renfe
website.

Please understand that both Renfe and ALSA tickets are for the time you bought
them so make sure you will be able to make it on time to the station (i.e. if
you buy a ticket for 18:00 it will not be valid for the 19:00 train/bus).

#### Car-pooling

There are several people that offer [car pool
rides](https://en.wikipedia.org/wiki/Carpool) between different points of Málaga
(some including the airport) to Almería.
[BlaBlaCar](https://www.blablacar.co.uk/search?fn=M%C3%A1laga%2C+Spain&tn=Almer%C3%ADa%2C+Spain)
is one of the companies that offers such a service.

#### By taxi

This may sound crazy, but there are several companies offering transfer between
the Málaga Airport and Almería ([185€](http://malagaradiotaxi.es/tarifas.html),
[220€](http://www.taxis-malaga.net/precios.php)) so if you pool with 4 people,
it is not that expensive and travel time is around 2 hours.

