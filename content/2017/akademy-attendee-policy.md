---
title: Akademy Attendee Policy
menu:
  "2017":
    weight: 21
    parent: program
---



KDE is dedicated to providing an enjoyable Akademy experience for everyone. As a
community, we value and respect all people, regardless of gender identity,
sexual orientation, race, ability, shape, size or preferred desktop environment.
We will not tolerate vilification, abuse or harassment in any form.

We appreciate that cultural differences may cause misunderstandings, so we will
try to clarify these and smooth misunderstandings as they arise. We expect
people to be both polite and proactive, and to make an effort to ensure a
pleasant conference experience for everyone.

Conference participants behaving in ways that run counter to these principles
risk being denied entrance or expelled from the conference at the discretion of
the conference organizers. Conference speakers and other more visible figures
especially should be aware of these concerns.

Harassment includes offensive verbal comments related to gender, sexual
orientation, disability, physical appearance, body size, race, religion, sexual
images in public spaces, deliberate intimidation, stalking, following,
unauthorized or inappropriate photography or recording, sustained disruption of
talks or other events, inappropriate physical contact, and unwelcome sexual
attention. Participants asked to stop any harassing behaviour are expected to
comply immediately.

If a participant engages in harassing behaviour, the conference organizers may
take any action they deem appropriate, including warning the offender or
expelling them from the conference. If you are being harassed, notice that
someone else is being harassed, or have any other concerns, please contact a
member of the conference team immediately. The conference team can be identified
by their purple team t-shirts.

The conference team will be happy to help participants contact venue security or
local law enforcement, provide escorts, or otherwise assist those experiencing
harassment to feel safe for the duration of the conference. We value your
attendance.

The conference team can be reached:

- in person at the registration desk
- by phone at +34 604 31 13 69 (Kenny D)
- by email to akademy-team@kde.org

Additional phone numbers in case of an emergency:

- Law enforcement/Emergency/Medical: 112
- Taxi company: +34 950 22 22 22

We expect participants to follow these guidelines and the [KDE Code of
Conduct](http://www.kde.org/code-of-conduct/) at all conference venues and
conference-related social events. We are committed to a fun and productive
conference for all attendees, and believe that this policy will help achieve
that goal.

We ask for the cooperation of all attendees in achieving our goal. Thank you!

