---
title: Venue
menu:
  "2017":
    weight: 34
    parent: travel
---

## Map

![Map showing locations of the
events](/media/2017/plano-akademy-y-akademy-es.jpg) [OpenStreetMap
link](https://www.openstreetmap.org/#map=18/36.82934/-2.40661&layers=N)

## Auditorio (red square between Aulario I and Aulario II)

We will have the rooms of track 1 here.

## Aulario IV

We will have the rooms of track 2 in the room named "Salón de Grados".  The
registration will be in the hall.  The
[BoFs](https://community.kde.org/Akademy/2017/AllBoF) will be in several rooms
in this building.  The [KDE e.V.
AGM](https://ev.kde.org/generalassembly/agenda.php) will also be in the room
named "Salón de Grados".  

## Address

[Universidad de Almería](https://www.ual.es/)

Ctra. Sacramento, s/n, 04120 La Cañada, Almería

## Bus to the venue from Almería

[See the wiki](https://community.kde.org/Akademy/2017/bus)
