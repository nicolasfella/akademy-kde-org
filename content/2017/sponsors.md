---
title: Sponsors
menu:
  "2017":
    weight: 6
---

## Sponsorship Opportunities

**A big thank you to our sponsors who help to make this event happen!
There are still sponsorship opportunities.  
If you are interested, please see 
[Sponsoring Akademy 2017](/2017/sponsoring) for more information,
including valuable sponsor benefits.**

## Sponsors for Akademy 2017

### Gold

{{< sponsor homepage="https://www.qt.io" img="/media/2017/tqtc_0.png" >}}

<p id="tqtc"><strong>The Qt Company</strong> (NASDAQ
 OMX Helsinki: QTCOM) is responsible for Qt development, productization 
and licensing under commercial and open-source licenses. Qt is a C++ 
based framework of libraries and tools that enables the development of 
powerful, interactive and cross-platform applications and devices.
<p>
Used by over a million developers worldwide, Qt is a C++ based framework
of libraries and tools that enables the development of powerful, interactive
and cross-platform applications and devices. Qt’s support for multiple
desktop, embedded and mobile operating systems allows developers to save
significant time related to application and device development by simply
reusing one code. Industry leaders such as Navico, ABB, Pitney Bowes, Thales,
Michelin, Magneti Marelli and Sennheiser power their products with Qt for their
in-vehicle devices, industrial automation applications and mission-critical
systems. The Qt Company operates in China, Finland, Germany, Korea, Norway,
Russia, Sweden and USA. Code less. Create more. Deploy everywhere.
</p>
<p>The Future is Written with Qt</p>

{{< /sponsor >}}

{{< sponsor homepage="https://blue-systems.com" img="/media/2017/bluesystems.png" >}}

<p id="bluesystems"><strong>Blue Systems</strong>
 is a company investing in Free/Libre Computer Technologies. It sponsors
 several KDE projects and distributions like Netrunner. Their goal is to
 offer solutions for people valuing freedom and choice.</p>

{{< /sponsor >}}


### Bronze


{{< sponsor homepage="https://www.kdab.com" img="/media/2017/kdab2.png" >}}

<p id="kdab"><strong>KDAB</strong> is the world's 
leading software consultancy for architecture, development and design of
 Qt, C++ and OpenGL applications across desktop, embedded and mobile 
platforms. The biggest independent contributor to Qt, KDAB experts build
 run-times, mix native and web technologies, solve hardware stack 
performance issues and porting problems for hundreds of customers, many 
among the Fortune 500. KDAB’s tools and extensive experience in 
creating, debugging, profiling and porting complex, great looking 
applications help developers worldwide to deliver successful projects. 
KDAB’s global experts, all full-time developers, provide market leading 
training with hands-on exercises for Qt, OpenGL and modern C++ in 
multiple languages.</p>

<https://www.kdab.com/>

{{< /sponsor >}}

{{< sponsor homepage="https://www.opensuse.org" img="/media/2017/opensuse.png" >}}

<p id="opensuse"><strong>The openSUSE project</strong>
 is a worldwide effort that promotes the use of Linux everywhere. 
openSUSE creates one of the world's best Linux distributions, working 
together in an open, transparent and friendly manner as part of the 
worldwide Free and Open Source Software community. The project is 
controlled by its community and relies on the contributions of 
individuals, working as testers, writers, translators, usability 
experts, artists and ambassadors or developers. The project embraces a 
wide variety of technology, people with different levels of expertise, 
speaking different languages and having different cultural backgrounds. 
Learn more at:</p>

<https://www.opensuse.org/>

{{< /sponsor >}}

{{< sponsor homepage="https://www.ppro.com" img="/media/2017/pprologo3.png" >}}

<p id="ppro">At <strong>PPRO</strong> 
we believe that awesome software and awesome people are the foundation of our
business and the key to our success. We hire excellent engineers and let them
build our services, which are used by our partners and millions of consumers.
Our Munich-based engineering team build our payment systems using FreeBSD,
modern C++, SQL, and Python, combined with open source components like cmake,
clang, React, Ice, MariaDB, git, Phabricator etc. PPRO's main products are
prepaid MasterCard and Visa cards, as well as access to alternative payment
methods for cross-border e-commerce. Learn more at:</p>

<https://www.ppro.com/>

{{< /sponsor >}}



### Supporters


{{< sponsor homepage="https://www.froglogic.com" img="/media/2017/froglogic.png" >}}

<p id="froglogic"><strong>Froglogic</strong>
is a software company based in Hamburg, Germany, with additional offices 
in the USA, and with over 3,000 customers world-wide. froglogic is famous
for its automated testing suite Squish with its flagship product Squish
GUI Tester, the market-leading automated testing tool for GUI applications
based on Qt, Java AWT/Swing, SWT/RCP and JavaFX, Mac OS X Carbon/Cocoa,
Windows MFC, NET and WPF, iOS Cocoa Touch, Android, and for HTML/Ajax/Flex-based
web applications running in a variety of web browsers. In addition, froglogic's
offering includes the professional cross-platform C, C++, C# and Tcl Code
Coverage tool Squish Coco. Learn more at:</p>

<https://www.froglogic.com/>

{{< /sponsor >}}

{{< sponsor homepage="https://nextcloud.com" img="/media/2017/nextcloud.png" >}}

<p id="nextcloud"><strong>Nextcloud</strong>
is the most popular Open Source file sync and share technology, giving you easy
and secure access to your data on your own server through mobile/desktop apps
or our web interface. You can extend nextcloud to manage your Calendars, Mail,
secure Video Calls, Bookmarks and much more.</p>


{{< /sponsor >}}

{{< sponsor homepage="https://openinventionnetwork.com" img="/media/2017/openinventionnetwork.jpg" >}}

<p id="oin"><strong>Open Invention Network</strong> 
is the largest patent non-aggression community in history and supports 
freedom of action in Linux as a key element of open source software. 
Launched in 2005 and funded by Google, IBM, NEC, Philips, Red Hat, Sony,
 SUSE, and Toyota, OIN has more than 2,000 community members and owns 
more than 1,100 global patents and applications. The OIN patent license 
and member cross-licenses are available royalty free to any party that 
joins the OIN community. Joining OIN helps provide community members 
with patent freedom of action in Linux and adjacent open source 
technologies, free of charge or royalties.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://www.redhat.com" img="/media/2017/redhat.png" >}}

<p id="redhat"><strong>Red Hat</strong> is the 
world's leading provider of open source solutions, using a community-powered
approach to provide reliable and high-performing cloud, virtualization,
storage, Linux, and middleware technologies.</p>


{{< /sponsor >}}


### Venue

{{< sponsor homepage="https://www.ual.es/" img="/media/2017/ual_logo.png" >}}

<p>Universidad de Almería, Electrical Engineering Area</p>

{{< /sponsor >}}


### Patrons of KDE

[KDE's Patrons](http://ev.kde.org/supporting-members.php)
also support the KDE Community through out the year.
