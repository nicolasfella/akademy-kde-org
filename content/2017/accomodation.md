---
title: Accomodation
menu:
  "2017":
    weight: 32
    parent: travel
---

## Civitas

This year's recommended accommodation is [Residencia
Civitas](http://www.residenciacivitas.com/) ([Promotional
Video](http://www.youtube.com/watch?v=v0PAAprhc_U)).

Civitas is a students dormitory with enough space for all Akademy attendees.
![Image of a single room with person at a
desk](/media/2017/Instalaciones-habitacion-civitas-01.jpg)

**Single room:**  
32.75 € per night, breakfast included  
42.10 € per night, breakfast and evening dinner included  

![Image of a twin room with one person at a desk and another on a
bed](/media/2017/Instalaciones-habitacion-civitas-05.jpg) 

**Twin room:**   
46.75 € per night, breakfast included  
56.10 € per night, breakfast and evening dinner included  

If you would like to share a room, but you don't have a roommate yet, there's [a
wiki page for searching a
roommate](https://community.kde.org/Akademy/2017/Find_a_room-mate). You can
search if someone is staying in the same dates than you or you can announce
yourself.

## Common rooms

Each floor has a common room with where you can relax with other attendees,
chatting or hacking together.

## Sponsored accommodation

**People requesting accommodation sponsorship should not book their own
accommodation**

## Booking process

In order to book a room and get the Akademy rates, send an email to
info@residenciacivitas.com with "Akademy 2017 reservation" and use the template
below when making the reservation, remove the options you don't want from the
two "I want a:" sections. Prices are per room/per night. **Make sure you book
before the end of May or the Civitas dormitory may be fully booked**

    Dear Residencia Civitas,

    My name is FULL_NAME and I want to make a reservation for Akademy 2017
congress on University of Almería.

    I want a:
    - Single room
    - Double (two beds) room, for me and FULL_NAME_2

    Dates:
    - check-in day:
    - check-out day:

    And I want:
    - Half board (breakfast and evening dinner)
    - Bed and Breakfast

    Please confirm the reservation.

    Best regards,

    NAME 

## Address

Calle Fernan Caballero, 1, Almería

If using the PideTaxi app for Taxi, use Nuestra Señora de Montserrat, 102,
Almería

## How to arrive ### From the airport

- The airport bus stop is the number 188 (you can check the [wait
  time](http://m.surbus.com/tiempo-espera/parada/188)).
- You need [to take Line 30](http://www.surbus.com/inicio.aspx?cat=0&id=30)
  (1,05 €). Be sure you are going back to city downtown since the bus pass twice
going and coming from suburbs.
- Get off at "Estación Intermodal", bus stop #292 (you can [check the wait
  time](http://m.surbus.com/tiempo-espera/parada/292)). This is just outside the
bus and train station.
- Walk 10 minutes to Civitas (check the [OpenStreetMap
  route](http://www.openstreetmap.org/directions?engine=mapzen_foot&route=36.83520%2C-2.45601%3B36.83752%2C-2.44828#map=17/36.83641/-2.45216)).

### From the Estación Intermodal

Estación Intermodal is the main public transport hub of Almería, where airport
buses, trains and intercity buses arrive and depart.

- Walk 10 minutes [OpenStreetMap
  Route](http://www.openstreetmap.org/directions?engine=mapzen_foot&route=36.83520%2C-2.45601%3B36.83752%2C-2.44828#map=17/36.83641/-2.45216).

## Other options

Should you prefer to stay in a different place, Almería has a wide range of
hotels you can choose from. You can use sites such as Booking, TripAdvisor,
Trivago to search for accommodation you like.

