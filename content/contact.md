---
title: Contact
menu:
  "2024":
    weight: 1002
  main:
    weight: 1002

---
Akademy is organized by [KDE e.V.](http://ev.kde.org/)

To contact the Akademy organizing team, please email [The Akademy Team](mailto:akademy-team@kde.org). For other matters, contact the [KDE e.V. Board](mailto:kde-ev-board@kde.org)

Stay up to date by following Akademy on [Mastodon](https://floss.social/@akademy) or [X (Twitter)](https://twitter.com/akademy), join our [Attendees' mailing list](https://mail.kde.org/mailman/listinfo/akademy-attendees) or hang out and ask questions in our [Matrix room](https://go.kde.org/matrix/#/#akademy:kde.org) [(Akademy Space)](https://go.kde.org/matrix/#/#attend-akademy:kde.org) or via IRC in [#akademy on Libera.chat](https://web.libera.chat/#akademy).

