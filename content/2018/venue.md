---
title: Venue
menu:
  "2018":
    weight: 22
    parent: details
---

The Venue for Akademy 2018 will be at the Neues EI Building at Campus Gußhaus of
TU Wien.

## Map

![Image of map with venue and nearest subway stations](/media/2018/map_venue_0.png)

[OpenStreetMap link](https://www.openstreetmap.org/#map=16/48.1980/16.3719)


## Conference rooms

The Akademy talks will be held at the rooms EI7 & EI8. They are located on the
ground floor of Neues EI. To find them just continue walking straight from the
entrance.

## KDE eV AGM

The AGM for KDE eV members will be held in room EI7 on Monday 1230-1530

## BoF rooms

The [BoFs](https://community.kde.org/Akademy/2018/AllBoF) will be in several
rooms in this building. We will announce the exact rooms at a later time.

## Address

TU Wien Neues EI Gusshaustraße 27-29 1040 Wien

## Public Transport

### Subway

Neues EI is located a short walking distance from the following subway stations:

- Taubstummengasse (U1)
- Karlsplatz (U1, U2 & U4)

### Tramway

- Paulanergasse (Tramway 1 & 62)
- Kärntner Ring, Oper (Tramway 1, 2, 62, 71 & D)
- Gußhausstraße (Tramway D)

## Directions from A&O Wien Hauptbahnhof

- Public Transport: Take subway U1 in direction Leopoldau one station to
  Taubstummengasse.
- Walking: From A&O you walk around 20 - 25 minutes to the venue.
