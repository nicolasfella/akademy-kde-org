---
title: Daytrip
menu:
  "2018":
    weight: 29
    parent: details
---

![View of Vienna from Kahlenberg](/media/2018/Panorama_vom_Kahlenberg.jpg)
<figcaption>Vienna from Kahlenberg by Clemens Pfeiffer</figcaption>

After 2 days of talks and 2 1/2 days of BoF it's time to take your eyes away
from the beautiful code that makes up our software and go to look at the
beautiful city of Vienna from above. The daytrip of Akademy 2018 will lead us to
[Kahlenberg](https://en.wikipedia.org/wiki/Kahlenberg), a 484 m high hill. But
don't worry, you don't need to hike up (except you want).

![View of Kahlenberg from Vienna](/media/2018/Kahlenberg_0.jpg)
<figcaption>Kahlenberg from the
city<br><small>Bwag/CC-BY-SA-4.0</small></figcaption>

## What to do there?

We have a few options what to do there:

- Take a look at Vienna from above
- Combine that look with a self-brought (you can also buy it up on the mountain)
  beer
- Have a coffee with a view at Cafe Kahlenberg
- Have fun climbing around at Waldseilpark Kahlenberg
- Explore the surrounding area and enjoy a walk through the Vienna Woods 

## How to get there (without hiking)?

We will meet at 14:00 outside the Opera exit of the subway station Karlsplatz
(next to the fountain). From there we will take public transport all the way to
Kahlenberg.

Public transport to Kahlenberg:

- Meeting Point: 14:00, subway station Karlsplatz, exit Opera
- Tramway D to Grinzinger Straße
- Bus 38A to Kahlenberg

## How to get there (with hiking)?

**Stadtwanderweg 1** is an easy hiking trail that leads up to Kahlenberg. It
will take you around 1.5 hours of hiking in which you need to complete an
elevation gain of around 350 meters. We will organize all the people interested
in hiking up into a group and will find you someone local to accompany you.

Register here if you are planning to join the hiking group: [Hiking Group
Registration](https://community.kde.org/Akademy/2018/DaytripHiking)

Directions & other Information:

- Meeting Point Hiking group: Meeting Point: 13:00, subway station Karlsplatz,
  exit Opera (be on time!)
- Tramway D to the last stop (Nussdorf, Beethovengang)
- Follow Stadtwanderweg 1 up to Kahlenberg (Duration: around 2 hours)
- No special hiking shoes required, but good outdoor (sport) shoes required
- Don't forget to bring water and a small snack to refill your energy on the way
  up

![Map of Kahlenberg showing the vista point and Forest Rope
Park](/media/2018/kahlenberg_map.png)
<figcaption>CC OpenStreetMap</figcaption>

## Waldseilpark Kahlenberg

10 minutes walk from the cafe on Kahlenberg there is the [Waldseilpark
Kahlenberg](https://www.waldseilpark-kahlenberg.at/en/), a forest rope park. It
offers 14 climbing courses on 3 different difficulty levels. Go there and have a
few fun hours challenging yourself to master as many courses as possible.

Forest rope park information:

- Closed shoes necessary
- No prior experience necessary, short introduction included
- No equipment needed, helmet & climbing harness included
- Ticket price: 28 € for up to 4 hours of climbing

## After the day on the mountain

After the day on the mountain (exact time will be announced during Akademy) we
will go to the [Prater](https://en.wikipedia.org/wiki/Wurstelprater) amusement
park to have a fun evening. Different from many modern amusement parks, this one
has no entrance fee and you only have to pay when you ride an attraction. There
are many possibilities there, e.g.: Ride the big or small ferris wheel to see
Vienna at night, ride rollercoasters, eat
[Langos](https://en.wikipedia.org/wiki/L%C3%A1ngos) and other small snacks or
other rides or go to the beergarden for traditional food & beers.  Getting there
(from Kahlenberg):

- Bus 38A to Grinzinger Straße
- Walk one block on Grinzinger Straße to Kreilplatz
- Bus 5B to Praterstern
- Walk to Prater

## Getting back to A&O (recommended accomodation) ### From Kahlenberg

- Bus 38A to Grinzinger Straße
- Tramway D to Alfred-Adler-Straße (last stop)

### From Prater

- Walk to Praterstern
- U1 in direction Oberlaa until Süditroler Platz/Hauptbahnhof.

