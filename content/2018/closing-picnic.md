---
title: Closing Picnic
menu:
  "2018":
    weight: 30
    parent: details
---

**Please note that due to a BBQ ban in Vienna this event is only a picnic. Only
bring food that doesn't need to be cooked or grilled.**

After a week of lectures & BoFs it's time for a final social event. We reserved
a BBQ place on the bank of New Danube where you all can chill, eat, swim & drink
(food and drinks not provided, bring your own).

## When & Where

Friday, 17th of August 6pm onwards  
BBQ place #8  

### How to get there?

- U2 to Donaustadtbrücke
- Take exit in direction Neue Donau
- Do not cross the new danube, stay on this side of the new danube
- Outside the station turn right and go straight until you see the restaurant
  wake_up
- After the restaurant wake_up, go down to the riverbank and follow it for 5
  minutes
- The BBQ place is directly next to the boardwalk

![Map of location f closing picnic and subway station](/media/2018/map_bbq.png)
<figcaption>CC OpenStreetMap</figcaption>

## What to bring?

The Akademy team will provide plates, cutlery, bread and some basic condiments.
You only need to bring food to eat & drinks. Also bring your swimming gear if
you want to swim in the New Danube. And if you are a local maybe you can bring
some fun sports stuff or music instruments.

