---
title: Program Overview
menu:
  "2018":
    weight: 21
    parent: details
---

<table border="1" style="border-top-style: solid; border-top-color: #ccc; border-top-width: 2px;">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<td>Fri 10th</td>
<td colspan="2"> <center></center></td>
<td><a href="/2018/pre-registration-event">Pre-Registration Event</a></td>

<tr>
<td>Sat 11th</td>
<td colspan="2"><center><a href="https://conf.kde.org/en/Akademy2018/public/schedule/1">Talks Day 1</a></center></td>
<td> </td>
</tr>
<tr>
<td>Sun 12th</td>
<td colspan="2"><center><a href="https://conf.kde.org/en/Akademy2018/public/schedule/2">Talks Day 2</a></center></td>
<td><center><a href="/2018/social-event">Social Event</a></center></td>
</tr>
<tr>
<td>Mon 13th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2018/Monday">BoFs, Workshops &amp; Meetings</a><br /><a href="https://ev.kde.org/generalassembly/">KDE eV AGM</a> 1230-1530 in EI7</center></td>
<td> </td>
</tr>
<tr>
<td>Tue 14th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2018/Tuesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center><a href="/2018/lgbt-dinner">LGBT Dinner</a><br /><a href="https://community.kde.org/Akademy/2018/Organised_Sightseeing_Tour">Vienna Sightseeing Tour</a></center></td>
</tr>
<tr>
<td>Wed 15th</td>
<td colspan="1"><center><a href="https://community.kde.org/Akademy/2018/Wednesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td colspan="2"><center><a href="/2018/daytrip">Daytrip</a></center></td>
</tr>
<tr>
<td>Thu 16th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2018/Thursday">BoFs, Workshops &amp; Meetings</a></center></td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2018/Organised_Sightseeing_Tour">Vienna Sightseeing Tour</a></center></td>
</tr>
<tr>
<td>Fri 17th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2018/Friday">BoFs, Workshops &amp; Meetings</a></center></td>
<td colspan="2"><center><a href="/2018/closing-picnic">Closing Picnic</a></center></td>
</tr>
</table>

***Lunch on Saturday & Sunday is provided free for those registering by Thursday
2nd Aug 23:59CEST,*** *along with food at the Friday 10th & Sunday evening social
events. Other meals you are responsible to sort out yourself, including if you
attend the BBQ.*
