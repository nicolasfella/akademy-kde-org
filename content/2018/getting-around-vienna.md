---
title: Getting around in Vienna
menu:
  "2018":
    weight: 33
    parent: travel
---

The best ways to get around in Vienna are [public transport](#public-transport),
[bike](#bike) and walking.

## Public transport

The public transport system in Vienna consists of the following transport
methods:

- UBahn (subway)
- Schnellbahn (city train)
- Straßenbahn (tramway)
- Autobus (city bus)

Overview plan (includes UBahn & Schnellbahn):
[Plan](https://www.wienerlinien.at/media/files/2017/svp-2017_217934.pdf)

See the [ticket](/2018/tickets) section for various kinds of tickets and special
offers for Akademy attendees.

## Routing apps

The following is a list of public transport routing apps for Vienna:

- [Öffi](https://oeffi.schildbach.de/index.html)
- [Transportr](https://transportr.grobox.de/)
- [Wien
  Mobil](https://www.wienerlinien.at/eportal3/ep/channelView.do/pageTypeId/66533/channelId/-3600061)

Note: Google maps also has routing for Vienna, but the data is not real time
data. So while it works for a general "how to get from A-to-B" information,
don't rely on the information because it doesn't include changes due to
construction work or other disturbances.

## Public transport during the night

On nights before weekdays, subway service ends around half past midnight. On
nights before Saturday, Sunday or public holiday (night to 15.08.) subways runs
all night, every 15 minutes.  To get around the city at night, you can use the
Nightline. Using the Nightline is included in daily or weekly passes or you can
use a normal single ticket.


**Note:** In the following plans, not every station is included, only the bigger
ones.  Nightlines under the week:
[Plan](https://www.wienerlinien.at/media/files/2016/nightline-werktage_199594.pdf)
Nightlines & Subway on the weekend:
[Plan](https://www.wienerlinien.at/media/files/2017/nightline_wochenende_220633.pdf)

## Bike

While not on the level of Copenhagen or Amsterdam, Vienna is a very
bike-friendly city.  Routing apps

- [Bike Citizens](https://www.bikecitizens.net/app/)
- [Wien
  Mobil](https://www.wienerlinien.at/eportal3/ep/channelView.do/pageTypeId/66533/channelId/-3600061)
(combined public transport, bike & walking routing)

## Rental bikes

Vienna has a public bike system called Citybike. It consists of over 120
stations, where you can rent or give back your bike. Riding a bike for up to one
hour is free, you need to register once with your Creditcard for a one time fee
of 1 €. You can find more information on the [Citybike
homepage](https://www.citybikewien.at/en/)  
Additionally, three other rental bike systems are available in Vienna: [Ofo](http://www.ofo.com/), [Donkey
Bike](https://www.donkey.bike/) & [oBike (Site in
German)](https://www.o.bike/at/)

