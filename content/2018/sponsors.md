---
title: Sponsors
menu:
  "2018":
    weight: 6
---

## Sponsorship Opportunities

**A big thank you to our sponsors who help to make this event happen!
There are still sponsorship opportunities.  
If you are interested, please see 
[Sponsoring Akademy 2018](/2018/sponsoring) for more information,
including valuable sponsor benefits.**

## Sponsors for Akademy 2018

### Gold

{{< sponsor homepage="https://www.qt.io" img="/media/2018/tqtc_0.png" >}}

<p id="tqtc"><strong>The Qt Company</strong> 
(NASDAQ OMX Helsinki: QTCOM) is responsible for Qt development, productization
and licensing under commercial and open-source licenses. Qt is a C++ based
framework of libraries and tools that enables the development of powerful,
interactive and cross-platform applications and devices.
<p>Used by over a million developers worldwide, Qt is a C++ based framework of 
libraries and tools that enables the development of powerful, interactive and 
cross-platform applications and devices. Qt’s support for multiple desktop, 
embedded and mobile operating systems allows developers to save significant
time related to application and device development by simply reusing one
code. Qt and KDE have a long history together, something The Qt Company values
and appreciates. Code less. Create more. Deploy everywhere.</p>

<https://www.qt.io/>

<p>The Future is Written with Qt</p>

{{< /sponsor >}}

{{< sponsor homepage="https://blue-systems.com" img="/media/2018/bluesystems.png" >}}

<p id="bluesystems"><strong>Blue Systems</strong>
is a company investing in Free/Libre Computer Technologies. It sponsors several
KDE projects and distributions like Netrunner. Their goal is to offer solutions
for people valuing freedom and choice.</p>

<https://blue-systems.com>

{{< /sponsor >}}


### Silver


{{< sponsor homepage="https://www.canonical.com" img="/media/2018/ubuntu_transparent.png" >}}

<p id="canonical"><strong>Canonical</strong>
is the company behind Ubuntu, a platform that spans from the PC and IoT devices
to the server and the cloud. It includes a comprehensive suite of
enterprise-grade tools for development, configuration, management and service
orchestration. Ubuntu comes with everything you need to run your organisation,
school, home or enterprise. Canonical provides enterprise products, support and
services for Ubuntu. Established in 2004, Canonical is a privately held company.</p>

<https://www.canonical.com>

{{< /sponsor >}}


### Bronze

{{< sponsor homepage="https://bytesource.net" img="/media/2018/bytesource_logo.png" >}}

<p id="bytesource">
<strong>ByteSource Technology Consulting GmbH</strong>
is based in Vienna and specialised in complex IT- solutions. <br /> As an
Atlassian Platinum Solution Partner Enterprise we also offer high flexible
solutions for team collaboration and DevOps. The company combines research
with the application of the latest state-of-the-art methods and procedures
from the IT-sector. ByteSource assists customers in reducing costs, while at
the same time optimising productivity and the quality of services. Our IT
experts have many years of experience as well as sound and proven expertise
and methodological competence - from strategy consulting to the implementation
of technical and technological solutions. This consulting company believes:
“Nothing is impossible!” The right IT-Know-how, combined with innovative
thinking and flexible action, turns visions into reality.</p>

<https://bytesource.net>


{{< /sponsor >}}

{{< sponsor homepage="https://www.kdab.com" img="/media/2018/kdab_wee.png" >}}


<p id="kdab"><strong>KDAB</strong>
is the world's leading software consultancy for architecture, development
and design of Qt, C++ and OpenGL applications across desktop, embedded and
mobile platforms. The biggest independent contributor to Qt, KDAB experts
build run-times, mix native and web technologies, solve hardware stack
performance issues and porting problems for hundreds of customers, many
among the Fortune 500. KDAB’s tools and extensive experience in creating,
debugging, profiling and porting complex, great looking applications help
developers worldwide to deliver successful projects. KDAB’s global experts,
all full-time developers, provide market leading training with hands-on
exercises for Qt, OpenGL and modern C++ in multiple languages.</p>

<https://www.kdab.com>


{{< /sponsor >}}

{{< sponsor homepage="https://www.opensuse.com" img="/media/2018/opensuse.png" >}}


<p id="opensuse">
<strong>The openSUSE project</strong>
is a worldwide effort that promotes the use of Linux everywhere. openSUSE
creates one of the world's best Linux distributions, working together in an
open, transparent and friendly manner as part of the worldwide Free and Open
Source Software community. The project is controlled by its community and
relies on the contributions of individuals, working as testers, writers,
translators, usability experts, artists and ambassadors or developers. The
project embraces a wide variety of technology, people with different levels
of expertise, speaking different languages and having different cultural
backgrounds. Learn more at:</p>

<https://www.opensuse.org>

{{< /sponsor >}}

{{< sponsor homepage="https://puri.sm" img="/media/2018/purism.png" >}}


<p id="purism"><strong>Purism</strong>
is a Social Purpose Corporation devoted to bringing security, privacy, software
freedom, and digital independence to everyone’s personal computing experience.
With operations based in San Francisco (California) and around the world, Purism
manufactures premium-quality laptops, tablets and phones, creating beautiful and
powerful devices meant to protect users’ digital lives without requiring a
compromise on ease of use.</p>

<p>Security and privacy-centric features come built-in with every product Purism
makes, making security and privacy the simpler, logical choice for individuals
and businesses. Learn more at:</p>

<https://puri.sm>


{{< /sponsor >}}


### Supporters

{{< sponsor homepage="https://slimbook.es" img="/media/2018/slimbook.png" >}}

<p id="slimbook"><strong>Slimbook</strong>
is a computer brand from Spain with Free Software vocation. Our products have
consistently been designed and tested to run GNU/Linux and, through cooperation,
provided among the best experience we can find in the market. In this fashion,
we collaborated with KDE and created the KDE Slimbook laptop to make sure we
have a good laptop that is today an example of what we can achieve working
together. See you at Akademy!</p>

<https://slimbook.es>


{{< /sponsor >}}

{{< sponsor homepage="https://pine64.org" img="/media/2018/pine.png" >}}


<p id="pine64"><strong>PINE64</strong>
is a community driven company striving to deliver open source ARM64 single
board computers to tinkerers, developers as well as education and business
sectors. PINE64’s history began with a successful Kickstarter campaign 2015
of its original PINE A64(+) single board computer. The company has since
grown in both scope and scale, and its offer now includes the Pinebook, a
sub-$100 laptop, a mini ITX Clusterboard capable of hosting 7 SOPine modules,
as well as the new powerful RockPro64 single board computer. The goal is to
provide the community with affordable and well-documented devices that
everyone can use, develop for and apply in a multitude of scenarios.</p>

<https://pine64.org>


{{< /sponsor >}}

{{< sponsor homepage="https://openinventionnetwork.com" img="/media/2018/openinventionnetwork.jpg" >}}


<p id="oin"><strong>Open Invention Network</strong> 
is the largest patent non-aggression community in history and supports freedom
of action in Linux as a key element of open source software. Launched in 2005
and funded by Google, IBM, NEC, Philips, Red Hat, Sony, SUSE, and Toyota, OIN
has more than 2,000 community members and owns more than 1,100 global patents
and applications. The OIN patent license and member cross-licenses are
available royalty free to any party that joins the OIN community. Joining OIN
helps provide community members with patent freedom of action in Linux and
adjacent open source technologies, free of charge or royalties.</p>

<https://openinventionnetwork.com>

{{< /sponsor >}}

{{< sponsor homepage="" img="/media/2018/logo_savernik.png" >}}


<p id="leo">
<strong>Dr. Leo Savernik IT Dienstleistungen</strong></p>


{{< /sponsor >}}

{{< sponsor homepage="https://mycroft.ai" img="/media/2018/mycroft_0.png" >}}

<p id="mycroft"><strong>Mycroft</strong>
The world's first Open Source Voice Assistant. We're building a voice AI that
runs anywhere and interacts exactly like a person. Mycroft has products for
developers, consumers, and enterprises who demand their technology be Private,
Customizable, and Open.</p>

<https://mycroft.ai>


{{< /sponsor >}}

{{< sponsor homepage="https://www.codethink.co.uk" img="/media/2018/codethink.png" >}}


<p id="codethink">In <strong>Codethink</strong>
we specialise in system-level Open Source software infrastructure to support
advanced technical applications, working across a range of industries including
finance, automotive, medical, telecoms. Typically we get involved in software
architecture, design, development, integration, debugging and improvement on
the deep scary plumbing code that makes most folks run away screaming.</p>

<https://www.codethink.co.uk>


{{< /sponsor >}}

### Venue


{{< sponsor homepage="https://www.fsinf.at/" img="/media/2018/fsinf.png" >}}

<p id="fsinf"><strong>Fachschaft Informatik</strong></p>

<https://www.fsinf.at/>

{{< /sponsor >}}

{{< sponsor homepage="https://htu.at/" img="/media/2018/htu.png" >}}

<p id="htu">
<strong>Hochschülerinnen- und Hochschülerschaft an der TU Wien</strong>

<https://htu.at/>

{{< /sponsor >}}

### Media Partners


{{< sponsor homepage="https://linux-magazine.com" img="/media/2018/linuxmagazine.png" >}}

<p id="linuxmagazine">
Linux Pro Magazine is a monthly publication aimed at the seasoned Linux user and
developer. Our articles keep the emphasis on real-life, practical techniques,
which has kept our content relevant in an increasingly challenging market.</p>

<p>Linux Pro Magazine delivers insightful technical articles on a range of topics
related to IT technology, including comprehensive coverage of technical
subjects, thorough and impartial reviews of new products, practical advice on
tools and strategies for system administrators, tips and techniques for
programming in the Linux environment, and discussions of advanced desktop
usage and applications.</p>

<http://www.linux-magazine.com>


{{< /sponsor >}}


### Patrons of KDE

[KDE's Patrons](http://ev.kde.org/supporting-members.php)
also support the KDE Community through out the year.

