---
title: Public Transport Tickets
menu:
  "2018":
    weight: 34
    parent: travel
---

All of Vienna consists only of one zone (called *Kernzone*) in the public
transport ticket. The only time you need to buy an additional ticket is when
coming from or going to the airport.

## Discounted Tickets for Public Transport

[Wiener Linien offers discounted
tickets](https://shop.wienerlinien.at/index.php/special_customer/1964/join/4fwAGsGdlw) to all Akademy attendees.

The ticket shop displays the full price, on checkout 10 % discount will be
deducted.

Discounted tickets can only be purchased online. The vending machines do not
sell discounted tickets.

**Note:** You can only buy tickets in subway stations and in the tramway. Busses &
Schnellbahn have no ticket machine inside, so buy your ticket in advance.

## Check suitability

Check whether the discounted tickets are suitable for your stay. Occasionally
regular tickets may be less expensive. The following table denotes the least
expensive ticket for any given duration.

All tickets are valid for Zone 100 (*Kernzone*) which encompasses all of Vienna
within the city limits.  

<table>
<tr>
<th>No. of Days
</th><th>Ticket
</th><th>Price*
</th><th>Discounted?
</th><th>Comments
</th></tr><tr>
<td class="centered">2 hrs
</td><td>Single trip Vienna<br />
</td><td class="number">2,40<br />
</td><td class="centered">no
</td><td>For a single trip in one direction, change lines as often as necessary
</td></tr><tr>
<td class="centered">1
</td><td>Day Ticket Vienna<br />
</td><td class="number">5,80<br />
</td><td class="centered">no
</td><td>Valid until 1:00am of next day
</td></tr><tr>
<td class="centered">24 hrs
</td><td>24 hours Vienna ticket<br />
</td><td class="number">8,00<br />
</td><td class="centered">no
</td><td>Valid from time of purchase for 24 hours
</td></tr><tr>
<td class="centered">2
</td><td><a href="https://shop.wienerlinien.at/index.php/special_customer/1964/join/4fwAGsGdlw">Congress ticket 2 Days</a><br />
</td><td class="number">11,52<br />
</td><td class="centered">yes
</td><td>
</td></tr><tr>
<td class="centered">48 hrs
</td><td>48 hours Vienna ticket<br />
</td><td class="number">14,10<br />
</td><td class="centered">no
</td><td>Valid from time of purchase for 48 hours
</td></tr><tr>
<td class="centered">3
</td><td><a href="https://shop.wienerlinien.at/index.php/special_customer/1964/join/4fwAGsGdlw">Congress Ticket 3 Days</a><br />
</td><td class="number">15,84<br />
</td><td class="centered">yes
</td><td>
</td></tr><tr>
<td class="centered">7
</td><td>Weekly ticket Vienna<br />
</td><td class="number">17,10<br />
</td><td class="centered">no
</td><td>Valid from midnight on Monday until 9.00 am on the following Monday
</td></tr><tr>
<td class="centered">72 hrs
</td><td>72 hours Vienna ticket<br />
</td><td class="number">17,10<br />
</td><td class="centered">no
</td><td>Valid from time of purchase for 72 hours
</td></tr><tr>
<td class="centered">4
</td><td><a href="https://shop.wienerlinien.at/index.php/special_customer/1964/join/4fwAGsGdlw">Congress Ticket 4 Days</a><br />
</td><td class="number">18,00<br />
</td><td class="centered">yes
</td><td>Best offer if first day of validity is not Monday
</td></tr><tr>
<td class="centered">5
</td><td><a href="https://shop.wienerlinien.at/index.php/special_customer/1964/join/4fwAGsGdlw">Congress Ticket 5 Days</a><br />
</td><td class="number">20,16<br />
</td><td class="centered">yes
</td><td>Best offer if first day of validity is not Monday
</td></tr><tr>
<td class="centered">6
</td><td><a href="https://shop.wienerlinien.at/index.php/special_customer/1964/join/4fwAGsGdlw">Congress Ticket 6 Days</a><br />
</td><td class="number">22,32<br />
</td><td class="centered">yes
</td><td>Best offer if first day of validity is not Monday
</td></tr><tr>
<td class="centered">7
</td><td><a href="https://shop.wienerlinien.at/index.php/special_customer/1964/join/4fwAGsGdlw">Congress Ticket 7 Days</a><br />
</td><td class="number">24,48<br />
</td><td class="centered">yes
</td><td>Best offer if first day of validity is not Monday<br />
</td></tr></table>

* Prices in table include discount. In the ticket shop, prices are displayed
  undiscounted. The discount will be deducted on payment page.

All Congress Tickets start at 00:00 of their chosen validity date and end after
23:59 after the amount of days has passed.

Example:

*Congress Ticket 4 Days*, valid from 10th Aug. 2018 will be valid from

10th Aug. 2018 00:00 until 13th Aug. 2018 23:59

## Links

- [Purchase discounted tickets online](https://shop.wienerlinien.at/index.php/special_customer/1964/join/4fwAGsGdlw)
- [Purchase regular tickets online](https://shop.wienerlinien.at/index.php/tickets) or use a local ticket vending machine after arrival. 
