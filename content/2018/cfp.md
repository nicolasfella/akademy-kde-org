---
title: Call for Participation
menu:
  "2018":
    weight: 32
    parent: details
---

Akademy is the annual KDE Community conference. If you are working on topics
relevant to KDE or Qt, this is your chance to present your work and ideas to the
KDE community at large. It will take place in Vienna, Austria, from Saturday
11th to Friday 17th of August, 2018. The talks will be held on Saturday the
**11th and Sunday the 12th.** The rest of the week will be Birds-of-a-Feather
meetings (BoFs), unconference sessions and workshops.

If you think you have something interesting to present, please tell us about it.
If you know of someone else who should present, please encourage them.

## What we are looking for

We are asking for talk proposals on topics relevant to KDE community and
technology, including:

- Topics related to KDE's current community goals:
  - Usability and Productivity for Basic Software
  - Privacy Software
  - Streamlined Onboarding of New Contributors
- Design and Usability in KDE
- Advanced Qt and C++ topics
- QML and declarative programming
- Overview of what is going on in the various areas of the KDE community
- KDE in action: use cases of KDE technology in real life; be it mobile, desktop
  deployments and so on
- Mobile/embedded applications, use cases and frameworks
- Presentation of new applications; new features and functions in existing
  applications
- Testing KDE software
- Release processes
- Collaboration between KDE and other Free Software projects
- Improving our governance and processes, community building
- Increasing our reach through efforts such as accessibility, promotion,
  translation and localization
- 3rd Party libraries / frameworks relevant to KDE
- KDE and the wider world.

Don't let this list restrict your ideas though. You can submit a proposal even
if it doesn't fit in this list of topics as long as it is relevant to KDE. To
get an idea of talks that were accepted previously, check out the program from
previous years:
[2017](https://conf.kde.org/en/akademy2017/public/schedule/speakers) and
[2015](https://conf.kde.org/en/akademy2015/public/schedule/speakers)

## What we offer

Many creative, interesting KDE, Qt and Free Software contributors and supporters
— your attentive and receptive audience. An opportunity to present your
application, share ideas and best practices, or gain new contributors. A cordial
environment with people who want you to succeed. This is your chance to make a
big splash!

Some travel funds are available. If your financial situation is the main reason
you might not attend, please apply for them. Akademy travel costs may be
reimbursed based on the KDE e.V. [Reimbursement
Policy](https://ev.kde.org/rules/reimbursement_policy.php). You can apply via
[https://reimbursements.kde.org](https://reimbursements.kde.org/)

## Proposal guidelines

Provide the following information on the proposal form:

- Title—the title of your session/presentation
- Abstract—a brief summary of your presentation
- Description—additional information not in the abstract; potential benefits of
  the topic for the audience
- A short bio—including anything that qualifies you to speak on your subject
- Feedback wanted—if you consider yourself as a beginner speaker or would like
  to receive more feedback and guidance from the Program Comitee to help you
prepare your speech

[Submit your
proposal](https://conf.kde.org/users/sign_in?conference_acronym=Akademy2018&locale=en)
**by March 12th, 23:59 CET.**

Akademy attracts people from all over the world. For this reason, all talks are
in English. If you need help with preparing your talk, please contact the
Program Committee.

Your submissions can be in any of the following forms:

- Technical talks are 30 minutes (possible extension to one hour)
- Panels are 30 minutes (possible extension to one hour)
- Fast track talks are 10 minutes long. They are featured in a single morning
  track.
- Lightning talks are 5 minutes long

Technical talks and fast track talk lengths include time for Q&A (questions and
answers). Lightning talks do not have Q&A.

For panel discussions of this year's Akademy we would love to see them focused
on any topic related to KDE's three goals.

The workshops are included in the 2nd part of Akademy and can last for up to
three hours.

Akademy has a lot of content so we will try hard to fit it into the schedule and
make sure that presentations start and end on time.

If your presentation must be longer than the standard time, please provide
reasons why this is so. Longer talks may be turned into workshops or special
sessions later in the week, outside of the main conference tracks. In that case,
we suggest a lightning-talk format teaser to promote the workshop. You don't
have to submit detailed proposals for the workshop parts yet. This will be done
later in an informal way. This call for participation focuses on the talks part
on the weekend.

For lightning talks, we will be collecting all of the presentations (we suggest
at most three slides) the day before the lightning track. All of the
presentations will be set-up and ready to go at the start of the lightning
track.

Akademy is upbeat, but it is not frivolous. Help us see that you care about your
topic, your presentation and your audience. Typos, sloppy or all-lowercase
formatting and similar appearance oversights leave a bad impression and may
count against your proposal. There's no need to overdo it. If it takes more than
two paragraphs to get to the point of your topic, it's too much and should be
slimmed down. The quicker you can make a good impression,both on the program
committee and your audience, the better.

We are looking for originality. Akademy is intended to move KDE forward. Having
the same people talking about the same things doesn't accomplish that goal.
Thus, we favor original and novel content. If you have presented on a topic
elsewhere, please add a new twist, new research, or recent development,
something unique. Of course, if your talk is plain awesome as is, go for that.

Everyone submitting a proposal will be notified by mid April, as to whether or
not their proposal was accepted for the Akademy 2018 Program.

All talks will be recorded and published on the Internet for free, along with a
copy of the slide deck, live demo or anything else associated with the
presentations. This benefits the larger KDE Community and those who can't make
it to Akademy. You will retain full ownership of your slides and other
materials, we request that you make your materials available explicitly under
the CC-BY license.

## Who we are

Meet the Akademy 2018 Program Committee:

Markus Feilner, Adriaan de Groot, Jos van den Oever, Aitor Pazos, Marta
Rybczynska, John Samuel, Cornelius Schumacher & Bhushan Shah

If you have any questions you can email the [Program
Committee](mailto:akademy-talks@kde.org)

