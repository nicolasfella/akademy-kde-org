---
title: Pre-Registration Event
menu:
  "2018":
    weight: 26
    parent: details
---

For all attendees that arrive on Friday, 10th of August or earlier that week
there is a chance to pick up their badges at the early registration get-together
on Friday, 10th of August at the **Kontaktraum** in the 6th floor of the venue from
18:00 to 22:00 with food and drinks provided. This way, you'll be also able to
avoid the Saturday morning crowd.

You can pick up your badge there, meet fellow Akademy attendees, and enjoy
drinks ( both alcoholic & non-alcoholic available) and food (vegan & vegetarian
option available). If the weather is nice you can use the terrace to take a look
at the inner city of Vienna from above.

## Map

![Map of venue and closest metro stations](/media/2018/map_venue_0.png)

[OpenStreetMap link](https://www.openstreetmap.org/#map=16/48.1980/16.3719)

## Address & Directions:

Kontaktraum   
Neues EI, 6th floor  
TU Wien  
Gusshaustraße 27-29  
1040 Wien

Enter the venue and use the elevators to get up to the 6th floor. You will be
directly in front of the Kontaktraum, where the pre-registration get-together is
happening.

