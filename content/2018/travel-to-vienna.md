---
title: Travelling to Vienna
menu:
  "2018":
    weight: 32
    parent: travel
---

You can travel to vienna [by airplane](#by-airplane), [by train](#by-train), [by
bus](#by-bus), or [by car](#by-car).

## By Airplane

### To Vienna International Airport (Flughafen Wien Schwechat, VIE)

Vienna International Airport has frequent connections to Frankfurt, Berlin,
Munich, Zurich, London and many other cities within Europe and abroad. See
[Arriving at
Vienna](http://www.viennaairport.com/en/passengers/arriving_passenger) for
detailed information about the airport facilities.

From the airport you can travel to the city

- by train:
  - Railjet (RJ) with any destination of Salzburg, Linz, Innsbruck,
München, or Bregenz will take you to Wien Hauptbahnhof. This is the train you
want to take if you're staying in the [recommended
accommodation](/2018/accomodation).
  - S7 (Destination *Wien Floridsdorf*) will take you to *Wien Mitte*.  
    Price: 4,20 EUR (if you don't have bought a ticket for Vienna in advance, ensure 
    to include *Kernzone*, then you do not have to buy another ticket for a connecting
    metro/tram/bus ride after getting off the train)
  - CAT (City Airport Train) will take you to *Wien Mitte*. Note: It has got its own 
    (more expensive) fare which does not include Kernzone, i. e. you have to buy a 
    separate ticket for any connecting metro/tram/bus ride. 
  You can use route planning on [Wiener Linien](https://www.wienerlinien.at/) to determine
  the proper metro/tram/bus lines after getting off the train.

- by taxi:
  - Estimated price to Vienna: depends on the Taxi company, with the cheapest one starting
  at 25 EUR (you can check the prices for different companies at 
  https://www.flughafentaxi-wien.at/en/ 

## By Train
### To Wien Hauptbahnhof (Wien Hbf)

Wien Hauptbahnhof (Vienna Central Station) is the main hub for public transport.
All international trains and all national ÖBB-trains via Vienna stop there.

From Hauptbahnhof you can continue

- by public transport:
  - to the [conference venue](/2018/venue): Metro U1 (Dest. *Leopoldau*)
    till *Taubstummengasse*, from there walk to Favoritenstraße 3-5, then turn right
    to Gußhausstraße 27.
  - with custom itinerary: Use route planning on
  [Wiener Linien](https://www.wienerlinien.at/).
- by walking:
  - to the recommended accommodation at Sonnwendgasse 11 which will take approx. 5 to 10 min. 

## By Bus

Vienna is reachable by quite a few bus companies. Be aware that they all stop at
different places in Vienna, there is no central bus station.

The following bus companies drive to Vienna:

- Eurolines
- Flixbus
- Student Agency (from/to Czechia)
- Orangeways (from/to Hungary)
- Polskibus (from/to Poland)

## By Car

- Inbound from west, take A1 till Vienna (Wien).
- Inbound from north, take A5, S1, S2.
- Inbound from south, take A2. 

Austrian freeways (Autobahn) have a toll system. You can buy an Autobahnvignette
at booths at the border. In Vienna, parking is severly restricted.  In most
districts, you can only park for a few hours and need to pay to park your car.
More Information:  
[https://www.wien.gv.at/english/transportation/parking/](https://www.wien.gv.at/english/transportation/parking/).

We recommend to leave your car at a Park & Ride facility, where parking for one
week costs only around 17 EUR. More Information:
[http://www.parkeninwien.at/en/Park-and-Ride.html](http://www.parkeninwien.at/en/Park-and-Ride.html)

